Version 1/151229 of Real Time Goal Seeking Characters by Nathanael Marion begins here.

Volume 1 - Dependencies

Include Editable Stored Actions by Ron Newcomb.
Include After Not Doing Something by Ron Newcomb.

Volume 2 - Standard rules modifications

[This is required because the action conversion causes problems when deciding whether a action succeeded. TODO: find a cleaner workaround?]

Check an actor asking something for (this is the translate asking for to giving without conversion rule):
	if the actor is the player:
		convert to request of the noun to perform giving it to action with the second noun and the actor;
	else:
		try the noun giving the second noun to the actor;

The translate asking for to giving without conversion rule is listed instead of the translate asking for to giving rule in the check asking it for rules.

Volume 3 - People have got objectives

Book 1 - Busy time remaining

[THis is the time an action will take, and someone will perform his next action when this number becomes zero.
Currently not used.]

A person has a number called busy time remaining. The busy time remaining of a person is usually 0.

Definition:
	A person is busy rather than idle if the busy time remaining of it is at least 1 and it is not the player.

Book 2 - Objectives

Part 1 - People list of objectives

A person has a list of stored actions called the objectives. [The stack of things a person has to do.]
A person has a number called stubbornness. The stubbornness of a person is usually 0. [To determine if someone is attempting the same action over and over without success.]

[if the objectives of a person is empty.]
Definition:
	a person is objectiveless if the number of entries in the objectives of it is 0 and it is not the player.


[If an actor fails 30 times in a row, he becomes objectiveless (to prevent infinite loops or very long objectives lists).

This can be change if the author make a situation with requires a long series of failures before someone achieving his objective.
Exemple: A person tries to open a locked door. He looks for the key, which is in a locked chest, the key of which is in another locked chest, the key of which is in another...
This person will fail every action until he finds the key of the last chest.]

The objectives limit is initially 30.


Chapter 1 - Controlling the objectives of people

[To determine the latest objective of someone.]
To decide what stored action is the next/last objective of (P - a person):
	if the number of entries in the objectives of P is 0 or P is the player: [if the person has no objective or is the player.]
		decide on the action of P waiting;
	let N be the number of entries in the objectives of P;
	let the next action be the entry N of the objectives of P;
	decide on the next action.

[To add an objective to someone.]
To (P - a person) will try (deed - a stored action):
	if P is the player, stop; [because the story never decides what the player have to do next.]
	if the actor part of the deed is not P: [so that the actor in the stored action is the person who will perform it.]
		now the actor part of the deed is P;
	add the deed to the objectives of P.

[To remove the last objective of someone.]
To remove the next/last objective of (P - a person):
	if P is the player, stop; [because the story never has never decided what the player had to do next.]
	if the number of entries in the objectives of P is greater than 0:
		let N be the number of entries in the objectives of P;
		remove entry N from the objectives of P.

[To remove all objective of someone.]
To clear the objectives of (P - a person):
	truncate the objectives of P to 0 entries.

[So that if the player asks something to someone, that person will add the request to his objectives.]
Unsuccessful attempt by someone (called P) doing something (this is the add request to objectives rule):
	P will try the current action.

[This is used by some rules below, but I'm not sure if this is really useful.]
To decide whether (deed - a stored action) aims possession:
	if action name part of deed is taking action or action name part of deed is asking it for action, yes;
	no. 

Part 2 - The new objective rulebook

[This rulebook is used for determining the next action of someone when the last action performed was unsuccessful.]

The new objective rules is an rule based rulebook producing a stored action.
The new objective rulebook have a person called actor. [For the author's convenience (shorter to type than "person asked")]

First new objective rule (this is the new objective set variables rule):
	now the actor is the person asked;

Last new objective rule (this is the actor could not acquire new objective rule):
	rule succeeds with result the action of the actor waiting.

Part 3 - Making people achieve their objectives

Every minute (this is the make people aim for their objective every minute rule):
	repeat with P running through on-stage persons which are not the player:
		if P is idle and P is not objectiveless:
			let deed be the next objective of P;
			if the deed aims possession and P encloses the noun part of the deed:
				[say "[deed] is aiming possession.";]
				remove the next objective of P;
			try the next objective of P;
		else if P is busy:
			decrement the busy time remaining of P;

After not someone (called P) doing something (this is the seek a new objective rule):
	[say "[reason the action failed][line break]";]
	let the solution be the stored action produced by the new objective rules for the reason the action failed; [The actor tries to find an action to bypass the obstacle.]
	[say "[next objective of P]";]
	if the number of entries of the objectives of P is greater than the objectives limit: [Just to be safe.]
		carry out the determining a new aim activity with P; [He becomes objectiveless.]
		stop;
	if the action name part of the solution is not the waiting action: [If he found a new objective.]
		if the actor part of the solution is not P, now the actor part of the solution is P;
		if the solution is listed in the objectives of P, increment the stubbornness of P; [To track whether he is attempting the same thing multiple times.]
		add the solution to the objectives of P; 
		if the stubbornness of P is less than 2: [If he tried uselessly the same thing less than a certain limit. TODO: tweak the limit?]
			let deed be the next objective of P;
			if the deed aims possession and P encloses the noun part of the deed:
				[say "[deed] is aiming possession.";]
				remove the next objective of P;
			try the next objective of P; 
		else:
			carry out the determining a new aim activity with P; [If he tried and failed the same action too many time.]
	else: [If he didn't find a new objective.]
		carry out the determining a new aim activity with P;

[Removes the last objective of someone if he achieved it.]
First after someone doing something (this is the remove objective after success rule):
	now the stubbornness of the actor is 0;
	[say "removing [current action]...";]
	remove the current action from the objectives of the actor, if present;
	continue the action.

[To be sure it is first. It has to, to the author can write his own after rules (that add new objectives, for exemple).]
The remove objective after success rule is listed first in the after rules.

Chapter 1 - Determining a new aim activity

[This is a hook for the author to try to find something to do for people who become objectiveless.]

Determining a new aim for something is an activity on persons.

Before determining a new aim for someone (called P) (this is the reset the objective of someone rule):
	now the stubbornness of P is 0;
	clear the objectives of P;


Volume 4 - Duration of actions

[Table of Action Durations
Deed	Duration
Taking inventory action	1
Taking action	1
Removing it from action	1
Dropping action	1
Putting it on action	1
Inserting it into action	1
Eating  action	1
Going action	1
Entering action	1
Exiting action	1
Getting off action	1
Looking action	1
Examining action	1
Looking under action	1
Searching action	1
Consulting it about  action	1
Locking it with action	1
Unlocking it with action	1
Switching on action	1
Switching off action	1
Opening action	1
Closing action	1
Wearing action	1
Taking off  action	1
Giving it to action	1
Showing it to action	1
Waking action	1
Throwing it at action	1
Attacking action	1
Kissing action	1
Answering it that action	1
Telling it about action	1
Asking it about action	1
Asking it for  action	1
Waiting action	1
Touching action	1
Waving action	1
Pulling action	1
Pushing action	1
Turning action	1
Pushing it to action	1
Squeezing  action	1
Saying yes action	1
Saying no action	1
Burning action	1
Waking up action	1
Thinking action	1
Smelling action	1
Listening to action	1
Tasting action	1
Cutting action	1
Jumping action	1
Tying it to action	1
Drinking action	1
Saying sorry action	1
Swinging action	1
Rubbing action	1
Setting it to action	1
Waving hands action	1
Buying action	1
Climbing action	1
Sleeping  action	1]





[OLD VERSION, JUST IN CASE]

[Volume 1 - Dependencies

Include Editable Stored Actions by Ron Newcomb.
Include After Not Doing Something by Ron Newcomb.

Volume 2 - People have got objectives

Book 1 - Busiy time remaining

A person has a number called busy time remaining. The busy time remaining of a person is usually 0.

Definition:
	A person is busy rather than idle if the busy time remaining of it is at least 1 and it is not the player.

Book 2 - Objectives

Part 1 - List of objectives

A person has a list of stored actions called the objectives.

Definition:
	a person is objectiveless if the number of entries in the objectives of it is 0 and it is not the player.

To decide what stored action is the next action of (P - a person):
	if the number of entries in the objectives of P is 0:
		decide on the action of P waiting;
	let N be the number of entries in the objectives of P;
	let the next action be the entry N of the objectives of P;
	decide on the next action.

[To decide what stored action is the next action of (P - a person):
	if the number of entries in the objectives of P is 0:
		decide on the action of P waiting;
	let N be the number of entries in the objectives of P;
	let the next action be the entry N of the objectives of P;
	if the actor part of the next action is not P:
		now the actor part of the next action is P;
	decide on the next action.]

To (P - a person) will try (deed - a stored action):
	if the actor part of the deed is not P:
		now the actor part of the deed is P;
	add the deed to the objectives of P.

To remove the last action of (P - a person):
	if the number of entries in the objectives of P is greater than 0:
		let N be the number of entries in the objectives of P;
		remove entry N from the objectives of P.

To clear the objectives of (P - a person):
	truncate the objectives of P to 0 entries.

Part 2 - The new objective rulebook

The new objective rules is an rule based rulebook producing a stored action.

Last new objective rule:
	rule succeeds with result the action of the goal seeker waiting.

The goal seeker is a person that varies.
The goal seeker noun is an object that varies.
The goal seeker second noun is an object that varies.

Part 3 - Updating the state activity

Every minute:
	repeat with P running through on-stage persons which are not the player:
		carry out the updating the state activity with P;
		let L be a list of stored actions;
		let iteration be 0;
		let counter be 0;
		while the reason the action failed is not the Startup rulebook:
			increment iteration;
			[say "[reason the action failed]";]
			add the next action of P to L;
			carry out the updating the state activity with P;
			if the next action of P is listed in L, increment counter;
			if (counter is 2 and the reason the action failed is not the Startup rulebook) or (iteration >= 50):
				clear the objectives of P;
				say "Objectiveless [P] !";
				break.

Updating the state of something is an activity on persons.

First before updating the state of somebody (called P):
	now the goal seeker is P;
	now the goal seeker noun is the noun part of the next action of the goal seeker;
	now the goal seeker second noun is the second noun part of the next action of the goal seeker;

For updating the state of a busy person (called P):
	decrement the busy time remaining of P.

For updating the state of a idle person when the number of entries in the objectives of the goal seeker is 0:
	do nothing.

For updating the state of a idle person (called P):
	[say "[next action of P], [actor part of the next action of P]";]
	if the action name part of the next action of P is not the waiting action:
		try the next action of P;
		if the reason the action failed is not the Startup rulebook:
			[say "[reason the action failed][line break]";]
			let the next objective be the stored action produced by the new objective rules for the reason the action failed;
			[say "[next objective]";]
			if the action name part of the next objective is not the waiting action:
				if the actor part of the next objective is not P, now the actor part of the next objective is P;
				add the next objective to the objectives of P;
			else:
				remove the last action of P;
		[say "[the objectives of P in brace notation]";]

Last after updating the state of somebody:
	now the goal seeker is the player.

First after someone doing something (this is the remove objective after success rule):
	remove the current action from the objectives of the actor;
	continue the action.

The remove objective after success rule is listed first in the after rules.

Volume 3 - Duration of actions

Table of Action Durations
Deed	Duration
Taking inventory action	1
Taking action	1
Removing it from action	1
Dropping action	1
Putting it on action	1
Inserting it into action	1
Eating  action	1
Going action	1
Entering action	1
Exiting action	1
Getting off action	1
Looking action	1
Examining action	1
Looking under action	1
Searching action	1
Consulting it about  action	1
Locking it with action	1
Unlocking it with action	1
Switching on action	1
Switching off action	1
Opening action	1
Closing action	1
Wearing action	1
Taking off  action	1
Giving it to action	1
Showing it to action	1
Waking action	1
Throwing it at action	1
Attacking action	1
Kissing action	1
Answering it that action	1
Telling it about action	1
Asking it about action	1
Asking it for  action	1
Waiting action	1
Touching action	1
Waving action	1
Pulling action	1
Pushing action	1
Turning action	1
Pushing it to action	1
Squeezing  action	1
Saying yes action	1
Saying no action	1
Burning action	1
Waking up action	1
Thinking action	1
Smelling action	1
Listening to action	1
Tasting action	1
Cutting action	1
Jumping action	1
Tying it to action	1
Drinking action	1
Saying sorry action	1
Swinging action	1
Rubbing action	1
Setting it to action	1
Waving hands action	1
Buying action	1
Climbing action	1
Sleeping  action	1]


Real Time Goal Seeking Characters ends here.

---- DOCUMENTATION ----

The documentation will soon be written.