Version 2/210516 of Out Of Reach Things by Nathanael Marion begins here.

"Things can now be out of reach, and the player won't be able to touch such objects."

Part - Definition of distant and out of reach things

A thing can be distant or near. A thing is usually near.

[TODO:

Not sure if that definition is useful. After all, we could also simply explicitely mark items enclosed by a distant thing as distant.

Also, if a distant container contains a person and another thing, this thing will still be considered out of reach for the person by the definition below, which is not correct. We'd need to repeat through every ancestor of the thing and the person up to their common ancestor to check if one of them is distant.

Conversely, a character enclosed by a distant thing will be able to touch a thing that is not distant.

Even worse, a distant person will not be able to touch themself!

Distance is a surprisingly tricky concept.

I think looping through the ancestors is overkill because it's only needed in rare situations. So for the moment authors shouldn't put people in distant things.]

Definition: a thing is out of reach if it is distant or it is enclosed by something distant.

Part - Exposing required Inform 6 variables

Chapter - The untouchable object - unindexed

[Before following the accessibility rules, Inform sets the `untouchable_object` global to the object we are trying to access.]
The untouchable-object is an object that varies.
The untouchable-object variable translates into I6 as "untouchable_object".

Chapter - Reaching silently

[Before following the accessibility rules, Inform sets the `untouchable_silence` global to false if we shouldn't print anything (for example if we are mereley checking if someone can touch something outside the context of an action).]
To decide whether reaching silently: (- (untouchable_silence) -).

Part - The access out of reach things rule

The access out of reach things rule is listed before the access through barriers rule in the accessibility rulebook.

Accessibility rule (this is the access out of reach things rule):
	if the untouchable-object is out of reach:
		[We don't use `abide by the reaching at a distance rules` because we must take no decision if access is denied (the access through barriers rule should still be able to make a decision after this rule).]
		follow the reaching at a distance rules for the untouchable-object;
		if the outcome of the rulebook is the deny access outcome:
			rule fails;

Part - The reaching at a distance rulebook

Reaching at a distance rules are an object-based rulebook.
Reaching at a distance rules have outcomes allow access (success) and deny access (failure).

Last reaching at a distance something (called the target) (this is the can't reach at a distance rule):
	if the person reaching is the player and not reaching silently:
		say "[The target] [are] out of reach." (A);
	deny access;

Part - French (for use with French Language by Nathanael Marion)

The can't reach at a distance rule response (A) is "[The target] [es] hors de portée.".

Out Of Reach Things ends here.

---- DOCUMENTATION ----

Chapter: Use

With this extension, we can declare things as near (the default) or distant.

	The sun is a backdrop. The sun is everywhere. The sun is distant.

By default, actions requiring a touchable thing will be stopped with a refusal message if the thing in question is distant or enclosed by something distant (the extension defines an "out of reach" adjective for things in this situation):

	>take sun
	The sun is out of reach.

This behaviour is governed by the reaching at a distance rules, which are analogous to the reaching inside and reaching outside rulebooks provided by Inform. Initially, the reaching at a distance rulebook contains a single rule stating that out of reach things aren't touchable (as shown above).

If we want to show a different message, we can write:

	Reaching at a distance the sun:
		say "The sun is obviously too high.";
		deny access;

The two possible outcomes are "deny access" (as above) and "allow access". So if we want to allow the player to reach the sun in some situations:

	Reaching at a distance the sun when the player wears the wings:
		say "(reaching the sun with your wings)[command clarification break]";
		allow access;

We should keep in mind that even when allowing access, the action can still fail for other reasons (for instance, if the player is inside a closed container while the distant noun is outside).

The reaching at a distance rulebook is run whenever Inform tries to know if an out of reach thing is touchable, just after the "before" stage but before the "instead" stage (specifically, in the accessibility rules, before the access through barriers rule).

That means that the rulebook is also run for actions performed by characters other than the player, or if we are simply writing "if the player can touch the sun". If those cases can happen in our story, we should check them and print something only if needed. Here is the same rule as earlier, revised:

	Reaching at a distance the sun when the person reaching wears the wings:
		if the person reaching is the player and not reaching silently:
			say "(reaching the sun with your wings)[command clarification break]";
		allow access;

The condition "if the person reaching is the player" ensures that the message will only be shown for the player, and "not reaching silently" ensures nothing is printed when no message is needed. The text "(reaching the sun with your wings)" will not be shown every turn in the following case, for instance:

	Every turn:
		if the player can touch the sun:
			increase the temperature of the player by 1;

Something to keep in mind: having a person marked as distant or enclosed by something distant can cause strange bugs. If a person can perform actions or we check in the source whether they can touch something, we mustn't make them out of reach!

Finally, this extension is fully compatible with works written in French.

Chapter: Change log

Version 2/210516:

	- The extension was rewritten so that it also works correctly for characters other than the player and when no message should be printed (when writing "if the player can touch the sun", for example). The name of rules, adjectives and others have been changed to be more idiomatic.

Version 1/150829:

	Initial release.

Example: * Icarus - Demonstrating the basic use of this extension.

	*:"Icarus"

	Include Out Of Reach Things by Nathanael Marion.

	The Labyrinth is a room. "The sun shines far above you."

	The pair of wings is a wearable thing in the Labyrinth.

	The sun is a distant scenery thing in the Labyrinth.
	The description is "Your father Daedalus, who took off a few minutes ago, warned you not to touch the sun.".

	Reaching at a distance the sun when the player wears the wings:
		say "(reaching the sun with your wings)[command clarification break]";
		allow access;

	Instead of touching the sun:
		say "You touch the sun! Unfortunately, the wax holding the feathers of your wings together melt and you fall into the sea.";
		end the story;

	Test me with "x sun / touch sun / wear wings / touch sun".

Here, it will still be possible to examine the sun, since it doesn't require a touchable noun, but the "instead of touching the sun" rule will only be considered if the player is wearing the wings.
