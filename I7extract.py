import sys

# By Nathanaël Marion.
# Public Domain.
#
# This Python script will extract all strings from an I7 source
# and replace all the substitutions by appropriate text
# (for exemple "[bracket]" by "[") so one can easily
# spell check one's I7 story.
#
# Since this script expands the substitutions
# for easy spell checking, you'll have to report
# the modifications by hand in your source.
#
# Not all substitutions are supported yet.
# See the TODO list below for missing features.
# Some substitutions will probably never be implemented
# (lists, adapting verbs, printing variables...)
# because it would be too difficult or impossible.
#
# To run : python I7extract.py <path_of_source>
# The extracted strings will be written into
# a file called "originalFileName_strings.txt"
#
#
# TODO:
# - Allow the user to specify custom substitutions in an exernal file.
# - Allow the user to specify a custom name for the output file?
# - Support the following substitutions :
#     - the (object)
#     - if/else/end if
#     - first time/only
#     - one of
#     - unicode characters
#     - verbs, adjectives, pronouns, and so on
#     - regarding (object)
#     - for French: au (object), du (object)


# The substitutions defined by the built-in extensions.
default_subs = [
	("[bold type]", ""),
	("[italic type]", ""),
	("[roman type]", ""),
	("[fixed letter spacing]", ""),
	("[variable letter spacing]", ""),
	("[']", "'"),
	("[line break]", "\n"),
	("[no line beak]", ""),
	("[paragraph break]", "\n\n"),
	("[run paragraph on]", ""),
	("[command clarification break]", "\n"),
	("[here]", "here"),
	("[now]", "now"),
	("[bracket]", "["),
	("[close bracket]", "]")
]

# The substitutions defined by the French language extension.
french_subs = [
	("[gras]", ""),
	("[italique]", ""),
	("[romain]", ""),
	("[largeur fixe]", ""),
	("[largeur variable]", ""),
	("[--]", "—"),
	("[_]", " "),
	("[à la ligne]", "\n"),
	("[sans passage à la ligne]", ""),
	("[saut de paragraphe]", "\n\n"),
	("[ici]", "ici"),
	("[maintenant]", "maintenant"),
	("[crochet]", "["),
	("[crochet fermant]", "]")
]

substitutions = default_subs + french_subs

def multi_replace(x, r):
	"""Replaces some texts in the string x according to the list of tuples r."""
	for i in r:
		x = x.replace(*i)
	return x

# The path of the source.
path = sys.argv[1]

# We read the source.
with open(path, "r") as f:
	source = f.read()
	input_file_name = f.name


strings = [] # The list that will contain the story's strings.
start = -1 # Will keep track of the beginning of each string.

# We run through the source to find the strings.
for i in range(len(source)):
	if source[i] == "\"":
		if start == -1: # Start of string.
			start = i + 1
		else: # End of string.
			current_string = multi_replace(source[start:i], substitutions) # We expand the subsitutions of the current string...
			strings.append(current_string) # ... and append it to the list.
			start = -1


# We create the output file name.
output_file_name = input_file_name.replace(".txt", "")
output_file_name = output_file_name.replace(".ni", "")
output_file_name += "_strings.txt"

# We write the output file. Each string will be separated by a row of "=".
with open(output_file_name, "w") as f:
	f.write("\n\n==========\n\n".join(strings))
