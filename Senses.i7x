Version 1/171103 of Senses by Nathanael Marion begins here.

"Makes things and rooms have sounds, scents, textures and tastes, which are analogous to the description property, but for the other senses."

Use authorial modesty.

Part 1 - Redefining the senses

Chapter 1 - Listening

A thing has a text called sound. The sound of a thing is usually "".
A room has a text called sound. The sound of a room is usually "".

The standard report listening rule is listed instead of the report listening rule in the report listening to rulebook.

Report an actor listening (this is the standard report listening rule):
	if the actor is the player:
		if the sound of the noun is empty:
			say "[We] [hear] nothing unexpected." (A);
		else:
			say "[sound of the noun][line break]";
	else:
		say "[The actor] [listen][if the noun is a thing]to [the noun][end if]." (B).

Chapter 2 - Smelling

A thing has a text called scent. The scent of a thing is usually "".
A room has a text called scent. The scent of a room is usually "".

The standard report smelling rule is listed instead of the report smelling rule in the report smelling rulebook.

Report an actor smelling (this is the standard report smelling rule):
	if the actor is the player:
		if the scent of the noun is empty:
			say "[We] [smell] nothing unexpected." (A);
		else:
			say "[scent of the noun][line break]";
	else:
		say "[The actor] [sniff][if the noun is a thing] [the noun][end if]." (B).

Chapter 3 - Touching

A thing has a text called texture. The texture of a thing is usually "".

The standard report touching things rule is listed instead of the report touching things rule in the report touching rulebook.

Report an actor touching (this is the standard report touching things rule):
	if the actor is the player:
		if the texture of the noun is empty:
			say "[We] [feel] nothing unexpected." (A);
		else:
			say "[texture of the noun][line break]";
	else:
		say "[The actor] [touch] [the noun]." (B).

Chapter 4 - Tasting

A thing has a text called taste. The taste of a thing is usually "".

The standard report tasting rule is listed instead of the report tasting rule in the report tasting rulebook.

Report an actor tasting (this is the standard report tasting rule):
	if the actor is the player:
		if the taste of the noun is empty:
			say "[We] [taste] nothing unexpected." (A);
		else:
			say "[taste of the noun][line break]";
	else:
		say "[The actor] [taste] [the noun]." (B).

Part 2 - Debugging commands - not for release

Chapter 1 - Sound

Definition: a thing is soundless if the sound of it is empty.
Definition: a room is soundless if the sound of it is empty.

Sound-debugging is an action out of world applying to nothing. Understand "soundless" as sound-debugging.

Report sound-debugging (this is the report soundless objects rule):
	if a room is soundless:
		say "The following rooms have no sound:[line break]" (A);
		repeat with O running through soundless rooms:
			say "-- [the O][line break]";
	else:
		say "Every room has a sound!" (B);
	if a thing is soundless:
		say "The following things have no sound:[line break]" (C);
		repeat with O running through soundless things:
			say "-- [the O][line break]";
	else:
		say "Everything has a sound!" (D).

Chapter 2 - Scent

Definition: a thing is scentless if the scent of it is empty.
Definition: a room is scentless if the scent of it is empty.

Scent-debugging is an action out of world applying to nothing. Understand "scentless" as scent-debugging.

Report scent-debugging (this is the report scentless objects rule):
	if a room is scentless:
		say "The following rooms have no scent:[line break]" (A);
		repeat with O running through scentless rooms:
			say "-- [the O][line break]";
	else:
		say "Every room has a scent!" (B);
	if a thing is scentless:
		say "The following things have no scent:[line break]" (C);
		repeat with O running through scentless things:
			say "-- [the O][line break]";
	else:
		say "Everything has a scent!" (D).

Chapter 3 - Texture

Definition: a thing is textureless if the texture of it is empty.

Texture-debugging is an action out of world applying to nothing. Understand "textureless" as texture-debugging.

Report texture-debugging (this is the report textureless things rule):
	if a thing is textureless:
		say "The following things have no texture:[line break]" (A);
		repeat with O running through textureless things:
			say "-- [the O][line break]";
	else:
		say "Everything has a texture!" (B).

Chapter 4 - Taste

Definition: a thing is tasteless if the taste of it is empty.

Taste-debugging is an action out of world applying to nothing. Understand "tasteless" as taste-debugging.

Report taste-debugging (this is the report tasteless things rule):
	if a thing is tasteless:
		say "The following things have no taste:[line break]" (A);
		repeat with O running through tasteless things:
			say "-- [the O][line break]";
	else:
		say "Everything has a taste!" (B).

Part 3 - In French (for use with French Language by Nathanael Marion)

The standard report listening rule response (A) is "[Tu] n['][adapt the verb entends for bg] rien de particulier.".
The standard report listening rule response (B) is "[The actor] [écoutes] attentivement[if the noun is a thing] [the noun][end if].".
The standard report smelling rule response (A) is "[Tu] ne [adapt the verb sens for bg] rien de particulier.".
The standard report smelling rule response (B) is "[The actor] [renifles][if the noun is a thing] [the noun][end if].".
The standard report touching things rule response (A) is "[Tu] [if the story tense is simple]ne [sens] rien[else if the story tense is perfect tense]n['][adapt the verb as in present tense] rien senti[else if the story tense is past perfect tense]n['][adapt the verb as in past tense] rien senti[end if] de particulier.".
The standard report touching things rule response (B) is "[The actor] [touches] [the noun].".
The standard report tasting rule response (A) is "[Tu] [if the story tense is simple]ne [remarques] rien[else if the story tense is perfect tense]n['][adapt the verb as in present tense] rien remarqué[else if the story tense is past perfect tense]n['][adapt the verb as in past tense] rien remarqué[end if] de particulier.".
The standard report tasting rule response (B) is "[The actor] [goûtes] [the noun].".

Chapter 1 - Debugging commands in French - not for release

The report soundless objects rule response (A) is "Les endroits suivants n'ont pas de [italic type]sound[roman type][_]:[line break]".
The report soundless objects rule response (B) is "Tous les endroit ont un [italic type]sound[roman type][_]!".
The report soundless objects rule response (C) is "Les choses suivantes n'ont pas de [italic type]sound[roman type][_]:[line break]".
The report soundless objects rule response (D) is "Toutes les choses ont un [italic type]sound[roman type][_]!".
The report scentless objects rule response (A) is "Les endroits suivants n'ont pas de [italic type]scent[roman type][_]:[line break]".
The report scentless objects rule response (B) is "Tous les endroits ont un [italic type]scent[roman type][_]!".
The report scentless objects rule response (C) is "Les choses suivantes n'ont pas de [italic type]scent[roman type][_]:[line break]".
The report scentless objects rule response (D) is "Toutes les choses ont un [italic type]scent[roman type][_]!".
The report textureless things rule response (A) is "Les choses suivantes n'ont pas de [italic type]texture[roman type][_]:[line break]".
The report textureless things rule response (B) is "Toutes les choses ont une [italic type]texture[roman type][_]!".
The report tasteless things rule response (A) is "Les choses suivantes n'ont pas de [italic type]taste[roman type][_]:[line break]".
The report tasteless things rule response (B) is "Toutes les choses ont un [italic type]taste[roman type][_]!".

Senses ends here.

---- DOCUMENTATION ----

This extension adds four text properties to things: the sound, the scent, the texture and the taste. They are used in the same way that the description property, but for the listening to, smelling, touching and tasting actions, respectively. Rooms have sounds and scents, too. Example:

	The amulet is a wearable thing. The sound of the amulet is "If you listen carefully to the amulet, you can hear voices whispering things that never should be said.". The scent is "Smell of metal. And blood.". The texture is "Cold. The less you touch it, the better.". The taste is "You prefer not to try it.".

This would give in-game:

	>listen to the amulet
	If you listen carefully to the amulet, you can hear voices whispering things that never should be said.
	
	>touch it
	Cold. The less you touch it, the better.

And so on.

The extension also provides four testing commands: SOUNDLESS, SCENTLESS, TEXTURELESS and TASTELESS, which list objects that haven't got the corresponding property.

Finally, this extension is fully compatible with works written in French.