Version 1/160101 of Standard Real Time Goals by Nathanael Marion begins here.

"Defines new objective rules used by the Real Time Goal Seeking Characters extension for the check rules of the Standard Rules."

Volume 1 - Useful phrases

A thing has a number called importance.

Definition:
	A thing is precious if its importance is 5 or more.

Definition:
	A thing is banal if its importance is 2 or less.

[TODO: raffiner le calcul.]
To update the importance of (D - a description of things):
	repeat with T running through D:
		if T is lit, increase the importance of T by 3;
		if T unlocks an object, increase the importance of T by 3;
		if T is a player's holdall, increment the importance of T;
		if T is a device, increment the importance of T;
		if T is edible, increment the importance of T;
		if T is wearable, increment the importance of T;

Volume 2 - New new objective rules

Book 1 - During action processing

[That one is a big one. It is partly taken from the implicitly pass through other barriers rule]

New objective for the basic accessibility rule:
	[Setting variables]
	let the current location be the location of the actor;
	let the current receptacle be the holder of the actor;
	let the target location be an object;
	let the target receptacle be an object;
	unless the actor can touch the noun:
		let the target location be the location of the noun;
		let the target receptacle be the holder of the noun;
	else unless the actor can touch the second noun:
		let the target location be the location of the second noun;
		let the target receptacle be the holder of the second noun;
	[if the actor is not in the same room than the noun]
	if the current location is not the target location:
		[if the actor is directly in the room]
		if the current receptacle is a room:
			[the actor will try to find the best route without doors]
			let way be the best route from the current location to the target location;
			if way is a direction:
				rule succeeds with result the action of the actor going way;
			[the actor will then try to find the best route with doors]
			let way be the best route from the current location to the target location, using doors;
			if way is a direction:
				rule succeeds with result the action of the actor going way;
			[the actor will then try to find the best route with locked doors]
			let way be the best route from the current location to the target location, using even locked doors;
			if way is a direction:
				rule succeeds with result the action of the actor going way;
		[else if the actor is enclosed by something]
		else:
			if the current receptacle is a supporter:
				rule succeeds with result the action of the actor getting off the current receptacle;
			else:
				rule succeeds with result the action of the actor exiting;
	[else if the actor is in the same room than the noun.]
	else:
		let the local ceiling be the common ancestor of the actor with the noun;
		[if the noun is "outer" than the actor]
		if the current receptacle is not the local ceiling:
			if the current receptacle is a supporter:
				rule succeeds with result the action of the actor getting off the current receptacle;
			else:
				rule succeeds with result the action of the actor exiting;
		[if the current receptacle is the noun, stop the action;]
		[if the current receptacle is the target receptacle, continue the action;]
		[let the target be the holder of the noun;]
		[if the noun is part of the target receptacle, let the target receptacle be the holder of the target receptacle;]
		[if the noun is "inner" than the actor]
		if the target receptacle is closed, rule succeeds with result the action of the actor opening the target receptacle;
		rule succeeds with result the action of the actor entering the target receptacle;
		[while the target is a thing:
			if the holder of the target is the local ceiling:
				silently try the actor trying entering the target;
				if the holder of the actor is not the target, stop the action;
				convert to the entering action on the noun;
				continue the action;
			let the target be the holder of the target;]

Book 2 - Standard actions concerning the actor's possessions

Part 1 - Taking

[New objective for the can't take yourself rule:]

[New objective for the can't take other people rule:]

[New objective for the can't take component parts rule:]

[New objective for the can't take people's possessions rule:]

[New objective for the can't take items out of play rule:]

[The actor will try to exit or to get off.]
New objective for the can't take what you're inside rule:
	if the noun is a container:
		rule succeeds with result the action of the actor exiting;
	else if the noun is a supporter:
		rule succeeds with result the action of the actor getting off the noun.

[New objective for the can't take what's already taken rule:]

[New objective for the can't take scenery rule:]

[New objective for the can only take things rule:]

[New objective for the can't take what's fixed in place rule:]

[If the actor failed to make room by using a player's holdall, he will try to drop something.]
New objective for the use player's holdall to avoid exceeding carrying capacity rule:
	update the importance of the things enclosed by the person asked;
	let T be the banalest thing enclosed by the actor;
	rule succeeds with result the action of the actor dropping T.

[The actor will try to drop something.]
New objective for the can't exceed carrying capacity rule:
	update the importance of the things enclosed by the person asked;
	let T be the banalest thing enclosed by the actor;
	rule succeeds with result the action of the actor dropping T.

Part 2 - Removing it from

[The actor will simply try to take the noun.]
New objective for the can't remove what's not inside rule:
	rule succeeds with result the action of the actor taking the noun.

[The actor will try to ask the person for the noun instead.]
New objective for the can't remove from people rule:
	rule succeeds with result the action of the actor asking the second noun for the noun.

Part 3 - Dropping

[New objective for the can't drop body parts rule:]

[New objective for the can't drop yourself rule:]

[New objective for the can't drop what's already dropped rule:]

[The actor will try to take the noun. TODO: useful? ]
New objective for the can't drop what's not held rule:
	if the noun is enclosed by a person (called P):
		rule succeeds with result the action of the actor asking P for the noun;
	rule succeeds with result the action of the actor taking the noun.

[The actor will try to take off the noun.]
New objective for the can't drop clothes being worn rule:
	rule succeeds with result the action of the actor taking off the noun.

[The actor will try getting off or exiting before dropping something.]
New objective for the can't drop if this exceeds carrying capacity rule:
	let the receptacle be the holder of the actor;
	if the receptacle is a supporter:
		rule succeeds with result the action of the actor getting off the receptacle;
	else if the receptacle is a container:
		rule succeeds with result the action of the actor exiting.

Part 4 - Putting it on

[The actor will try to take the noun.]
New objective for the can't put what's not held rule:
	if the noun is enclosed by a person (called P):
		rule succeeds with result the action of the actor asking P for the noun;
	rule succeeds with result the action of the actor taking the noun.

[New objective for the can't put something on itself rule:]

[TODO: make the actor drop the noun?]
[New objective for the can't put onto what's not a supporter rule:]

[The actor will try to take off the noun.]
New objective for the can't put clothes being worn rule:
	rule succeeds with result the action of the actor taking off the noun.

[The actor will try to take something on the second noun.]
New objective for the can't put if this exceeds carrying capacity rule:
	update the importance of the things on the second noun;
	let T be the preciousest portable thing enclosed by the actor;
	if T is a thing:
		rule succeeds with result the action of the actor taking T.

Part 5 - Inserting it into

[The actor will try to take the noun.]
New objective for the can't insert what's not held rule:
	if the noun is enclosed by a person (called P):
		rule succeeds with result the action of the actor asking P for the noun;
	rule succeeds with result the action of the actor taking the noun.

[New objective for the can't insert something into itself rule:]

[The actor will try to open the second noun.]
New objective for the can't insert into closed containers rule:
	rule succeeds with result the action of the actor opening the second noun.

[TODO: make the actor drop the noun?]
[New objective for the can't insert into what's not a container rule:]

[The actor will try to take off the noun.]
New objective for the can't insert clothes being worn rule:
	rule succeeds with result the action of the actor taking off the noun.

[The actor will try to take something in the second noun.]
New objective for the can't insert if this exceeds carrying capacity rule:
	update the importance of the things in the second noun;
	let T be the preciousest portable thing enclosed by the actor;
	if T is a thing:
		rule succeeds with result the action of the actor taking T.

Part 6 - Eating

[New objective for the can't eat unless edible rule:]

[The actor will try to take off the noun.]
New objective for the can't eat clothing without removing it first rule:
	rule succeeds with result the action of the actor taking off the noun.

[The actor will try to take the noun.]
New objective for the can't eat other people's food rule:
	if a person (called P) carries the noun:
		rule succeeds with result the action of the actor asking P for the noun;

[The actor will try to take the noun.]
New objective for the can't eat portable food without carrying it rule:
	rule succeeds with result the action of the actor taking the noun.

Book 3 - Standard actions which move the actor

Part 1 - Going

[The actor will try to get off or to exit.]
New objective for the can't travel in what's not a vehicle rule:
	let the nonvehicle be the holder of the actor;
	if the nonvehicle is a supporter:
		rule succeeds with result the action of the actor getting off the nonvehicle;
	else:
		rule succeeds with result the action of the actor exiting.

[New objective for the can't go through undescribed doors rule:]

[The actor will try to open the door.]
New objective for the can't go through closed doors rule:
	let way be the noun;
	let the obstacle be room-or-door way from the location of the actor;
	if the obstacle is not a door, make no decision;
	rule succeeds with result the action of the actor opening the obstacle.

[New objective for the can't go that way rule:]

Part 2 - Entering

[New objective for the can't enter what's already entered rule:]

[New objective for the can't enter what's not enterable rule:]

[TODO: déplacer]
[New objective for the French can't enter what's not enterable rule:]

[The actor will try to open the container.]
New objective for the can't enter closed containers rule:
	rule succeeds with result the action of the actor opening the noun.

[The actor will try to take the most valuable thing on the noun.]
New objective for the can't enter if this exceeds carrying capacity rule:
	let T be a thing;
	if the noun is a supporter:
		update the importance of the things on the noun;
		let T be the preciousest portable thing on the noun;
	else:
		update the importance of the things in the noun;
		let T be the preciousest portable thing in the noun;
	if T is a thing:
		rule succeeds with result the action of the actor taking T.

[The actor wil try to drop the noun.]
New objective for the can't enter something carried rule:
	rule succeeds with result the action of the actor dropping the noun.

[TODO: I don't really know how to take care of that.]
[New objective for the implicitly pass through other barriers rule:]

Part 3 - Exiting

[New objective for the can't exit when not inside anything rule:]

[The actor will try to open the container.]
New objective for the can't exit closed containers rule:
	rule succeeds with result the action of the actor opening the holder of the actor.

Part 4 - Getting off

[New objective for the can't get off things rule:]

Book 4 - Standard actions concerning the actor's vision

Part 1 - Searching

[New objective for the can't search unless container or supporter rule:]

New objective for the can't search closed opaque containers rule:
	rule succeeds with result the action of the actor opening the noun.

Book 5 - Standard actions which change the state of things

Part 1 - Locking it with

[New objective for the can't lock without a lock rule:]

[New objective for the can't lock what's already locked rule:]

[The actor will try to close the noun.]
New objective for the can't lock what's open rule:
	rule succeeds with result the action of the actor closing the noun.

[The actor will try to find the correct key if he hasn't got it. Otherwise, he will lock the container.]
New objective for the can't lock without the correct key rule:
	if something unlocks the noun:
		let the opener be the matching key of the noun;
		if the actor encloses the opener:
			rule succeeds with result the action of the actor locking the noun with the opener;
		if the opener is enclosed by a person (called P):
			rule succeeds with result the action of the actor asking P for the opener;
		rule succeeds with result the action of the actor taking the opener.

Part 2 - Unlocking with

[New objective for the can't unlock without a lock rule:]

[New objective for the can't unlock what's already unlocked rule:]

[The actor will try to find the correct key if he hasn't got it. Otherwise, he will unlock the container.]
New objective for the can't unlock without the correct key rule:
	if something unlocks the noun:
		let the opener be the matching key of the noun;
		if the actor encloses the opener:
			rule succeeds with result the action of the actor unlocking the noun with the opener;
		if the opener is enclosed by a person (called P):
			rule succeeds with result the action of the actor asking P for the opener;
		rule succeeds with result the action of the actor taking the opener.

Part 3 - Switching on

[New objective for the can't switch on unless switchable rule:]

[New objective for the can't switch on what's already on rule:]

Part 4 - Switching off

[New objective for the can't switch off unless switchable rule:]

[New objective for the can't switch off what's already off rule:]

Part 5 - Opening

[New objective for the can't open unless openable rule:]

[The actor will try to find the correct key if he hasn't got it. Otherwise, he will unlock the container.]
New objective for the can't open what's locked rule:
	if something unlocks the noun:
		let the opener be the matching key of the noun;
		if the actor encloses the opener:
			rule succeeds with result the action of the actor unlocking the noun with the opener;
		if the opener is enclosed by a person (called P):
			rule succeeds with result the action of the actor asking P for the opener;
		rule succeeds with result the action of the actor taking the opener.

[New objective for the can't open what's already open rule:]

Part 6 - Closing

[New objective for the can't close unless openable rule:]

[New objective for the can't close what's already closed rule:]

Part 7 - Wearing

[New objective for the can't wear what's not clothing rule:]

[The actor will try to take the noun.]
New objective for the can't wear what's not held rule:
	if the noun is enclosed by a person (called P):
		rule succeeds with result the action of the actor asking P for the noun;
	rule succeeds with result the action of the actor taking the noun.

[New objective for the can't wear what's already worn rule:]

Part 8 - Taking off

[New objective for the can't take off what's not worn rule:]

[The actor will try to put something in a player's holdall if he has one, otherwise he will try to drop something.]
New objective for the can't exceed carrying capacity when taking off rule:
	update the importance of the things carried by the person asked;
	let T be the banalest thing carried by the actor;
	if the actor carries a player's holdall (called the sack):
		rule succeeds with result the action of the actor inserting T into the sack;
	else:
		rule succeeds with result the action of the actor dropping T.

Book 6 - Standard actions concerning other people

Part 1 - Giving it to

[The actor will try to take the noun.]
New objective for the can't give what you haven't got rule:
	if the noun is enclosed by a person (called P):
		rule succeeds with result the action of the actor asking P for the noun;
	rule succeeds with result the action of the actor taking the noun.

[New objective for the can't give to yourself rule:]

[New objective for the can't give to a non-person rule:]

[The actor will try to take off the noun.]
New objective for the can't give clothes being worn rule:
	rule succeeds with result the action of the actor taking off the noun.

[New objective for the block giving rule:]

[New objective for the can't exceed carrying capacity when giving rule:]

Part 2 - Showing it to

[The actor will try to take the noun.]
New objective for the can't show what you haven't got rule:
	if the noun is enclosed by a person (called P):
		rule succeeds with result the action of the actor asking P for the noun;
	rule succeeds with result the action of the actor taking the noun.

[New objective for the block showing rule:]

Part 3 - Waking

[New objective for the block waking rule:]

Part 4 - Throwing it at

[The actor will try to take off the noun.]
New objective for the implicitly remove thrown clothing rule:
	rule succeeds with result the action of the actor taking off the noun.

[New objective for the futile to throw things at inanimate objects rule:]

[New objective for the block throwing at rule:]

Part 5 - Attacking

[New objective for the block attacking rule:]

Part 6 - Kissing

[New objective for the kissing yourself rule:]

[New objective for the block kissing rule:]

Part 7 - Telling it about

[New objective for the telling yourself rule:]

Part 8 - Asking it for

[New objective for the asking yourself for something rule:]

[New objective for the translate asking for to giving rule:]

Book 7 - Standard actions which are checked but then do nothing unless rules intervene


Part 1 - Waving

[New objective for the can't wave what's not held rule:]

Part 2 - Pulling

[New objective for the can't pull what's fixed in place rule:

New objective for the can't pull scenery rule:

New objective for the can't pull people rule:]

Part 3 - Pushing

[New objective for the can't push what's fixed in place rule:

New objective for the can't push scenery rule:

New objective for the can't push people rule:]

Part 4 - Turning

[New objective for the can't turn what's fixed in place rule:

New objective for the can't turn scenery rule:

New objective for the can't turn people rule:]

Part 5 - Pushing it to

[New objective for the can't push unpushable things rule:

New objective for the can't push to non-directions rule:

New objective for the can't push vertically rule:

New objective for the can't push from within rule:

New objective for the standard pushing in directions rule:

New objective for the block pushing in directions rule:]

Part 6 - Squeezing

[New objective for the innuendo about squeezing people rule:]

Book 7 - Standard actions which always do nothing unless rules intervene

[New objective for the block saying yes rule:

New objective for the block saying no rule:

New objective for the block burning rule:

New objective for the block waking up rule:

New objective for the block thinking rule:

New objective for the block cutting rule:

New objective for the block tying rule:

New objective for the block drinking rule:

New objective for the block saying sorry rule:

New objective for the block swinging rule:

New objective for the can't rub another person rule:

New objective for the block setting it to rule:

New objective for the block buying rule:

New objective for the block climbing rule:

New objective for the block sleeping rule:]

Standard Real Time Goals ends here.

---- DOCUMENTATION ----

The documentation will soon be written.