Version 1/171101 of Glulx Status Line Text Effects (for Glulx only) by Nathanael Marion begins here.

"Gives control over text formatting for the status line in Glulx. Requires version 5 of Glulx Text Effects."

"adapted from Glulx Text Effects by Emily Short"

Use authorial modesty.

Include version 5 of Glulx Text Effects by Emily Short.

Part 1 - The Table of Status Line User Styles definition

Table of Status Line User Styles
style name (a glulx text style)	background color (a text)	color (a text)	first line indentation (a number)	fixed width (a truth state)	font weight (a font weight)	indentation (a number)	italic (a truth state)	justification (a text justification)	relative size (a number)	reversed (a truth state)
with 1 blank row

Part 2 - Sorting the Table of Status Line User Styles

[We sort the table of styles to combine style definitions together]

Before starting the virtual machine (this is the sort the Table of Status Line User Styles rule):
	[First change empty style names to all-styles]
	repeat through the Table of Status Line User Styles:
		if there is no style name entry:
			now the style name entry is all-styles;
	sort the Table of Status Line User Styles in style name order;
	let row1 be 1;
	let row2 be 2;
	[Overwrite the first row of each style with the specifications of subsequent rows of the style]
	while row2 <= the number of rows in the Table of Status Line User Styles:
		choose row row2 in the Table of Status Line User Styles;
		if there is a style name entry:
			if (the style name in row row1 of the Table of Status Line User Styles) is the style name entry:
				if there is a background color entry:
					now the background color in row row1 of the Table of Status Line User Styles is the background color entry;
				if there is a color entry:
					now the color in row row1 of the Table of Status Line User Styles is the color entry;
				if there is a first line indentation entry:
					now the first line indentation in row row1 of the Table of Status Line User Styles is the first line indentation entry;
				if there is a fixed width entry:
					now the fixed width in row row1 of the Table of Status Line User Styles is the fixed width entry;
				if there is a font weight entry:
					now the font weight in row row1 of the Table of Status Line User Styles is the font weight entry;
				if there is a indentation entry:
					now the indentation in row row1 of the Table of Status Line User Styles is the indentation entry;
				if there is a italic entry:
					now the italic in row row1 of the Table of Status Line User Styles is the italic entry;
				if there is a justification entry:
					now the justification in row row1 of the Table of Status Line User Styles is the justification entry;
				if there is a relative size entry:
					now the relative size in row row1 of the Table of Status Line User Styles is the relative size entry;
				if there is a reversed entry:
					now the reversed in row row1 of the Table of Status Line User Styles is the reversed entry;
				blank out the whole row;
			otherwise:
				now row1 is row2;
		increment row2.

Part 3 - Setting the status line styles - unindexed

Last before starting the virtual machine (this is the set text styles for the status line rule):
	repeat through the Table of Status Line User Styles:
		if there is a background color entry:
			set the status line background color for the style name entry to the background color entry;
		if there is a color entry:
			set the status line color for the style name entry to the color entry;
		if there is a first line indentation entry:
			set the status line first line indentation for the style name entry to the first line indentation entry;
		if there is a fixed width entry:
			set status line fixed width for the style name entry to the fixed width entry;
		if there is a font weight entry:
			set the status line font weight for the style name entry to the font weight entry;
		if there is a indentation entry:
			set the status line indentation for the style name entry to the indentation entry;
		if there is a italic entry:
			set status line italic for the style name entry to the italic entry;
		if there is a justification entry:
			set the status line justification for the style name entry to the justification entry;
		if there is a relative size entry:
			set the status line relative size for the style name entry to the relative size entry;
		if there is a reversed entry:
			set status line reversed for the style name entry to the reversed entry.

To set the status line background color for (style - a glulx text style) to (N - a text):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_BackColor, GTE_ConvertColour( {N} ) ); -).

To set the status line color for (style - a glulx text style) to (N - a text):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_TextColor, GTE_ConvertColour( {N} ) ); -).

To set the status line first line indentation for (style - a glulx text style) to (N - a number):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_ParaIndentation, {N} ); -).

To set status line fixed width for (style - a glulx text style) to (N - truth state):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_Proportional, ( {N} + 1 ) % 2 ); -).

To set the status line font weight for (style - a glulx text style) to (N - a font weight):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_Weight, {N} - 2 ); -).

To set the status line indentation for (style - a glulx text style) to (N - a number):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_Indentation, {N} ); -).

To set status line italic for (style - a glulx text style) to (N - a truth state):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_Oblique, {N} ); -).

To set the status line justification for (style - a glulx text style) to (N - a text justification):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_Justification, {N} - 1 ); -).

To set the status line relative size for (style - a glulx text style) to (N - a number):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_Size, {N} ); -).

To set status line reversed for (style - a glulx text style) to (N - a truth state):
	(- GTE_SetStylehint( wintype_TextGrid, {style}, stylehint_ReverseColor, {N} ); -).

Glulx Status Line Text Effects ends here.

---- DOCUMENTATION ----

This extension allows us to change the appearance of the text in the status line.

It works exactly like Glulx Text Effects by Emily Short, except that the table defining the status line styles is called Table of Status Line User Styles. See Glulx Text Effects' documentation for usage.