Vorple for 6L38 (for Glulx only) by Nathanael Marion begins here.

"Replaces parts of the Vorple extension so that they work with 6L38."

Use authorial modesty.

Include version 3/181103 of Vorple by Juhana Leinonen.

Volume 1 - Vorple

Chapter 6 - Prompt (in place of Chapter 6 - Prompt in Vorple by Juhana Leinonen)

[This is an internal helper variable that shouldn't be changed manually. To change the prompt, changing the usual "command prompt" variable should work fine.]
The Vorple prompt is text that varies. The Vorple prompt is "".

Last before reading a command (this is the convert default prompt to Vorple prompt rule):
	if Vorple is supported:
		let new prompt be the substituted form of the command prompt; [this prevents any say phrases with side effects inside the command prompt from triggering twice]
		if the Vorple prompt is not the new prompt:
			now the Vorple prompt is the new prompt;
			execute JavaScript command "vorple.prompt.setPrefix('[escaped Vorple prompt]')".

Last for clarifying the parser's choice (this is the change Vorple prompt when clarifying choice rule):
	follow the convert default prompt to Vorple prompt rule;
	make no decision.
	
Last after asking which do you mean (this is the change Vorple prompt when asking which do you mean rule):
	follow the convert default prompt to Vorple prompt rule;
	make no decision.

First for printing a parser error when the latest parser error is the I beg your pardon error (this is the change Vorple prompt when input is empty rule):
	follow the convert default prompt to Vorple prompt rule;
	make no decision.

This is the print the final prompt with Vorple rule:
	if Vorple is supported:
		do nothing; [Vorple will print its own prompt]
	otherwise:
		follow the print the final prompt rule.

The print the final prompt with Vorple rule is listed instead of the print the final prompt rule in the before handling the final question rulebook.

Include (-
Replace YesOrNo;
-) before "Parser.i6t".

Include (-
[ YesOrNo i j;
	for (::) {
		if (location ~= nothing && parent(player) ~= nothing) DrawStatusLine();
		KeyboardPrimitive(buffer2, parse2);
		j = parse2-->0;
		if (j) { ! at least one word entered
			i = parse2-->1;
			if (i == YES1__WD or YES2__WD or YES3__WD) rtrue;
			if (i == NO1__WD or NO2__WD or NO3__WD) rfalse;
		}
		YES_OR_NO_QUESTION_INTERNAL_RM('A');

		! VORPLE PRINTS ITS OWN PROMPT
		if (vorple_support == false) print "> ";
	}
];
-) after "Parser.i6t".

Include (-
Replace PrintPrompt;
-) before "Printing.i6t".

Include (-
[ PrintPrompt i;
	RunTimeProblemShow();
	ClearRTP();
	style roman;
	EnsureBreakBeforePrompt();
	if( ~~(+ Vorple support +) )	TEXT_TY_Say( (Global_Vars-->1) );
	ClearBoxedText();
	ClearParagraphing(14);
];
-) after "Printing.i6t".

Volume 2 - Vorple Status Line (for use with Vorple Status Line by Juhana Leinonen)

Chapter 1 - Constructing the status line (in place of Chapter 1 - Constructing the status line in Vorple Status Line by Juhana Leinonen)

Left hand Vorple status line, middle Vorple status line, right hand Vorple status line and mobile Vorple status line are text that varies.

The left hand Vorple status line is usually " [left hand status line]".
The right hand Vorple status line is usually "[right hand status line] ".
The mobile Vorple status line is usually "[if the Vorple status line size is 1][middle Vorple status line][otherwise][left hand status line][end if]".

[don't change this number directly – internal use only]
The Vorple status line size is a number that varies. The Vorple status line size is 0.

Constructing the Vorple status line is an activity.

To construct the/a/-- Vorple status line with (column count - number) column/columns:
	if column count > 3 or column count < 0:
		throw Vorple run-time error "Vorple Status Line: status line must have exactly 1, 2 or 3 columns, [column count] requested";
		rule fails;
	now Vorple status line size is column count;
	remove the Vorple status line;
	place an element called "status-line-container" at the top level;
	if the full-width status line option is active:
		execute JavaScript command "$('.status-line-container').prependTo('main#haven')";
	otherwise:
		execute JavaScript command "$('.status-line-container').prependTo('#output')";
	set output focus to element called "status-line-container";
	if column count is greater than 1:
		place a block level element called "status-line-left col-xs lg-only";
	if column count is not 2:
		place a block level element called "status-line-middle col-xs lg-only";
	if column count is greater than 1:
		place a block level element called "status-line-right col-xs lg-only";
	place a block level element called "status-line-mobile col-xs sm-only";
	set output focus to the main window.
	
Last for constructing the Vorple status line (this is the default Vorple status line rule):
	if Vorple status line size is greater than 1:
		display text left hand Vorple status line in the element called "status-line-left";
	if Vorple status line size is not 2:
		display text middle Vorple status line in the element called "status-line-middle";
	if Vorple status line size is greater than 1:
		display text right hand Vorple status line in the element called "status-line-right";
	display text mobile Vorple status line in the element called "status-line-mobile";
	make no decision.

To refresh the/-- Vorple status line:
	if Vorple is supported and the Vorple status line size > 0:
		save the internal state of line breaks;
		carry out the constructing the Vorple status line activity;
		restore the internal state of line breaks.

To remove the/-- Vorple status line:
	remove the element called "status-line-container".
	
To clear the/-- Vorple status line:
	execute JavaScript command "$('.status-line-container').children().empty()".

A Vorple interface update rule (this is the refresh Vorple status line rule):
	refresh the Vorple status line.

Vorple for 6L38 ends here.

---- DOCUMENTATION ----

Some parts of the Vorple extensions uses syntax that are not recognised by Inform 7 6L38. This extension just replaces those parts so that they use 6L38 syntax. Everything is equivalent otherwise.

To use it, just include it instead of the version 3/181103 of Vorple by Juhana Leinonen. Using the extension with any other version may cause issues.

For the curious, this extension only replaces the following:

	First rule for ...
	Last rule for ...

With:

	First for ...
	Last for ...

Because for some reason, 6L38 does not like the word "rule" in these constructions.