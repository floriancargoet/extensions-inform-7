Version 2/181103 of Vorple Howler (for Glulx only) by Nathanael Marion begins here.

"Allows to use the howler.js JavaScript library in a Vorple project."

"with the howler.js library"

Include version 3 of Vorple by Juhana Leinonen.

Part - Initialisations

Vorple interface setup (this is the setup Howler rule):
	execute JavaScript command "window.howls = {};".

First when play begins (this is the reset Howler when play begins rule):
	execute JavaScript command "Howler.mute(false).volume(1).unload(); howls = {};".

Part - Global

Chapter - Properties

[TODO: Are the global properties useful? They seem to be a bit low-level.]

Chapter - Methods

To mute all sounds:
	execute JavaScript command "Howler.mute(true);".

To unmute all sounds:
	execute JavaScript command "Howler.mute(false);".

To set the global volume to (N - a number):
	execute JavaScript command "Howler.volume([N] / 100);".

To decide what number is the global volume:
	execute JavaScript command "return Howler.volume() * 100;";
	decide on the number returned by the JavaScript command.

[TODO: add the codecs method?]

To unload all sounds:
	execute JavaScript command "Howler.unload(); howls = {};".

Part - The Howler sound kind

A howler sound is a kind of object.

A howl state is a kind of value.
The howl states are howl-unloaded, howl-loading and howl-loaded.

Chapter - Properties

A howler sound has a text called source. [TODO: Allow to use a list to specify multiple extensions.]

A howler sound has a number called initial volume.
The initial volume of a howler sound is usually 100.

A howler sound can be HTML5.
A howler sound is seldom HTML5.

A howler sound can be looping.
A howler sound is usually not looping.

[TODO: Preload and autoplay property? I don't see the use of `preload`since we always preload Howler sounds. As for`autoplay`, it is handled automatically by the extension (false when preloading a howl sound, true when playing it while it's not yet initialised.]

A howler sound can be muted.
A howler sound is seldom muted.

[TODO: Sprites?]

A howler sound has a number called initial rate.
The initial rate of a howler sound is usually 100.

[TODO: Pool, format, xhrWithCredentials ?]

A howler sound has a text called onload command.
A howler sound has a text called onloaderror command.
A howler sound has a text called onplayerror command.
A howler sound has a text called onplay command.
A howler sound has a text called onend command.
A howler sound has a text called onpause command.
A howler sound has a text called onstop command.
A howler sound has a text called onmute command.
A howler sound has a text called onvolume command.
A howler sound has a text called onrate command.
A howler sound has a text called onseek command.
A howler sound has a text called onfade command.
A howler sound has a text called onunlock command.

Definition: a howler sound (called howl) is initialised:
	execute JavaScript command "return howls.hasOwnProperty('[identifier of howl]');";
	if the Javascript command returned true:
		yes;
	no.

[The _JSname is the Javascript identifier of the Howl sound. It starts empty but can be set manually if we want. However, it's not supposed to be used directly: one should use the `name of (howl sound)` phrase above to make sure the name is always generated before using.]
A howler sound has a text called identifier.
The identifier of a howler sound is usually "REPLACE_ME".

Part - Phrases

Chapter - Creating the JS Howl object - unindexed

To initialise/initialize (howl - a howler sound), with autoplay:
	let autoplay be whether or not with autoplay;
	if howl is not initialised:
		execute JavaScript command "
			howls.[identifier of howl] = new Howl({
				src: '[source of howl]',
				volume: [initial volume of howl] / 100,
				[if howl is HTML5]html5: true,[end if]
				[if howl is looping]loop: true,[end if]
				autoplay: [autoplay],
				[if howl is muted]muted: true,[end if]
				rate: [initial rate of howl] / 100,
				[if the onload command of howl is not empty]onload: function() {[onload command of howl]},[end if]
				onloaderror: function(id, err) {console.log(err); [onloaderror command of howl]},
				onplayerror: function(id, err) {console.log(err); [onplayerror command of howl]},
				[if the onplay command of howl is not empty]onplay: function(id) {[onplay command of howl]},[end if]
				[if the onend command of howl is not empty]onend: function(id) {[onend command of howl]},[end if]
				[if the onpause command of howl is not empty]onpause: function(id) {[onpause command of howl]},[end if]
				[if the onstop command of howl is not empty]onstop: function(id) {[onstop command of howl]},[end if]
				[if the onmute command of howl is not empty]onmute: function(id) {[onmute command of howl]},[end if]
				[if the onvolume command of howl is not empty]onvolume: function(id) {[onvolume command of howl]},[end if]
				[if the onrate command of howl is not empty]onrate: function(id) {[onrate command of howl]},[end if]
				[if the onseek command of howl is not empty]onseek: function(id) {[onseek command of howl]},[end if]
				[if the onfade command of howl is not empty]onfade: function(id) {[onfade command of howl]},[end if]
				[if the onunlock command of howl is not empty]onunlock: function(id) {[onunlock command of howl]},[end if]
			});
		".

Chapter - Controlling a Howler sound

To preload (howl - a howler sound):
	initialise howl;

To play (howl - a howler sound), leaving current playbacks:
	if howl is initialised:
		if leaving current playbacks:
			execute JavaScript command "howls.[identifier of howl].play();";
		else:
			execute JavaScript command "howls.[identifier of howl].stop().play();";
	else:
		initialise howl, with autoplay.

To pause (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].pause()"

To unpause (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].play();";

To stop (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].stop();";

To mute (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].mute(true);";
	else:
		now howl is muted.

To unmute (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].mute(false);";
	else:
		now howl is not muted.

To set the/-- volume of (howl - a howler sound) to (N - a number):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].volume([N] / 100);";
	else:
		now the initial volume of howl is N.

To fade (howl - howler sound) from (N1 - a number) to (N2 - a number) over/in (T - a number) milliseconds/ms:
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].fade([N1] / 100, [N2] / 100, [T]);";

To fade out (howl - a howler sound) over/in (T - a number) milliseconds/ms:
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].fade(howls.[identifier of howl].volume(), 0, [T]);";
	else:
		now the initial volume of howl is 0.

To fade in (howl - a howler sound) over/in (T - a number) milliseconds/ms:
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].fade(0, 1, [T]);";
	else:
		now the initial volume of howl is 100.

To fade (howl - a howler sound) to (N - a number) over/in (T - a number) milliseconds/ms:
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].fade(howls.[identifier of howl].volume(), [N] / 100, [T]);".

To set the/-- rate of (howl - a howler sound) to (N - a number):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].rate([N] / 100);";
	else:
		now the initial rate of howl is N.

To set the/-- position of (howl - a howler sound) to (N - a real number) second/seconds/s:
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].seek([N to 4 decimal places]);".

To loop (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].loop(true);";
	else:
		now howl is looping.

To unloop (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].loop(false);";
	else:
		now howl is not looping.

[TODO: Add the ability to manually load a Howl sound? I can't see when it could be useful, since the extension always preloads them.]

To unload (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "howls.[identifier of howl].unload(); delete howls.[identifier of howl];".

Chapter - Getting the state of a Howler sound

To decide what number is the volume of (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "return howls.[identifier of howl].volume() * 100;";
		decide on the number returned by the JavaScript command;
	else:
		decide on the initial volume of howl.

To decide what number is the rate of (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "return howls.[identifier of howl].rate() * 100;";
		decide on the number returned by the JavaScript command;
	else:
		decide on the initial rate of howl.

To decide what real number is the position of (howl - a howler sound):
	if howl is initialised:
		[Vorple doesn't support returned real numbers, so we multiply by 1000 in the JavaScript to get a integer, then divide by 1000.0 to get an Inform real number.]
		execute JavaScript command "return howls.[identifier of howl].seek() * 1000;";
		let N be the number returned by the JavaScript command;
		decide on N / 1000.0;
	else:
		decide on 0.0.

To decide whether (howl - a howler sound) loops:
	if howl is initialised:
		execute JavaScript command "return howls.[identifier of howl].loop();";
		decide on whether or not the JavaScript command returned true;
	else:
		if howl is looping, yes;
		no.

To decide what howl state is the state of (howl - a howler sound):
	if howl is initialised:
		execute JavaScript command "return howls.[identifier of howl].state();";
		let T be the text returned by the JavaScript command;
		if T is "loaded", decide on howl-loaded;
		if T is "loading", decide on howl-loading;
	decide on howl-unloaded.
	[TODO: We default to howl-unloaded. Should we add another howl state for when the Howler sound is not initialized (i.e. its JS object has not been created)?]

To decide whether (howl - a howler sound) is playing:
	if howl is initialised:
		execute JavaScript command "return howls.[identifier of howl].playing();";
		decide on whether or not the JavaScript command returned true;
	else:
		no.

To decide what real number is the duration of (howl - a howler sound):
	if howl is initialised:
		[Vorple doesn't support returned real numbers, so we multiply by 1000 in the JavaScript to get a integer, then divide by 1000.0 to get an Inform real number.]
		execute JavaScript command "return howls.[identifier of howl].duration() * 1000;";
		let N be the number returned by the JavaScript command;
		decide on N / 1000.0;
	else:
		decide on 0.0.

Chapter - Listening to new events

[TODO: Should we add the possibility to add event listeners during play (with Howl.on)? I don't think it's useful, since one could just use the default events on the Howler sound object.
And since the extension does not support controlling each sound from a same Howl object individually, one can't bind an event to a particular ID.]

Part - 3D sounds

[TODO]

Vorple Howler ends here.

---- DOCUMENTATION ----

Chapter: Introduction

Section: About

With this extension, we can use the howler.js JavaScript library within a Vorple project. This library allows for fine audio control and has many features. To have an idea of what it can do, you can go to the howler.js website:

	https://howlerjs.com/

If you want sounds in you story but the default Vorple Multimedia extension is not enough for what you want, then this extension is for you!

Note however that it only replicates the features of howler.js. It doesn't implement things like channels or playlists, which are possible to add with the features provided. (See the examples at the end!)

Section: Getting started

First of all, we'll need to download the howler.js library. Head to

	https://github.com/goldfire/howler.js/releases/latest

and click on "Source code" to download it, then extract the ZIP file. Search for "howler.js" in the "dist" folder and copy it to your project's materials folder. This is the only file we need.

We'll then have to add the following to our source:

	Release along with JavaScript "howler.js".

We can now add some sounds in the materials folder (MP3 is recommended, but other formats work as well), and we are good to play our first sound:

	Release along with the file "MyGreatSong.mp3".
	
	My great song is a howler sound.
	The source is "MyGreatSong.mp3".
	The identifier is "myGreatSong".
	
	When play begins:
		play my great song.

It's as simple as that. Continue reading to see all the extension has to offer.

Chapter: The howler sound kind

Section: Creating new sounds

Each sound is represented by a new kind of object, the howler sound. As seen above, all we need is to set its "source" property and its identifier.

The source is the name of the file. Don't forget to release along with that said file. The identifier is a text needed by the extension. It has to be only composed of non-accented latin letters, numbers and underscores, and should not start with a number. So the most straightforward is to use the name of the howler sound with spaces removed.

	Release along with the file "MyGreatSong.mp3".
	
	My great song is a howler sound.
	The source is "MyGreatSong.mp3".
	The identifier is "myGreatSong".

The following formats are supported: MP3, MPEG, OPUS, OGG, OGA, WAV, AAC, CAF, M4A, MP4, WEBA, WEBM, FLAC. However, not all browsers support all formats; MP3 and WEBM are recommended.

Section: Initial properties

In addition to the source, a howler sound has many properties that describe its initial state. Keep in mind that those are only used when the sound is loaded by the story (see next chapter). To change one of them during play, use the phrases (also next chapter).

	The initial volume of my great song is 70.

The initial volume is a number between 0 and 100 describing how loud will be the sound. (0 is silent, 100 is the default.)

	The initial rate of my great song is 150.

The initial rate is a real number determining the speed at which the sound will play. 100 is the normal speed, 200 is twice the normal speed, 50 half of it... It has to be between 50 and 400.

	My great song is looping.

The looping property will make the sound loop if set.

	My great song is muted.

The sound will start muted if the muted property is set. It can later be unmuted via a phrase.

Finally, howler sounds have text specifying JavaScript commands to be executed at certain events (when the sound starts playing or its volume is changed, for example). See the chapter "Advanced use" for more informations.

Chapter: Controlling a howler sound

Next we'll see how to control howler sounds during play. The effects of the following phrases will be immediate if the song is playing, otherwise they will happen the next time it is played, as noted.

Remember, during play, use these phrases instead of changing the initial properties of a howler sound! In fact, the phrases will never modify the initial properties, except when written otherwise.

With that said, let's start.

Section: Playing and loading

To play a sound, just use:

	play my great song;

If the specified sound was already playing, it will first be stopped, unless the following phrase option is used:

	play my great song, leaving current playbacks;

In that case, we can have multiple instances of the same sound playing at the same time.

Playing a sound will not be immediate: the browser will first have to download it, which can take some time for long musics and slower internet connections.

To suppress this delay, we can preload the sound, some time before playing it:

	When play begins:
		preload my great song.
	
	Instead of switching on the radio:
		play my great song.

We should preload sounds near the beginning of the story, or of a chapter where the sound will be used. However, preloading many sounds at the same time may be slow on some connections. (I haven't tested.) Also, it may not be useful to preload short sound effects, and it is useless to preload sounds just before playing them.

To know if a sound is currently playing, we can use the following condition:

	if my great song is playing:

When we know for certain a sound will never be used again during a playthrough, we can unload it to save some memory:

	unload my great song;

If a sound is played or preloaded again after being unloaded, it will just be loaded once more. It may cause the browser to download it one more time, so it could harm players with limited internet connection. Even if I'm not sure if it's the case, unload with caution.

Section: Pausing

	pause my great song;
	unpause my great song;

Pauses and resumes a howler sound. If a sound is not yet loaded, pausing or unpausing it does nothing.

Section: Stopping

	stop my great song;

The sound will immediatly stop, and start over when played again. Nothing is done if it was not playing to begin with.

Section: Muting and unmuting

	mute my great song;
	unmute my great song;

The first one mutes the sound. It will silently continue playing, until it is unmuted with the second one. If the sound hasn't been loaded yet, they will change the "muted" property of the howler sound so that they take effect when it is played.

Section: Changing and getting the volume

	set the volume of my great song to 50;

Changes the current volume of a howler sound. The number should be between 0 and 100. If the sound hasn't been loaded yet, the "initial volume" property will be modified instead.

	the volume of my great song

Gets the current volume of a howler sound. Beware, it can be different from the "initial volume" property if the previous phrase has been used during play!

Section: Fading

	fade my great song from 20 to 85 over 3500 ms;
	fade out my great song over 3500 ms;
	fade in my great song over 3500 ms;
	fade my great song to 25 over 3500 ms;

Fade a howler sound from one volume to another, over a certain duration. The first one will fade from and to the specified volumes. The second one will fade from the current volume of the sound to 0. The third one will fade from 0 to 100. The last will fade from the current volume of the sound to the specified one.

If the first volume is different from the current volume of the sound, the sound will jump to it instantly before fading to the second one.

As usual, the volume has to be between 0 and 100. The durations are in milliseconds: 1000 ms is one second, 500 ms is half of one, and so on.

The "fade out" phrase will set instantly the "initial volume" property of a sound to 0 if it hasn't been loaded yet. The "fade in" will do the same thing, but to 100.

Section: Changing the rate

	set the rate of my great song to 70;

Changes the speed at which the sound is playing. The number has to be between 50 and 400. If the sound hasn't been loaded yet, the "initial rate" property will be modified instead.

	the rate of my great song

Gets the current rate of a howler sound. Beware, it can be different from the "initial rate" property if the previous phrase has been used during play!

Section: Looping

	loop my great song;
	unloop my great song;

Make a howler sound loop or not. If the sound hasn't been loaded yet, the "looping" property will be modified instead.

	if my great song loops:

Determines if a howler sound is currently looping. Beware, it can be different from the "looping" property if the previous phrases have been used during play! So don't use "if my great song is looping", which will test the initial property.

Section: Changing and getting the current position

	set the position of my great song to 17 seconds;

Changes the current position of the song to the given time in seconds. It can be a real number, to get a fraction of second. It does nothing if the sound in not loaded.

	the position of my great song

Gets the current position of a song, in seconds. The result is a real number with 3 decimal places. Gives 0 if the song is not playing or is not loaded.

Section: Getting the duration

	the duration of my great song

Gets the duration of a howler sound, in second. As for the position, the result is a real number with 3 decimal places. Gives 0 if the sound is not playing or is not loaded.

Chapter: Global controls

It is possible to control all the sounds globally, to mute them all for example. This has the advantage to keep the individual state of each sound. If a howler sound was muted individually before we mute the story globally, it will stay muted when we unmute the story.

Section: Muting all sounds

	mute all sounds;
	unmute all sounds;

Mutes and unmutes the story globally. This keeps the muted state of each sound, as explained above.

Section: Changing and getting the global volume

	set the global volume to 50;

Changes the global volume to a number between 0 and 100 (as usual). The story begins at 100. Each howler sounds still keeps their relative volume to each other.

For example, if Sound 1 has a volume of 60, Sound 2 a volume of 24 and we change the global volume from 100 to 50, the *computed* volume of the sounds will be 30 and 12 respectively. But getting the individual volumes of each sound will still give 60 and 24, since only the global volume has actually changed.

We can also get the global volume with:

	global volume

Section: Unloading all sounds

	unload all sounds;

Does the same as unloading every sound one after the other, but faster. Not many stories would need it, however.

One thing to know is that all sound are unloaded when the player restarts a story. This may cause the browser to download them again during play, so it could harm players with limited internet connection.

Chapter: Advanced

This section covers advanced features of the extension, that will mainly be useful to people who can write JavaScript. If that's not your case, you should still read this chapter, since it shows how to do useful things even for those with little knowledge of JavaScript.

Section: The HTML5 property

A howler sound can be declared as "HTML5":

	My great song is an HTML5 howler sound.

This will cause the howler.js library to use HTML 5 Audio instead of the Web Audio API.

This is mainly useful for large file such as music. If a sound is HTML5, then it will begin to play without having us to wait for it to load entirely. It will thus start faster.

Some features may not work as expected with HTML 5 on some browser, but it is usually safe to consider that they will work.

Section: Getting the state of a howler sound

It is possible to determine if a sound is loading or has loaded with the following:

	state of my great song

The result is a value of kind "howl state", which can be one of the following:

	howl-unloaded
	howl-loading
	howl-loaded

A sound is unloaded if we have never played nor preloaded it, or after having unloaded it during play. A sound is loading when we start preloading it or playing it while not having preloaded it before. A sound is loaded when it is ready to play.

Usually, we don't need these informations since the extension and the library take care of everything for us.

Section: Listening to events

A howler sounds has mutiple properties holding texts that will be used as JavaScript commands on certain events.

Those properties are:

	onload command: when the sound is loaded.
	
	onloaderror command: when the sound fails to load.
	
	onplayerror command: when the sound is unable to play.
	
	onplay command: each time the sound starts playing; for a looping sound, it will fire at the beginning of each loop.
	
	onend command: each time the sound finishes playing; for a looping sound, it will fire at the end of each loop.
	
	onpause: each time the sound is paused with "pause (howler sound)".
	
	onstop command: each time the sound is stopped with "stop (howler sound)".
	
	onmute command: each time the sound is muted or unmuted.
	
	onvolume command: each time the volume of the sound is changed.
	
	onrate command: each time the rate of the sound is changed.
	
	onseek command: each time the position of the sound is changed.
	
	onfade command: each time the sound finishes fading.
	
	onunlock command: on some browsers, audio cannot play until the user has interacted with the page; this event fires when audio has been unlocked by the browser.

A common use would be to execute some Inform rule when an event happens:

	The onend command of my great song is "vorple.prompt.queueCommand('__songend', true)".
	
	After reading a command:
		if the player's command matches "__songend":
			say "My great song ended. So sad!";
			reject the player's command.

In this example, the command "__songend" is queued when the sound ends. We then catch this command in an "After reading a command" rule.

Section: Getting a JavaScript identifier for a sound

When writing JavaScript commands, use the "identifier" property of a howler sound to refer to it.

An example: we could use it for a JavaScript variable that counts the number of time a sound has played:

	execute JavaScript command "var [identifier of my great song]_counter = 0;";

Section: How are howler sounds stored in JavaScript

This section is for those who want to access the sounds via JavaScript. You can skip it if you don't want to do that.

When the page loads, a variable called "howls", whose value is an empty object, is created. When a howl sound is loaded, its corresponding Howl object is added to this variable with its identifier as the key.

So, to access a sound via JavaScript:

	To do something with (howl - a howler sound):
		if howl is initialised:
			execute JavaScript command "var x = howls.[identifier of howl];"

The example above shows one important thing: we have to check if a sound is initialised. That means it has been loaded and has been stored in the "howls" object, so that it can be accessed safely. The phrases defined in this extension already make this check, so we only need it when writing our own JavaScript directly.

When play begins, the story is unmuted, is volume is reset to 100 and all the sounds are unloaded. Moreover, when sounds are unloaded, their corresponding entries in the "howls" object are deleted.

Chapter: Additional informations

Section: About the howler.js library

You can learn more about the howler.js library and read its documentation here:

	https://howlerjs.com/

The library is released under the MIT licence, so I suppose you have to mention it in the credits of your story.

Section: Missing features

Not all howler.js features are implemented right now. Here is the list of what I may eventually add:

	- Multiple formats in the "source" property
	- 3D audio
	- Sprites
	- Controlling each instance of a same sound independently with its ID
	- Adding event listeners during play

And here are the ones that I don't think can be useful in an Inform project.

	- The global properties.
	- The "codecs" global method.
	- The "preload" and "autoplay" properties of a howler sound; those are handled automatically by the extension when playing or preloading a sound.
	- The Howl methods "pool", "format" and "xhrWithCredentials".

If you need one of them, contact me and I'll do my best to implement it!

Section: Contact

I can be contacted at:

	natrium729@gmail.com

If you have feature requests or found a bug, you can also use the but tracker at:

	https://bitbucket.org/Natrium729/extensions-inform-7/issues

Section: Changelog

Version 2/181103:

	Updated the extension for Vorple 3.1.

Version 1/180823:

	Initital release.

Example: * About the Examples - A small note concerning the examples below.

The sound files are not provided, so if you want to test the examples, you'll have to find some sounds and rename them so that they match their name in the examples.

The examples with 3 stars or more show how to do nice things, like repeating a sound a certain number of times, using the phrases provided by this extension and some JavaScript. Feel free to copy them as you wish.

Example: *** Stroke of Midnight - Having a clock chime a certain amount of time at each hour.

A new kind of Howler sound is created for sounds that are supposed to play multiple times in a row. We then specify the onplay command of repeating sounds to provide the JavaScript needed to make them repeat.

	*: "Stroke of Midnight"

	Include Vorple Howler by Nathanael Marion.
	Release along with the "Vorple" interpreter.

	Chapter 1 - The repeating sound kind

	A repeating sound is a kind of howler sound.
	The onplay command of a repeating sound is usually "
		[identifier of item described]_counter++;
		if ([identifier of item described]_counter == [identifier of item described]_target) {
			this.loop(false);
			[identifier of item described]_counter = 0;
		}
		".
	
	To play (howl - a repeating sound) repeating (N - a number) time/times:
		loop howl;
		execute JavaScript command "
			var [identifier of howl]_counter = 0;
			var [identifier of howl]_target = [N];
		";
		play howl.
	
	The chime is a repeating sound.
	The source is "chime.mp3".
	The identifier is "chime".
	
	Chapter 2 - The scenario
	
	The Hall is a room. "An enormous clock stands in front of you.".
	
	The clock is a thing in the Hall.
	The description is "It is [time of day].".
	
	Every turn when the minutes part of time of day is 59:
		let N be 1;
		if the hours part of time of day is greater than 11:
			let N be the hours part of time of day minus 11;
		else:
			let N be the hours part of time of day plus 1;
		play the chime repeating N times.
	
	Instead of attacking the clock:
		say "You hit the clock, which lets a single chime escape.";
		play the chime repeating 1 time.
	
	The time of day is 11:58 PM.
	
	Test me with "hit clock / wait / hit clock".

As you can see above, if we want to play a repeating sound a single time, we have to use the custom phrase (specifying that we want no repetition) and not the one provided by the extension. Another drawback is that playing a repeating sound when it is already playing will stop it first; we won't be able to have multiple independent sets of the chime sound playing.