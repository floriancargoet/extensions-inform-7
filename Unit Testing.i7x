Version 1/201015 of Unit Testing by Nathanael Marion begins here.

"A extension for authors who want to write unit tests. Requires Text Capture by Eric Eve."

"inspired and partly based on Simple Unit Tests by Dannii Willis"

[TODO:
- Assert for commands.
- Assert for relations?
- Assert for properties?
- Restore the state of the VM after each test?
]

Volume - Dependencies

Include Text Capture by Eric Eve.

Volume - Capturing output

[We use a custom I6 routine instead of a I7 regular expression to strip whitespace from a text because it's faster. The time gained is non-negligible when there are a lot of unit tests testing captured output.]

Include (-
[ StripWhitespace txt  len i s e new_txt;
	TEXT_TY_Transmute(txt);
	len = TEXT_TY_CharacterLength(txt);
	! Look for the first non-space character.
	for (i = 0 : i < len : i++) {
		if (~~(BlkValueRead(txt, i) == 10 or 13 or 32 or 9)) { ! not \n, \r, space or tab.
			s = i;
			break;
		}
	}
	! Look for the last non-space character.
	for (i = len - 1 : i >= 0 : i-- ) {
		if (~~(BlkValueRead(txt, i) == 10 or 13 or 32 or 9)) { ! not \n, \r, space or tab.
			e = i + 1;
			break;
		}
	}

	! At this point, if the initial text was empty, e and s are both 0, and
	! everything will still work as expected and we'll get an empty text at the end.

	! From this point, len will be the length of the text with the whitespace stripped.
	len = e - s;
	new_txt = BlkValueCreate(TEXT_TY);
	TEXT_TY_Transmute(new_txt);
	if (BlkValueSetLBCapacity(new_txt, len + 1)) {
		for (i = 0 : i < len : i++) {
			BlkValueWrite(new_txt, i, BlkValueRead(txt, i + s));
		}
		BlkValueWrite(new_txt, len, 0);
		BlkValueCopy(txt, new_txt);
	}
	BlkValueFree(new_txt);
];
-)

To strip whitespace from (T - a text):
	(- StripWhitespace({-lvalue-by-reference:T}); -)

To decide what text is the captured output:
	stop capturing text;
	let T be "[the captured text]";
	strip whitespace from T;
	decide on T.

Volume - Formatting kinds

[These phrases are used to change the output when an sayable value is displayed.]

[By default, a sayable value is just said.]
To say representation of (X - a sayable value):
	say "[X]".

[A text will have quotation marks around it.
TODO: Format the quotation marks inside the text?]
To say representation of (X - a text):
	say "'[X]'".

Volume - The counters - unindexed

[The number of assertions we made.]
The total assertion count is initially 0.

[The number of assertions that failed.]
The assertion failure count is initially 0.

Volume - The unit tests

Book - Regular unit tests

Part - The kind

A unit test is a kind of object.
The plural of unit test is unit tests.
A unit test has a text called description.

Part - The testing activity

Testing something is an activity on unit tests.

Book - Simple unit tests

Simple unit test is a rulebook.

Part - The simple unit test placeholder - unindexed

[Used as the value for "last unit test run" when running simple unit tests.]
The simple-unit-test-placeholder is a unit test.

Book - Running the unit tests

[It's an object that varies and not a unit test that varies so that it can have the value "nothing".]
The last unit test run is an object that varies.
The last unit test run is usually nothing.

Running all unit tests is an action out of world.
Understand  "run all/the/-- unit tests/test" and "unit tests/test" as running all unit tests.

Carry out running all unit tests:
	now the total assertion count is 0;
	now the assertion failure count is 0;
	start capturing text;
	[Run simple unit tests.]
	now the last unit test run is simple-unit-test-placeholder;
	follow the Simple unit test rules;
	[Run the regular unit tests.]
	repeat with UT running through unit tests:
		now the last unit test run is UT;
		carry out the testing activity with UT;
	now the last unit test run is nothing;
	stop capturing text;
	say "[line break]";
	if the total assertion count is 0:
		say "There are no assertions is this project!";
	else if the assertion failure count is 0:
		say "All [total assertion count] assertion[s] passed!";
	else:
		say "[assertion failure count] of [total assertion count] assertion[s] failed."

Volume - Asserting Phrases

Book - Making an assertion fail - unindexed

To make (name - a text) fail expecting (expected - a text) getting/receiving (received - a text):
	increment the assertion failure count;
	if the last unit test run is a unit test:
		say "[bold type]";
		if the last unit test run is the simple-unit-test-placeholder:
			say "Simple unit test rules";
		else if the description of the last unit test run is empty:
			say "[last unit test run]";
		else:
			say "[description of the last unit test run]";
		say "[roman type][line break]";
		now the last unit test run is nothing;
	say "FAILED: [name].";
	if expected is not "$nothing$":
		say "    Expected: [expected][line break]";
	if received is not "$nothing$":
		say "    Got: [received][line break]".

Book - A equals B

[Instead of

	assert "my assertion" that A equals B;

I could instead create the phrase

	assert "my assertion" that A is B;

But then Inform will complain if we are not comparing two values but checking if a value has a property. For example,

	assert "the box is open" that the box is open;

It will say that we can't compare a thing with an either/or property.]

To assert (name - a text) that (received - a value) equals (expected - a value):
	stop capturing text;
	increment the total assertion count;
	unless received is expected:
		make "[name]" fail expecting "[representation of expected]" getting "[representation of received]";
	start capturing text.

Book - A does not equal B

To assert (name - a text) that (received - a value) does not equal (unexpected - a value):
	stop capturing text;
	increment the total assertion count;
	unless received is not unexpected:
		make "[name]" fail expecting "anything but [representation of unexpected]" getting "[representation of received]";
	start capturing text.

Book - A is greater than B

To assert (name - a text) that (received - an arithmetic value) is greater than (min - an arithmetic value):
	stop capturing text;
	increment the total assertion count;
	unless received is greater than min:
		make "[name]" fail expecting "greater than [representation of min]" getting "[representation of received]";
	start capturing text.

Book - A is at least B

To assert (name - a text) that (received - an arithmetic value) is at least (min - an arithmetic value):
	stop capturing text;
	increment the total assertion count;
	unless received is at least min:
		make "[name]" fail expecting "at least [representation of min]" getting "[representation of received]";
	start capturing text.

Book - A is less than B

To assert (name - a text) that (received - an arithmetic value) is less than (max - an arithmetic value):
	stop capturing text;
	increment the total assertion count;
	unless received is less than max:
		make "[name]" fail expecting "less than [representation of max]" getting "[representation of received]";
	start capturing text.

Book - A is at most B

To assert (name - a text) that (received - an arithmetic value) is at most (max - an arithmetic value):
	stop capturing text;
	increment the total assertion count;
	unless received is at most max:
		make "[name]" fail expecting "at most [representation of max]" getting "[representation of received]";
	start capturing text.

Book - Text is empty

To assert (name - a text) that (received - a text) is empty:
	stop capturing text;
	increment the total assertion count;
	unless received is empty:
		make "[name]" fail expecting "an empty text" getting "[representation of received]";
	start capturing text.

Book - Text A matches text B

To assert (name - a text) that (received - a text) matches the text (find - a text):
	stop capturing text;
	increment the total assertion count;
	unless received matches the text find:
		make "[name]" fail expecting "a text containing [representation of find]" getting "[representation of received]";
	start capturing text.

Book - Text A exactly matches text B

To assert (name - a text) that (T1 - a text) exactly matches the text (T2 - a text):
	stop capturing text;
	increment the total assertion count;
	unless T1 exactly matches the text T2:
		make "[name]" fail expecting "exactly [representation of T2]" getting "[representation of T1]";
	start capturing text.

Book - Any condition

To assert (name - a text) that (C - a condition):
	(- AssertAny({name}, {C}); -).

Include (-
[ AssertAny name c;
	EndCapture();
	(+ the total assertion count +)++;
	if (~~(c)) {
		(+ the assertion failure count +)++;
		print "FAILED (arbitrary condition): ", (TEXT_TY_Say) name, "^";
	}
	StartCapture();
];
-).

Unit Testing ends here.

---- DOCUMENTATION ----

Chapter: Simple unit tests

The simplest way to make tests is to write some assertions in simple unit test rules.

	Simple unit test:
		assert "adding numbers" that 1 + 1 equals 2.

The text after "assert" is the name of the assertion. If the assertion fails, its name will be displayed along with additional information (what was received and what was expected). The available assertions are listed in a chapter below.

We can write our assertions in one gigantic rule, or break it into smaller rules. We can also name the rules for clarification, but the extension won't display the names; they will only act as comments for the author.

To run the tests, we should perform the running all unit tests action with the command "run all unit tests" (or "unit tests"). We can also try it directly in the source:

	When play begins: try running all unit tests.

Chapter: The unit tests kind

For more complicated unit tests, the unit test kind is available. The test associated to a unit test object is written in the testing activity.

	My-test is a unit test. "Arithmetic tests".

	For testing my-test:
		assert "adding numbers" that 1 + 1 equals 2.

Those tests will also be run by the running all unit tests action.

We can write a custom name in quotations marks after the sentence creating the unit test; it will be displayed in place of the bare name when the test fails.

Unit test objects become useful when a whole class of tests requires the same setup or cleanup.

	An artifact test is a kind of unit test.

	Before testing an artifact test:
		now the player carries the ancient artifact.

	After testing an artifact test:
		now the ancient artifact is nowhere.

	My-test is an artifact test.
	For testing my-test:
		assert ...

Failed assertions will also be grouped by unit test when displayed, whereas all simple unit test assertions will be under a single group. Unit tests objects can therefore help organise tests.

Chapter: Available assertions

The following assertions are available in unit tests:

	assert "equality" that the player equals Alice;
	assert "inequality" that the score does not equal 10;
	assert "greater than" that the score is greater than 0;
	assert "at least" that the score is at least 5;
	assert "less than" that the score is less than 10;
	assert "at most" that the score is at most 10;
	assert "empty text" that the description of the lamp is empty;
	assert "text matching" that the description of the player matches the text "good";
	assert "text exactly matching" that "[the lamp]" exactly matches the text "the lamp";

In the first and second phrases, it is important to use "equals" instead of "is" if we want to know whether a value is the same as another value. If we use "is" and the assertions fails, the actual and expected value won't be shown. We should use "is" in other cases, as when we want to know if an object has a particular property:

	assert "the lamp is switched on" that the lamp is switched on.

The assertions "is greater than", "is at least", "is less than" and "is at most" accept any arithmetic value (numbers, times, units...).

The two last assertions determine if texts are matching or exactly matching, as explained in the chapter "Advanced Text" of Inform's documentation.

Finally, we can test any other arbitrary condition, but we'll have less information if they fail:

	assert "the player is in the car" that the player is in the car;
	assert "the troll has the treasure" that the troll carries the gold;

... and so on.

Chapter: Testing the story's output

This extension uses Text Capture by Eric Eve to allow the author to test the game's output. We can use "[the captured output]" to have the captured text with the whitespace at its beginning and end stripped.

	Simple unit test:
		say "Hello!";
		assert "capturing text" that "[captured output]" exactly matches the text "Hello!".

Chapter: Planned features

More features that are planned (or wished), but not yet implemented:

	- Auto-undoing unit tests: tests that restore the state of the game after being run.
	- Command tests: tests that run a player's command and that can make assertions about the produced action name, noun or captured text.

Example: * Testing showcase - Showing success and failure for each kind of test.

	*: "Simple unit tests"

	Include Unit Testing by Nathanael Marion.

	Test room is a room.

	Alice is a woman.
	The box is a closed container.

	When play begins: try running the unit tests.

	Simple unit test (this is the equality testing rule):
		assert "equality succeeding 1" that 1 equals 1;
		assert "equality succeeding 2" that the player equals the player;
		assert "equality failing 1" that 1 equals 2;
		assert "equality failing 2" that the player equals Alice.

	Simple unit test (this is the inequality testing rule):
		assert "inequality succeeding 1" that 1 does not equal 2;
		assert "inequality succeeding 2" that the player does not equal Alice;
		assert "inequality failing 1" that 1 does not equal 1;
		assert "inequality failing 2" that the player does not equal the player.

	Simple unit test (this is the greater than testing rule):
		assert "greater than succeeding 1" that 2 is greater than 1;
		assert "greater than succeeding 2" that 12:00 PM is greater than 10:00 AM;
		assert "greater than failing 1" that 1 is greater than 2;
		assert "greater than failing 2" that 10:00 AM is greater than 12:00 PM.

	Simple unit test (this is the at least testing rule):
		assert "at least succeeding 1" that 2 is at least 1;
		assert "at least succeeding 2" that 2 is at least 2;
		assert "at least succeeding 3" that 12:00 PM is at least 10:00 AM;
		assert "at least failing 1" that 1 is at least 2;
		assert "at least failing 2" that 10:00 AM is at least 12:00 PM.

	Simple unit test (this is the less than testing rule):
		assert "less than succeeding 1" that 1 is less than 2;
		assert "less than succeeding 2" that 10:00 AM is less than 12:00 PM;
		assert "less than failing 1" that 2 is less than 1;
		assert "less than failing 2" that 12:00 PM is less than 10:00 AM.

	Simple unit test (this is the at most testing rule):
		assert "at most succeeding 1" that 1 is at most 2;
		assert "at most succeeding 2" that 2 is at most 2;
		assert "at most succeeding 3" that 10:00 AM is at most 12:00 PM;
		assert "at most failing 1" that 2 is at most 1;
		assert "at most failing 2" that 12:00 PM is at most 10:00 AM.

	Simple unit test (this is the empty text testing rule):
		assert "empty text succeeding" that "" is empty;
		assert "empty text failing" that "Hello World" is empty.

	Simple unit test (this is the matching text testing rule):
		assert "matching text succeeding" that "Hello World" matches the text "World";
		assert "matching text failing" that "Hello World" matches the text "Goodbye".

	Simple unit test (this is the exactly matching text testing rule):
		assert "exactly matching text succeeding" that "Hello World" exactly matches the text "Hello World";
		assert "exactly matching text failing" that "Hello World" exactly matches the text "Goodbye World".

	Simple unit test (this is the any testing rule):
		assert "any succeeding 1" that the player is in the test room;
		assert "any succeeding 2" that the box is closed;
		assert "any failing 1" that the player is in the box;
		assert "any failing 2" that the box is open.

	Simple unit test (this is the captured output testing rule):
		say "Hello World";
		assert "captured output succeeding" that "[captured output]" exactly matches the text "Hello World";
		say "Hello World";
		assert "captured output failing" that "[captured output]" exactly matches the text "Goodbye World".
