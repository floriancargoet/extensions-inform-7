Version 1/190105 of Switching Between English and French by Nathanael Marion begins here.

"Allows the language of play to switch between French and English at runtime. Requires that the game is written in French."

[TODO
- Brackets for the parser voice when in English, since the parser voice in French has them.
- Add brackets when changing the description length and requesting the pronoun meaning.]

Volume 1 - To determine which extension is used - Unindexed

Chapter - Rideable Vehicles

Section - Without Rideable Vehicles - Unindexed (for use without Rideable Vehicles by Graham Nelson)

To use the French Rideable Vehicles responses:
	do nothing.

To use the English Rideable Vehicles responses:
	do nothing.

Chapter - Locksmith

Section - Without Locksmith - Unindexed (for use without Locksmith by Emily Short)

To use the French Locksmith responses:
	do nothing.

To use the English Locksmith responses:
	do nothing.

Chapter - Basic Screen Effects

Section - Without Basic Screen Effects - Unindexed (for use without Basic Screen Effects by Emily Short)

To use the French Basic Screen Effects responses:
	do nothing.

To use the English Basic Screen Effects responses:
	do nothing.

Chapter - Inanimate Listeners

Section - Without Inanimate Listeners - Unindexed (for use without Inanimate Listeners by Emily Short)

To use the French Inanimate Listeners responses:
	do nothing.

To use the English Inanimate Listeners responses:
	do nothing.

Chapter - Skeleton Keys

Section - Without Skeleton Keys - Unindexed (for use without Skeleton Keys by Emily Short)

To use the French Skeleton Keys responses:
	do nothing.

To use the English Skeleton Keys responses:
	do nothing.

Chapter - Epistemology

Section - Without Epistemology - Unindexed (for use without Epistemology by Eric Eve)

To use the French Epistemology responses:
	do nothing.

To use the English Epistemology responses:
	do nothing.

Volume 1 - Basic phrases

The current language is a natural language that varies. The current language is the French Language.

To decide whether in French:
	if the current language is the French language:
		yes;
	no.

To decide whether in English:
	if the current language is the English language:
		yes;
	no.

To switch to French:
	now the current language is the French Language;
	use the French Standard Rules responses;
	use the French Rideable Vehicles responses;
	use the French Locksmith responses;
	use the French Basic Screen Effects responses;
	use the French Inanimate Listeners responses;
	use the French Skeleton Keys responses;
	use the French Epistemology responses;

To switch to English:
	now the current language is the English Language;
	use the English Standard Rules responses;
	use the English Rideable Vehicles responses;
	use the English Locksmith responses;
	use the English Basic Screen Effects responses;
	use the English Inanimate Listeners responses;
	use the English Skeleton Keys responses;
	use the English Epistemology responses;

Volume - Printed names

An object has a text called French printed name. The French printed name of an object is usually "*objet non traduit*".
An object has a text called English printed name. The English printed name of an object is usually "*untranslated object*".

The printed name of an object is usually "[if in French][French printed name][else][English printed name][end if]".

An object has a text called French printed plural name. The French printed plural name of an object is usually "*objets non traduits*".
An object has a text called English printed plural name. The English printed plural name of an object is usually "*untranslated objects*".

The printed plural name of an object is usually "[if in French][French printed plural name][else][English printed plural name][end if]".

Volume - Articles

[A thing can be custom-articled. A thing is seldom custom-articled.]
An object has a text called French indefinite article. The French indefinite article of an object is usually "[if plural-named]des[else if male]un[else]une[end if]".
An object has a text called English indefinite article. The English indefinite article of an object is usually "[if plural-named]some[else if the printed name of the item described starts with a vowel]an[else]a[end if]".

[To say an for (O - an object):
	if "[O]" matches the regular expression "^<aâàeéèêiîoôuûüæœh>":
		say "an";
	else:
		say "a".]

The indefinite article of an object is usually "[if in French][French indefinite article][else][English indefinite article][end if]".

[To say (something - object) avec article indéfini:
	(- print (a) {something}; -).
	
To say (something - object) avec Article indéfini:
	(- CIndefArt({something}); -).]

[To say (something - object) avec article défini:
	(- print (the) {something}; -).

To say (something - object) avec Article défini:
	(- print (The) {something}; -).]

To say the (O - an object):
	if in French:
		if O is proper-named:
			say "[O]";
		else if O is plural-named:
			say "les [O]";
		else if "[O]" starts with a vowel:
			say "l['][O]";
		else if O is male:
			say "le [O]";
		else:
			say "la [O]";
	else:
		say "[unless O is proper-named]the [end unless][O]".

To say The (O - an object):
	if in French:
		if O is proper-named:
			say "[O]";
		else if O is plural-named:
			say "Les [O]";
		else if "[O]" starts with a vowel:
			say "L['][O]";
		else if O is male:
			say "Le [O]";
		else:
			say "La [O]";
	else:
		say "[unless O is proper-named]The [end unless][O]".

[To say a/an (O - an object):
	say "[unless O is proper-named][indefinite article of O] [end if][O]".

To say A/An (O - an object):
	say "[unless O is proper-named][indefinite article of O] [end if][O]" in sentence case.]

Volume - Descriptions

A thing has a text called French description. The French description of a thing is usually "Rien de particulier concernant [the item described].".
An thing has a text called English description. The English description of a thing is usually "[We] [see] nothing special about [the item described].".

The description of a thing is usually "[if in French][French description][else][English description][end if]".
The description of the player is "[if in French][French description][else][English description][end if]".

A room has a text called French description. The French description of a room is usually "".
An room has a text called English description. The English description of a room is usually "".

The description of a room is usually "[if in French][French description][else][English description][end if]".

Volume - Numbers - Unindexed

Include (-
[ LanguageNumberEnglishWords n f;
    if (n == 0)    { print "zero"; rfalse; }
    if (n < 0)     { print "minus "; n = -n; }
#Iftrue (WORDSIZE == 4);
    if (n >= 1000000000) {
        if (f == 1) print ", ";
    	print (LanguageNumberEnglishWords) n/1000000000, " billion"; n = n%1000000000; f = 1;
    }
    if (n >= 1000000) {
        if (f == 1) print ", ";
    	print (LanguageNumberEnglishWords) n/1000000, " million"; n = n%1000000; f = 1;
    }
#Endif;
    if (n >= 1000) {
        if (f == 1) print ", ";
    	print (LanguageNumberEnglishWords) n/1000, " thousand"; n = n%1000; f = 1;
    }
    if (n >= 100)  {
        if (f == 1) print ", ";
        print (LanguageNumberEnglishWords) n/100, " hundred"; n = n%100; f = 1;
    }
    if (n == 0) rfalse;
    #Ifdef DIALECT_US;
    if (f == 1) print " ";
    #Ifnot;
    if (f == 1) print " and ";
    #Endif;
    switch (n) {
      1:    print "one";
      2:    print "two";
      3:    print "three";
      4:    print "four";
      5:    print "five";
      6:    print "six";
      7:    print "seven";
      8:    print "eight";
      9:    print "nine";
      10:   print "ten";
      11:   print "eleven";
      12:   print "twelve";
      13:   print "thirteen";
      14:   print "fourteen";
      15:   print "fifteen";
      16:   print "sixteen";
      17:   print "seventeen";
      18:   print "eighteen";
      19:   print "nineteen";
      20 to 99: switch (n/10) {
        2:  print "twenty";
        3:  print "thirty";
        4:  print "forty";
        5:  print "fifty";
        6:  print "sixty";
        7:  print "seventy";
        8:  print "eighty";
        9:  print "ninety";
        }
        if (n%10 ~= 0) print "-", (LanguageNumberEnglishWords) n%10;
    }
];
-)

To say (something - number) in French words
	(documented at phs_numwords):
	(- print (number) say__n=({something}); -).

To say (something - number) in English words
	(documented at phs_numwords):
	(- LanguageNumberEnglishWords({something}); -).

To say (N - a number) in words:
	if in French, say "[N in French words]";
	else say "[N in English words]".

Volume - Times - Unindexed

Book 1 - Printing Times

Include (-
[ PrintTimeOfDayEnglishDigital t h aop;
	if (t<0) { print "<no time>"; return; }
	if (t >= TWELVE_HOURS) { aop = "pm"; t = t - TWELVE_HOURS; } else aop = "am";
	h = t/ONE_HOUR; if (h==0) h=12;
	print h, ":";
	if (t%ONE_HOUR < 10) print "0"; print t%ONE_HOUR, " ", (string) aop;
];

[ PrintTimeOfDayEnglishWords t h m dir aop;
	h = (t/ONE_HOUR) % 12; m = t%ONE_HOUR; if (h==0) h=12;
	if (m==0) { LanguageNumberEnglishWords(h); print " o'clock"; return; }
	dir = "past";
	if (m > HALF_HOUR) { m = ONE_HOUR-m; h = (h+1)%12; if (h==0) h=12; dir = "to"; }
	switch(m) {
		QUARTER_HOUR: print "quarter"; HALF_HOUR: print "half";
		default: LanguageNumberEnglishWords(m);
		    if (m%5 ~= 0) {
				if (m == 1) print " minute"; else print " minutes";
		    }
	}
	print " ", (string) dir, " "; LanguageNumberEnglishWords(h);
];
-)

To say French time (T - a time):
	(- PrintTimeOfDay({T}); -).

To say English time (T - a time):
	(- PrintTimeOfDayEnglishDigital({T}); -).

To say (T - a time):
	if in French, say "[French time T]";
	else say "[English time T]".


To say (T - a time) in French words:
	(- print (PrintTimeOfDayEnglish) {T}; -).

To say (T - a time) in English words:
	(- PrintTimeOfDayEnglishWords({T}); -).

To say (T - a time) in words:
	if in French, say "[T in French words]";
	else say "[T in English words]".

Book 2 - Parsing Time

Chapter 2 - Parse time (in place of Chapter 2.2.7 - Times of day in French Language by Nathanael Marion)

[affiche le temps en format 24 h, comme "16 h 26"]
Include (-
[ PrintTimeOfDay t h aop;
	if (t<0) { print "<pas d'heure>"; return; }
	h = t/ONE_HOUR;
	print h, " h";
	if (t%ONE_HOUR ~= 0) print " ", t%ONE_HOUR;
];
-) instead of "Digital Printing" in "Time.i6t".

Include (-
[ PrintTimeOfDayEnglish t h m dir aop half;
	h = (t/ONE_HOUR); m = t%ONE_HOUR; dir = " ";
	if (m > HALF_HOUR) { h = h+1; if (h == 24) h = 0; m = ONE_HOUR-m; dir = " moins "; half = 1; }
	switch(h)
	{
		0: print "minuit";
		1: print "une heure";
		12: print "midi";
		21: print "vingt et une heures";
		default: print (number) h, " heures";
	}
	if (m > 0)
	{
		print (string) dir;
		switch(m)
		{
			1: print "une";
			21: print "vingt et une";
			QUARTER_HOUR: if (half == 0) print "et "; else print "le "; print "quart";
			HALF_HOUR: print "et demie";
			default: print (number) m;
		}
	}
];
-) instead of "Analogue Printing" in "Time.i6t".

Include (-
[ LanguageTimeOfDay hours mins;
    print hours/10, hours%10, " h ", mins/10, mins%10;
];
-) instead of "Time" in "Language.i6t".

Include (-
[ RELATIVE_TIME_TOKEN first_word second_word offhour mult mn original_wn;
	original_wn = wn;
	wn = original_wn;
	
	first_word = NextWordStopped(); wn--;
	if (first_word == 'an' or 'a//') mn=1; else mn=TryNumber(wn);
	
    if (mn == -1000) {
		first_word = NextWordStopped();
		if (first_word == 'half') offhour = HALF_HOUR;
		if (first_word == 'quarter') offhour = QUARTER_HOUR;
		if (offhour > 0) {
			second_word = NextWordStopped();
			if (second_word == 'of') second_word = NextWordStopped();
			if (second_word == 'an') second_word = NextWordStopped();
			if (second_word == 'hour') {
				parsed_number = offhour;
				return GPR_NUMBER;
			}
		}
		return GPR_FAIL;
    }
	wn++;
	
	first_word = NextWordStopped();
	switch (first_word) {
		'minutes', 'minute', 'min': mult = 1;
		'hour', 'hours', 'heures', 'heure', 'h//': mult = 60;
		default: return GPR_FAIL;
	}
	parsed_number = mn*mult;
	if (mult == 60) {
		mn=TryNumber(wn);
		if (mn ~= -1000) {
			wn++;
			first_word = NextWordStopped();
			if (first_word == 'minutes' or 'minute')
				parsed_number = parsed_number + mn;
			else wn = wn - 2;
		}
	}
	return GPR_NUMBER;
];
-) instead of "Relative Time Token" in "Time.i6t".

Volume - Pronoms

Part - Pronouns (in place of "Part 4.1 - Pronouns and possessives in commands" in French Language by Nathanael Marion)

Include (-
Array LanguagePronouns table

	! word        possible GNAs                   connected
	!             to follow:                      to:
	!             a     i
	!             s  p  s  p
	!             mfnmfnmfnmfn

	 ! Object pronouns
	  '-le'    $$100000100000                    NULL
	  '-la'    $$010000010000                    NULL
	  '-les'   $$000110000110                    NULL
	  '-lui'   $$110000110000                    NULL
	  '-leur'  $$000110000110                    NULL        ! tirets remis
!      'le'    $$100000100000                    NULL        ! car l'article le/la/les vient parfois interférer
!      'la'    $$010000010000                    NULL        ! par exemple "mange la pomme" est compris comme "mange-la" si "pomme" est inconnu, d'où des messages d'erreur troublants pour le joueur
!      'les'   $$000110000110                    NULL
!      'lui'   $$110000110000                    NULL ! dans "donne-lui", "lui" est m ou f
!      'leur'  $$000110000110                    NULL
	  
	  ! Disjunctive pronouns
!*! ! féminin accepté pour 'luy' (mot bidon) pour traiter les cas 'dedans', 'dessus', 'l^'... (genre inconnu)
!   en fait, '-lui' pourrait jouer le même rôle
	  'luy'    $$110000110000                    NULL ! "l'ouvrir" devient "ouvrir luy", "luy" étant m ou f
	  'lui'    $$100000100000                    NULL
	  'elle'   $$010000010000                    NULL
	  'eux'    $$000110000110                    NULL
	  'elles'  $$000010000010                    NULL

	'it'	$$001000111000	NULL
	'him'	$$100000000000	NULL
	'her'	$$010000000000	NULL
	'them'	$$000111000111	NULL;
-) instead of "Pronouns" in "Language.i6t".

Include (-
Array LanguageDescriptors table

	! word        possible GNAs   descriptor      connected
	!             to follow:      type:           to:
	!             a     i
	!             s  p  s  p
	!             mfnmfnmfnmfn                 

	'mon'    $$100000100000    POSSESS_PK      0  !*! ce qui suit ne doit pas fonctionner souvent
	  'ma'     $$010000010000    POSSESS_PK      0  !*! du moins je l'espère car mon/ma/mes devrait
	  'mes'    $$000110000110    POSSESS_PK      0  !*! changer en fonction du type de parole
	  'ton'    $$100000100000    POSSESS_PK      0  !*!
	  'ta'     $$010000010000    POSSESS_PK      0  !*! (quoique ça a l'air tres tolerant)
	  'tes'    $$000110000110    POSSESS_PK      0  !*!
	  'notre'  $$110000110000    POSSESS_PK      0  !*!
	  'nos'    $$000110000110    POSSESS_PK      0  !*!
	  'votre'  $$110000110000    POSSESS_PK      0  !*!
	  'vos'    $$000110000110    POSSESS_PK      0  !*!
	  'son'    $$100000100000    POSSESS_PK      '-lui'        ! tirets remis
	  'sa'     $$010000010000    POSSESS_PK      '-lui'
	  'ses'    $$000110000110    POSSESS_PK      '-lui'
	  'leur'   $$110000110000    POSSESS_PK      '-les'
	  'leurs'  $$000110000110    POSSESS_PK      '-les'
!      'son'    $$100000100000    POSSESS_PK      'lui' 
!      'sa'     $$010000010000    POSSESS_PK      'lui'
!      'ses'    $$000110000110    POSSESS_PK      'lui'
!      'leur'   $$110000110000    POSSESS_PK      'les'
!      'leurs'  $$000110000110    POSSESS_PK      'les' 

	  'le'     $$100000100000    DEFART_PK       NULL
	  'la'     $$010000010000    DEFART_PK       NULL
	  'l^'     $$110000110000    DEFART_PK       NULL
	  'les'    $$000110000110    DEFART_PK       NULL
	  'un'     $$100000100000    INDEFART_PK     NULL
	  'une'    $$010000010000    INDEFART_PK     NULL
	  'des'    $$000110000110    INDEFART_PK     NULL

	 'allumé'  $$100000100000    light           NULL
	 'allumée' $$010000010000    light           NULL
	 'éteint'  $$100000100000    (-light)        NULL
	 'éteinte' $$010000010000    (-light)        NULL

    'my'      $$111111111111    POSSESS_PK      0
    'this'    $$111111111111    POSSESS_PK      0
    'these'   $$000111000111    POSSESS_PK      0
    'that'    $$111111111111    POSSESS_PK      1
    'those'   $$000111000111    POSSESS_PK      1
    'his'     $$111111111111    POSSESS_PK      'him'
    'her'     $$111111111111    POSSESS_PK      'her'
    'their'   $$111111111111    POSSESS_PK      'them'
    'its'     $$111111111111    POSSESS_PK      'it'
    'the'     $$111111111111    DEFART_PK       NULL
    'a//'     $$111000111000    INDEFART_PK     NULL
    'an'      $$111000111000    INDEFART_PK     NULL
    'some'    $$000111000111    INDEFART_PK     NULL
    'lit'     $$111111111111    light           NULL
    'lighted' $$111111111111    light           NULL
    'unlit'   $$111111111111    (-light)        NULL;
-) instead of "Descriptors" in "Language.i6t".

Volume - Command parser internals

Part 1 - Command parser internals (in place of Part 4.6 - Command parser internals in French Language by Nathanael Marion)

Include (-
! Cette routine est utilisée par PrintCommand, qui affiche la commande tapée par le joueur  ("Je n'ai compris que : XXX")
! TODO : Trouver un moyen d'ajouter des mots depuis I7 ? (Par exemple avec un tableau.) Ce n'est pas très très grave sinon.
[ LanguageVerb i;
	switch (i)
	{
		'i//','inv','inventaire': print "inventaire";
		'r//': print "regarder";
		'x//': print "examiner";
		'z//': print "attendre";
		'v//': print "regarder";
		'a//': print "attendre";
		'!//': print "dire";
		'?//': print "demander";
		'q//': print "quitter";
		'pr': print "prendre";
		
		! Les mots du dictionnaire d'Inform n'ont que 9 lettres au maximum. Il faut donc indiquer comment afficher un verbe à 10 lettres ou plus.
		! (TODO : valable aussi pour Glulx ? Il me semble qu'on peut augmenter cette limite en tout cas.)
		'verrouiller': print "verrouiller";
		'deverrouiller': print "déverrouiller";
		'introduire': print "introduire";
		'transferer': print "transférer";
		'abandonner': print "abandonner";
		'farfouiller': print "farfouiller";
		'depoussierer': print "dépoussierer";
		'questionner': print "questionner";
		'interroger': print "interroger";

		! On n'écrit pas les accents dans un verbe pour plus de facilité pour le joueur (cf. enleve_accents).
		! Il faut donc réécrire les accents pour les verbes qui en ont besoin.

		'inserer': print "insérer";
		'transferer': print "transférer";
		'decoller': print "décoller";
		'lacher': print "lâcher";
		'oter': print "ôter";
		'revetir': print "revêtir";
		'vetir':	 print "vêtir";
		'deguiser': print "déguiser";
		'devorer': print "dévorer";
		'bruler': print "brûler";
		'detruire': print "détruire";
		'ecraser': print "écraser";
		'elaguer': print "élaguer";
		'decrire': print "décrire";
		'ecouter': print "écouter";
		'gouter': print "goûter";
		'tater': print "tâter";
		'trainer': print "traîner";
		'deplacer': print "déplacer";
		'regler': print "régler";
		'devisser': print "dévisser";
		'eteindre': print "éteindre";
		'arreter': print "arrêter";
		'demarrer': print "démarrer";
		'recurer': print "récurer";
		'repondre': print "répondre";
		'reveiller': print "réveiller";
		'eveiller': print "éveiller";
		'etreindre': print "étreindre";
		'reflechir': print "réfléchir";
		
		! Y'a pas que les verbes qui sont accentués !
		'a':	 print "à";
		'derriere': print "derrière";
		'cle': print "clé";
		
		default: rfalse;
	}
	rtrue;
];

[ LanguageVerbLikesAdverb w;
	if (w == 'look' or 'go' or 'push' or 'walk') rtrue;
	rfalse;
];

[ LanguageVerbMayBeName w;
	if (w == 'long' or 'short' or 'normal' or 'brief' or 'full' or 'verbose') rtrue;
	rfalse;
];
-) instead of "Commands" in "Language.i6t".

Include (-
Constant AGAIN1__WD     = 'again';
Constant AGAIN2__WD     = 'encore';
Constant AGAIN3__WD     = 'g//';
Constant OOPS1__WD      = 'oops';
Constant OOPS2__WD      = 'oops'; ! 'o//';
Constant OOPS3__WD      = 'euh';
Constant UNDO1__WD      = 'undo';
Constant UNDO2__WD      = 'annule';
Constant UNDO3__WD      = 'annuler';

Constant ALL1__WD     = 'tous';
Constant ALL2__WD     = 'toutes';
Constant ALL3__WD     = 'tout';
Constant ALL4__WD     = 'all';
Constant ALL5__WD     = 'everything';
Constant AND1__WD     = 'and';
Constant AND2__WD     = 'et';
Constant AND3__WD     = 'et';
Constant BUT1__WD     = 'but';
Constant BUT2__WD     = 'excepte';
Constant BUT3__WD     = 'sauf';
Constant ME1__WD      = 'moi';
Constant ME2__WD      = 'me';
Constant ME3__WD      = 'vous';
Constant OF1__WD      = 'de';
Constant OF2__WD      = 'du';
Constant OF3__WD      = 'of';
Constant OF4__WD      = 'of';
Constant OTHER1__WD   = 'another';
Constant OTHER2__WD   = 'other';
Constant OTHER3__WD   = 'autre';
Constant THEN1__WD    = 'then';
Constant THEN2__WD    = 'puis';
Constant THEN3__WD    = 'ensuite';

Constant NO1__WD      = 'n//';
Constant NO2__WD      = 'no';
Constant NO3__WD      = 'non';
Constant YES1__WD     = 'o//';
Constant YES2__WD     = 'y//';
Constant YES3__WD     = 'oui';

Constant AMUSING__WD  = 'amusing';
Constant FULLSCORE1__WD = 'fullscore';
Constant FULLSCORE2__WD = 'full';
Constant QUIT1__WD    = 'quit'; !*! "q//" ?
Constant QUIT2__WD    = 'quitter';
Constant RESTART__WD  = 'recommencer';
Constant RESTORE__WD  = 'charger';
-) instead of "Vocabulary" in "Language.i6t".

Volume 1 - Responses (in place of Volume 3 - Responses in French Language by Nathanael Marion)

Part 1.1 - Responses

Chapter 1.1.1 - In the Standard Rules

Section 1.1.1.1 - In French

To use the French Standard Rules responses:
	now the block vaguely going rule response (A) is "[bracket]Vous devez indiquez dans quelle direction vous voulez aller.[close bracket]";
	now the print the final prompt rule response (A) is "> [run paragraph on]";
	now the print the final question rule response (A) is "Voulez-vous ";
	now the print the final question rule response (B) is " ou ";
	now the standard respond to final question rule response (A) is "Faites un choix parmi les propositions ci-dessus.";
	now the you-can-also-see rule response (A) is "[Tu] ";
	now the you-can-also-see rule response (B) is "Sur [the domain], [tu] ";
	now the you-can-also-see rule response (C) is "Dans [the domain], [tu] ";
	now the you-can-also-see rule response (D) is "[regarding the player][adapt the verb peux for bg] aussi voir ";
	now the you-can-also-see rule response (E) is "[regarding the player][adapt the verb peux for bg] voir ";
	now the you-can-also-see rule response (F) is "";
	now the use initial appearance in room descriptions rule response (A) is "Sur [the item], ";
	now the describe what's on scenery supporters in room descriptions rule response (A) is "Sur [the item], ";
	now the print empty inventory rule response (A) is "[Tu] n['][adapt the verb as for bg] rien.";
	now the print standard inventory rule response (A) is "[Tu-j'][adapt the verb as for bg][_]:[line break]";
	now the report other people taking inventory rule response (A) is "[The actor] [fais] l'inventaire de [if the actor is plural-named]leurs[else]ses[end if] possessions.";
	now the can't take yourself rule response (A) is "[Tu] [te-se] [adapt the verb possèdes for bg] [toi]-même[if the player is plural-named]s[end if]. Voilà une problématique sur laquelle les philosophes n['][adapt the verb as from the third person plural for bg] pas dû passer beaucoup de temps.";
	now the can't take other people rule response (A) is "[regarding the noun][Il] n['][if the story tense is the present tense and the noun is singular-named]apprécierait sûrement pas[else if the story tense is the present tense and the noun is plural-named]apprécieraient sûrement pas[else if (the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense) and the noun is singular-named]aurait sûrement pas apprécié[else if (the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense) and the noun is plural-named]auraient sûrement pas apprécié[else if the story tense is the future tense and the noun is singular-named]appréciera sûrement pas[else if the story tense is the future tense and the noun is plural-named]apprécieront sûrement pas[end if] cela..";
	now the can't take component parts rule response (A) is "Cela [adapt the verb sembles for bg] faire partie [du whole].";
	now the can't take people's possessions rule response (A) is "Cela [adapt the verb sembles for bg] appartenir [au owner].";
	now the can't take items out of play rule response (A) is "[The noun] [negate the verb es for bg] [disponible].";
	now the can't take what you're inside rule response (A) is "[Tu] [adapt the verb dois for bg] d'abord [if noun is a supporter]descendre[otherwise]sortir[end if] [du noun].";
	now the can't take what's already taken rule response (A) is "[Tu] [regarding the noun][l'][regarding the player][adapt the verb as for bg] déjà.";
	now the can't take scenery rule response (A) is "[C'est-c'était] trop difficile à transporter";
	now the can only take things rule response (A) is "[Tu] [negate the verb peux for bg] transporter cela..";
	now the can't take what's fixed in place rule response (A) is "[C'est-c'était] fixé sur place.";
	now the use player's holdall to avoid exceeding carrying capacity rule response (A) is "(mettant [the transferred item] dans [the current working sack] pour faire de la place)[command clarification break]";
	now the can't exceed carrying capacity rule response (A) is "[Tu] [adapt the verb transportes for bg] déjà trop d'objets.";
	now the standard report taking rule response (A) is "Voilà qui [es] fait.";
	now the standard report taking rule response (B) is "[The actor] [ramasses] [the noun].";
	now the can't remove what's not inside rule response (A) is "[regarding the noun][Il] [negate the verb es for bg] [ici] [maintenant].";
	now the can't remove from people rule response (A) is "Cela [adapt the verb sembles for bg] appartenir [au owner].";
	now the can't drop yourself rule response (A) is "[Tu] [adapt the verb manques for bg] de dextérité pour cela.";
	now the can't drop body parts rule response (A) is "[Tu] [negate the verb peux for bg] poser une partie [de toi]-même[if the player is plural-named]s[end if].";
	now the can't drop what's already dropped rule response (A) is "[The noun] [adapt the verb es for bg] déjà [ici].";
	now the can't drop what's not held rule response (A) is "[Tu] [negate the verb as for bg] cela.";
	now the can't drop clothes being worn rule response (A) is "(enlevant d'abord [the noun])[command clarification break]";
	now the can't drop if this exceeds carrying capacity rule response (A) is "Il n'y [adapt the verb as for bg] plus de place sur [the receptacle].";
	now the can't drop if this exceeds carrying capacity rule response (B) is "Il n'y [adapt the verb as for bg] plus de place dans [the receptacle].";
	now the standard report dropping rule response (A) is "Voilà qui [es] fait.";
	now the standard report dropping rule response (B) is "[The actor] [poses] [the noun].";
	now the can't put something on itself rule response (A) is "[Tu] [negate the verb peux for bg] poser un objet sur lui-même.";
	now the can't put onto what's not a supporter rule response (A) is "Poser des objets sur [the second noun] ne [regarding nothing][adapt the verb mènes for bg] à rien.";
	now the can't put clothes being worn rule response (A) is "(enlevant d'abord [the noun])[command clarification break]";
	now the can't put if this exceeds carrying capacity rule response (A) is "Il n'y [adapt the verb as for bg] plus assez de place sur [the second noun].";
	now the concise report putting rule response (A) is "[C'est] fait.";
	now the standard report putting rule response (A) is "[if the actor is the player][Tu][else][The actor][end if] [poses] [the noun] sur [the second noun].";
	now the can't insert something into itself rule response (A) is "[Tu] [negate the verb peux for bg] mettre un objet à l'intérieur de lui-même.";
	now the can't insert into closed containers rule response (A) is "[The second noun] [adapt the verb es for bg] [fermé].";
	now the can't insert into what's not a container rule response (A) is "[The second noun] [negate the verb peux for bg] contenir d'objet.";
	now the can't insert clothes being worn rule response (A) is "(enlevant d'abord [the noun])[command clarification break]";
	now the can't insert if this exceeds carrying capacity rule response (A) is "Il n'y [adapt the verb as for bg] plus de place dans [the second noun].";
	now the concise report inserting rule response (A) is "[C'est] fait.";
	now the standard report inserting rule response (A) is "[if the actor is the player and the story tense is simple][Tu] [else if the actor is the player and the story tense is composé][Tu-j'][else][The actor] [end if][mets] [the noun] dans [the second noun].";
	now the can't eat unless edible rule response (A) is "Cela [adapt the verb es for bg] non comestible, selon toute évidence.";
	now the can't eat clothing without removing it first rule response (A) is "(enlevant d'abord [the noun])[command clarification break]";
	now the can't eat other people's food rule response (A) is "[The owner] n['][if the story tense is the present tense and the owner is singular-named]apprécierait sûrement pas[else if the story tense is the present tense and the owner is plural-named]apprécieraient sûrement pas[else if (the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense) and the owner is singular-named]aurait sûrement pas apprécié[else if (the story tense is the past historic tense or the story tense is the perfect tense) and the owner is singular-named]aurait sûrement pas apprécié[else if the story tense is the future tense and the owner is singular-named]appréciera sûrement pas[else if the story tense is the future tense and the owner is plural-named]apprécieront sûrement pas[end if] cela.";
	now the standard report eating rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][manges] [the noun]. Pas mauvais.";
	now the standard report eating rule response (B) is "[The actor] [manges] [the noun].";
	now the stand up before going rule response (A) is "(descendant d'abord [du chaise])[command clarification break]";
	now the can't travel in what's not a vehicle rule response (A) is "[Tu] [adapt the verb dois for bg] d'abord descendre [du nonvehicle].";
	now the can't travel in what's not a vehicle rule response (B) is "[Tu] [adapt the verb dois for bg] d'abord sortir [du nonvehicle].";
	now the can't go through undescribed doors rule response (A) is "[Tu] [negate the verb peux for bg] aller par là.";
	now the can't go through closed doors rule response (A) is "(ouvrant d'abord [the door gone through])[command clarification break]";
	now the can't go that way rule response (A) is "[Tu] [negate the verb peux for bg] aller par là.";
	now the can't go that way rule response (B) is "[Tu] [negate the verb peux for bg], puisque [the door gone through] ne [adapt the verb mènes for bg] nulle part.";
	now the describe room gone into rule response (A) is "[The actor] [vas] en haut";
	now the describe room gone into rule response (B) is "[The actor] [vas] en bas";
	now the describe room gone into rule response (C) is "[The actor] [vas] [if the noun is dedans]à l'intérieur[else if the noun is dehors]à l'extérieur[else][au noun][end if]";
	now the describe room gone into rule response (D) is "[The actor] [arrives] d'en haut";
	now the describe room gone into rule response (E) is "[The actor] [arrives] d'en bas";
	now the describe room gone into rule response (F) is "[The actor] [arrives] [if the back way is dedans]de l'intérieur[else if the back way is dehors]de l'extérieur[else][du back way][end if]";
	now the describe room gone into rule response (G) is "[The actor] [arrives]";
	now the describe room gone into rule response (H) is "[The actor] [arrives] [au room gone to] d'en haut";
	now the describe room gone into rule response (I) is "[The actor] [arrives] [au room gone to] d'en bas";
	now the describe room gone into rule response (J) is "[The actor] [arrives] [au room gone to] [if the back way is dedans]de l'intérieur[else if the back way is dehors]de l'extérieur[else][du back way][end if]";
	now the describe room gone into rule response (K) is "[The actor] [empruntes] [the noun]";
	now the describe room gone into rule response (L) is "[The actor] [arrives] [du noun]";
	now the describe room gone into rule response (M) is "sur [the vehicle gone by]";
	now the describe room gone into rule response (N) is "dans [the vehicle gone by]";
	now the describe room gone into rule response (O) is ", en poussant [the thing gone with], [t']emmenant";
	now the describe room gone into rule response (P) is ", en poussant [the thing gone with]";
	now the describe room gone into rule response (Q) is ", en poussant [the thing gone with]";
	now the describe room gone into rule response (R) is ", en poussant [the thing gone with] à l'intérieur";
	now the describe room gone into rule response (S) is ", [t']emmenant";
	now the can't enter what's already entered rule response (A) is "Impossible, [tu es-étais] déjà sur [the noun].";
	now the can't enter what's already entered rule response (B) is "Impossible, [tu es-étais] déjà dans [the noun].";
	now the can't enter closed containers rule response (A) is "[Tu] [negate the verb peux for bg] entrer dans [the noun], qui [adapt the verb es for bg] [fermé].";
	now the can't enter if this exceeds carrying capacity rule response (A) is "Il n'y [adapt the verb as for bg] plus de place sur [the noun].";
	now the can't enter if this exceeds carrying capacity rule response (B) is "Il n'y [adapt the verb as for bg] plus de place dans [the noun].";
	now the can't enter something carried rule response (A) is "[Tu] [negate the verb peux for bg] y entrer si [the noun] [negate the verb es for bg] au sol.";
	now the implicitly pass through other barriers rule response (A) is "(descendant [du current home])[command clarification break]";
	now the implicitly pass through other barriers rule response (B) is "(sortant [du current home])[command clarification break]";
	now the implicitly pass through other barriers rule response (C) is "(montant sur [the target])[command clarification break]";
	now the implicitly pass through other barriers rule response (D) is "(entrant dans [the target])[command clarification break]";
	now the implicitly pass through other barriers rule response (E) is "(entrant dans [the target])[command clarification break]";
	now the standard report entering rule response (A) is "[if the story tense is simple][Tu] [montes][else if the story tense is the perfect tense][Tu] [adapt the verb es in present tense] [monté][else if the story tense is the past perfect tense][Tu-j'][adapt the verb es in past tense] [monté][end if] sur [the noun].";
	now the standard report entering rule response (B) is "[if the story tense is simple][Tu-j'][entres][else if the story tense is the perfect tense][Tu] [adapt the verb es in present tense] [entré][else if the story tense is the past perfect tense][Tu-j'][adapt the verb es in past tense] [entré][end if] dans [the noun].";
	now the standard report entering rule response (C) is "[The actor] [if the story tense is simple][entres][else if the story tense is the perfect tense][adapt the verb es in present tense] [entré][else if the story tense is the past perfect tense][adapt the verb es in past tense] [entré][end if] dans [the noun].";
	now the standard report entering rule response (D) is "[The actor] [if the story tense is simple][montes][else if the story tense is the perfect tense][adapt the verb es in present tense] [monté][else if the story tense is the past perfect tense][adapt the verb es in past tense] [monté][end if] sur [the noun].";
	now the can't exit when not inside anything rule response (A) is "[Tu] [negate the verb peux for bg] sortir.";
	now the can't exit closed containers rule response (A) is "[Tu] [negate the verb peux for bg] sortir [du cage], qui [adapt the verb es for bg] [fermé].";
	now the standard report exiting rule response (A) is "[if the story tense is simple][Tu] [descends][else if the story tense is the perfect tense][Tu] [adapt the verb es in present tense] [descendu][else if the story tense is the past perfect tense][Tu-j'][adapt the verb es in past tense] [descendu][end if] [du container exited from].";
	now the standard report exiting rule response (B) is "[if the story tense is simple][Tu] [sors][else if the story tense is the perfect tense][Tu] [adapt the verb es in present tense] [sorti][else if the story tense is the past perfect tense][Tu-j'][adapt the verb es in past tense] [sorti][end if] [du container exited from].";
	now the standard report exiting rule response (C) is "[The actor] [if the story tense is simple][sors][else if the story tense is the perfect tense][adapt the verb es in present tense] [sorti][else if the story tense is the past perfect tense][adapt the verb es in past tense] [sorti][end if] [du container exited from].";
	now the can't get off things rule response (A) is "[Tu] [negate the verb es for bg] sur [the noun].";
	now the standard report getting off rule response (A) is "[if the actor is the player and the story tense is simple][Tu] [descends][else if the actor is the player and the story tense is the perfect tense][Tu] [adapt the verb es in present tense] [descendu][else if the actor is the player and the story tense is the past perfect tense][Tu-j'][adapt the verb es in past tense] [descendu][else if the actor is not the player and the story tense is simple][The actor] [descends][else if the actor is not the player and the story tense is the perfect tense][The actor] [adapt the verb es in present tense] [descendu][else if the actor is not the player and the story tense is the past perfect tense][The actor][adapt the verb es in past tense] [descendu][end if] [du noun].";
	now the room description heading rule response (A) is "L'obscurité";
	now the room description heading rule response (B) is " (sur [the intermediate level])";
	now the room description heading rule response (C) is " (dans [the intermediate level])";
	now the room description body text rule response (A) is "Il [adapt the verb fais for bg] noir, et [tu] ne [adapt the verb peux for bg] rien voir.";
	now the other people looking rule response (A) is "[The actor] [regardes] autour [if the actor is male]de lui[else]d'elle[end if].";
	now the examine directions rule response (A) is "En dépit de [tes] efforts, [tu] [if the story tense is simple]ne [vois] rien[else if the story tense is the perfect tense]n['][adapt the verb as in present tense] rien vu[else if the story tense is the past perfect tense]n['][adapt the verb as in past tense] rien vu[end if] de particulier dans cette direction.";
	now the examine devices rule response (A) is "[The noun] [adapt the verb es for bg] [if story tense is present tense]actuellement [end if][if the noun is switched on][allumé][otherwise][éteint][end if].";
	now the examine undescribed things rule response (A) is "Rien de particulier concernant [the noun].";
	now the report other people examining rule response (A) is "[The actor] [examines] [the noun] attentivement.";
	now the standard looking under rule response (A) is "Ces recherches [adapt the verb es from the third person plural] vaines.";
	now the report other people looking under rule response (A) is "[The actor] [regardes] sous [the noun].";
	now the can't search unless container or supporter rule response (A) is "[Tu] [if the story tense is simple]ne [trouves] rien[else if the story tense is the perfect tense]n['][adapt the verb as in present tense] rien trouvé[else if the story tense is the past perfect tense]n['][adapt the verb as in past tense] rien trouvé[end if] d'intéressant.";
	now the can't search closed opaque containers rule response (A) is "[Tu] [negate the verb peux for bg] voir à l'intérieur [du noun], puisqu['][il] [adapt the verb es for bg] [fermé].";
	now the report other people searching rule response (A) is "[The actor] [fouilles] [the noun].";
	now the block consulting rule response (A) is "[Tu] [if the story tense is simple]ne [trouves] rien[else if the story tense is the perfect tense]n['][adapt the verb as in present tense] rien trouvé[else if the story tense is the past perfect tense]n['][adapt the verb as in past tense] rien trouvé[end if] rien d'intéressant dans [the noun].";
	now the block consulting rule response (B) is "[The actor] [regardes] dans [the noun].";
	now the can't lock without a lock rule response (A) is "[Tu] [negate the verb peux for bg] verrouiller cela.";
	now the can't lock what's already locked rule response (A) is "[regarding the noun][Il] [adapt the verb es for bg] déjà [verrouillé].";
	now the can't lock what's open rule response (A) is "[Tu] [adapt the verb dois for bg] d'abord fermer [the noun].";
	now the can't lock without the correct key rule response (A) is "[regarding the second noun][Il] [negate the verb entres for bg] dans la serrure.";
	now the standard report locking rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][verrouilles] [the noun].";
	now the standard report locking rule response (B) is "[The actor] [verrouilles] [the noun]";
	now the can't unlock without a lock rule response (A) is "[Tu] [negate the verb peux for bg] déverrouiller cela.";
	now the can't unlock what's already unlocked rule response (A) is "[regarding the noun][Il] [adapt the verb es for bg] déjà [déverrouillé].";
	now the can't unlock without the correct key rule response (A) is "[regarding the second noun][Il] [negate the verb entres for bg] dans la serrure.";
	now the standard report unlocking rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][déverrouilles] [the noun].";
	now the standard report unlocking rule response (B) is "[The actor] [déverrouilles] [the noun].";
	now the can't switch on unless switchable rule response (A) is "[Tu] [negate the verb peux for bg] allumer cela.";
	now the can't switch on what's already on rule response (A) is "[regarding the noun][Il] [adapt the verb es for bg] déjà [allumé].";
	now the standard report switching on rule response (A) is "[if the actor is the player][Tu-j'][else][The actor] [end if][allumes] [the noun].";
	now the can't switch off unless switchable rule response (A) is "[Tu] [negate the verb peux for bg] éteindre cela.";
	now the can't switch off what's already off rule response (A) is "[regarding the noun][Il] [adapt the verb es for bg] déjà [éteint].";
	now the standard report switching off rule response (A) is "[if the actor is the player][Tu-j'][else][The actor] [end if][éteins] [the noun].";
	now the can't open unless openable rule response (A) is "[Tu] [negate the verb peux for bg] ouvrir [the noun].";
	now the can't open what's locked rule response (A) is "[regarding the noun][Il] [adapt the verb sembles for bg] être [fermé] à clef.";
	now the can't open what's already open rule response (A) is "[regarding the noun][Il] [adapt the verb es for bg] déjà [ouvert].";
	now the reveal any newly visible interior rule response (A) is "[Tu-j'][ouvres] [the noun], révélant ";
	now the standard report opening rule response (A) is "[Tu-j'][ouvres] [the noun].";
	now the standard report opening rule response (B) is "[The actor] [ouvres] [the noun].";
	now the standard report opening rule response (C) is "[The noun] s['][if the story tense is simple][ouvres][else if the story tense is the perfect tense][adapt the verb es in present tense] [ouvert][else if the story tense is the past perfect tense][adapt the verb es in past tense] [ouvert][end if].";
	now the can't close unless openable rule response (A) is "[Tu] [negate the verb peux for bg] fermer [the noun].";
	now the can't close what's already closed rule response (A) is "[regarding the noun][Il] [adapt the verb es for bg] déjà [fermé].";
	now the standard report closing rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][fermes] [the noun].";
	now the standard report closing rule response (B) is "[The actor] [fermes] [the noun].";
	now the standard report closing rule response (C) is "[The noun] [if the story tense is simple]se [fermes][else if the story tense is the perfect tense]s['][adapt the verb es in present tense] [fermé][else if the story tense is the past perfect tense]s['][adapt the verb es in past tense] [fermé][end if].";
	now the can't wear what's not clothing rule response (A) is "[Tu] [negate the verb peux for bg] porter cela.";
	now the can't wear what's not held rule response (A) is "[Tu] [negate the verb as for bg] cela sur [toi].";
	now the can't wear what's already worn rule response (A) is "[Tu] [regarding the noun][le] [regarding the player][adapt the verb portes for bg] déjà.";
	now the standard report wearing rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][mets] [the noun].";
	now the standard report wearing rule response (B) is "[The actor] [enfiles] [the noun].";
	now the can't take off what's not worn rule response (A) is "[Tu] [negate the verb portes for bg] [the noun].";
	now the can't exceed carrying capacity when taking off rule response (A) is "[Tu] [adapt the verb portes for bg] déjà trop de choses.";
	now the standard report taking off rule response (A) is "[Tu-j'][enlèves] [the noun].";
	now the standard report taking off rule response (B) is "[The actor] [enlèves] [the noun].";
	now the can't give what you haven't got rule response (A) is "[Tu] [negate the verb as for bg] [the noun] en main.";
	now the can't give to yourself rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][jongles] avec [the noun] pendant un moment, mais [tu] [n'arrives pas] à grand-chose.";
	now the can't give to a non-person rule response (A) is "[The second noun] ne [adapt the verb peux for bg] rien recevoir.";
	now the can't give clothes being worn rule response (A) is "(enlevant d'abord [the noun])[command clarification break]";
	now the block giving rule response (A) is "[The second noun] [negate the verb as for bg] l'air [intéressé].";
	now the can't exceed carrying capacity when giving rule response (A) is "[The second noun] [adapt the verb portes for bg] déjà trop de choses.";
	now the standard report giving rule response (A) is "[if le story tense is simple][Tu] [else][Tu-j'][end if][donnes] [the noun] [au second noun].";
	now the standard report giving rule response (B) is "[The actor] [if le story tense is simple][te-lui] [else][t'-lui][end if][regarding the actor][donnes] [the noun].";
	now the standard report giving rule response (C) is "[The actor] [donnes] [the noun] [au second noun].";
	now the can't show what you haven't got rule response (A) is "[Tu] [negate the verb as for bg] [the noun] en main.";
	now the block showing rule response (A) is "[The second noun] [adapt the verb es for bg] peu [impressionné]";
	now the block waking rule response (A) is "Cela [negate the verb sembles for bg] nécessaire.";
	now the implicitly remove thrown clothing rule response (A) is "(enlevant d'abord [the noun])[command clarification break]";
	now the futile to throw things at inanimate objects rule response (A) is "Futile.";
	now the block throwing at rule response (A) is "[Tu-j'][hésites] au moment crucial.";
	now the block attacking rule response (A) is "La violence [negate the verb es for bg] une solution ici.";
	now the kissing yourself rule response (A) is "Cela ne [t'][regarding nothing][if the story tense is simple][aides] pas beaucoup[else if the story tense is perfect tense][adapt the verb as in present tense] pas beaucoup aidé[else if the story tense is past perfect tense][adapt the verb as in past tense] pas beaucoup aidé[end if].";
	now the block kissing rule response (A) is "[The noun] n['][if the story tense is the present tense and the noun is singular-named]aimerait peut-être pas[else if the story tense is the present tense and the noun is plural-named]aimeraient peut-être pas[else if (the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense) and the noun is singular-named]aurait peut-être pas aimé[else if (the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense) and the noun is plural-named]auraient peut-être pas aimé[else if the story tense is the future tense and the noun is singular-named]aimera peut-être pas[else if the story tense is the future tense and the noun is plural-named]aimeront peut-être pas[end if] cela.";
	now the block answering rule response (A) is "Pas de réponse.";
	now the telling yourself rule response (A) is "[Tu] [if the story tense is simple][te-se] [parles][else if the story tense is perfect tense and (the story viewpoint is first person singular or the story viewpoint is first person plural or the story viewpoint is third person plural)][te-se] [adapt the verb es in present tense] parlé[else if the story tense is perfect tense][t'-s'][adapt the verb es in present tense] parlé[else if the story tense is past perfect tense and (the story viewpoint is first person singular or the story viewpoint is first person plural or the story viewpoint is third person plural)][te-se] [adapt the verb es in past tense] parlé[else if the story tense is past perfect tense][t'-s'][adapt the verb es in past tense] parlé[end if] à [toi]-même[if the player is plural-named]s[end if] un moment.";
	now the block telling rule response (A) is "Pas de réaction.";
	now the block asking rule response (A) is "Pas de réponse.";
	now the standard report waiting rule response (A) is "Le temps [passes]…";
	now the standard report waiting rule response (B) is "[The actor] [attends].";
	now the report touching yourself rule response (A) is "[Tu] n'y [adapt the verb vois for bg] aucune utilité.";
	now the report touching yourself rule response (B) is "[The actor] [te touches].";
	now the report touching other people rule response (A) is "[Tu] [adapt the verb sais for bg] tenir [tes] mains tranquilles.";
	now the report touching other people rule response (B) is "[The actor] [if the story tense is simple][te] [regarding the actor][touches][else if the story tense is perfect tense][t'][regarding the actor][adapt the verb as in present tense] [regarding the player][touché][else if the story tense is past perfect tense][t'][regarding the actor][adapt the verb as in past tense] [regarding the player][touché][end if].";
	now the report touching other people rule response (C) is "[The actor] [touches] [the noun].";
	now the report touching things rule response (A) is "[Tu] [if the story tense is simple]ne [sens] rien[else if the story tense is perfect tense]n['][adapt the verb as in present tense] rien senti[else if the story tense is past perfect tense]n['][adapt the verb as in past tense] rien senti[end if] de particulier.";
	now the report touching things rule response (B) is "[The actor] [touches] [the noun].";
	now the can't wave what's not held rule response (A) is "[Tu] [negate the verb as for bg] cela sur [toi].";
	now the report waving things rule response (A) is "[Tu-j'][agites] [the noun].";
	now the report waving things rule response (B) is "[The actor] [agites] [the noun].";
	now the can't pull what's fixed in place rule response (A) is "[C'est-c'était] fixé sur place.";
	now the can't pull scenery rule response (A) is "[Tu-j']en [adapt the verb es for bg] [incapable].";
	now the can't pull people rule response (A) is "[if the story tense is the present tense]Ce serait[else if the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense]Cela aurait été[else if the story tense is the future tense]Ce sera[end if] moins que courtois.";
	now the report pulling rule response (A) is "Rien d'évident ne [if the story tense is simple]se [produis][else if the story tense is perfect tense]s'est produit[else if the story tense is past perfect tense]s'était produit[end if].";
	now the report pulling rule response (B) is "[The actor] [tires] [the noun].";
	now the can't push what's fixed in place rule response (A) is "[C'est-c'était] fixé sur place.";
	now the can't push scenery rule response (A) is "[Tu-j']en [adapt the verb es for bg] [incapable].";
	now the can't push people rule response (A) is "[if the story tense is the present tense]Ce serait[else if the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense]Cela aurait été[else if the story tense is the future tense]Ce sera[end if] moins que courtois.";
	now the report pushing rule response (A) is "Rien d'évident ne [if the story tense is simple]se [produis][else if the story tense is perfect tense]s'est produit[else if the story tense is past perfect tense]s'était produit[end if].";
	now the report pushing rule response (B) is "[The actor] [pousses] [the noun].";
	now the can't turn what's fixed in place rule response (A) is "[C'est-c'était] fixé sur place.";
	now the can't turn scenery rule response (A) is "[Tu-j']en [adapt the verb es for bg] [incapable].";
	now the can't turn people rule response (A) is "[if the story tense is the present tense]Ce serait[else if the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense]Cela aurait été[else if the story tense is the future tense]Ce sera[end if] moins que courtois.";
	now the report turning rule response (A) is "Rien d'évident ne [if the story tense is simple]se [produis][else if the story tense is perfect tense]s'est produit[else if the story tense is past perfect tense]s'était produit[end if].";
	now the report turning rule response (B) is "[The actor] [tournes] [the noun].";
	now the can't push unpushable things rule response (A) is "[The noun] [negate the verb peux for bg] être [poussé] autre part.";
	now the can't push to non-directions rule response (A) is "Ce [negate the verb es for bg] une direction.";
	now the can't push vertically rule response (A) is "[The noun] [negate the verb peux for bg] être [poussé] vers le haut ou vers le bas.";
	now the can't push from within rule response (A) is "[The noun] [negate the verb peux for bg] être [poussé] [if the story tense is present tense]d'ici[else]de cet endroit[end if].";
	now the block pushing in directions rule response (A) is "[The noun] [negate the verb peux for bg] être [poussé] d'un endroit à un autre.";
	now the innuendo about squeezing people rule response (A) is "[Tu] [adapt the verb sais for bg] tenir [tes] mains tranquilles.";
	now the report squeezing rule response (A) is "[Tu] n['][adapt the verb arrives for bg] à rien ainsi.";
	now the report squeezing rule response (B) is "[The actor] [presses] [the noun].";
	now the block saying yes rule response (A) is "Mmmh[_]?";
	now the block saying no rule response (A) is "Mmmh[_]?";
	now the block burning rule response (A) is "Cet acte dangereux [if the story tense is the present tense]ne mènerait pas[else if the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense]n'aurait pas mené[else if the story tense is the future tense]ne mènera pas[end if] à grand-chose.";
	now the block waking up rule response (A) is "Il [negate the verb s'agis for bg] d'un rêve.";
	now the block thinking rule response (A) is "Quelle bonne idée.";
	now the report smelling rule response (A) is "[Tu] ne [adapt the verb sens for bg] rien de particulier.";
	now the report smelling rule response (B) is "[The actor] [renifles].";
	now the report listening rule response (A) is "[Tu] n['][adapt the verb entends for bg] rien de particulier.";
	now the report listening rule response (B) is "[The actor] [écoutes] attentivement.";
	now the report tasting rule response (A) is "[Tu] [if the story tense is simple]ne [remarques] rien[else if the story tense is perfect tense]n['][adapt the verb as in present tense] rien remarqué[else if the story tense is past perfect tense]n['][adapt the verb as in past tense] rien remarqué[end if] de particulier.";
	now the report tasting rule response (B) is "[The actor] [goûtes] [the noun].";
	now the block cutting rule response (A) is "[regarding the noun][Le] couper [if the story tense is the present tense]ne servirait pas[else if the story tense is the past historic tense or the story tense is the perfect tense or the story tense is the past tense]n'aurait pas servi[else if the story tense is the future tense]ne servira pas[end if] à grand-chose.";
	now the report jumping rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][sautes] sur place, vainement.";
	now the report jumping rule response (B) is "[The actor] [sautes] sur place.";
	now the block tying rule response (A) is "[Tu] n['][adapt the verb arrives for bg] à rien comme cela.";
	now the block drinking rule response (A) is "[The noun] n['][adapt the verb as for bg] rien de buvable.";
	now the block saying sorry rule response (A) is "Ce [negate the verb es for bg] grave.";
	now the block swinging rule response (A) is "Il n'y [adapt the verb as for bg] rien à balancer [ici].";
	now the can't rub another person rule response (A) is "[Tu] [adapt the verb sais for bg] tenir [tes] mains tranquilles.";
	now the report rubbing rule response (A) is "[if the story tense is simple][Tu] [else][Tu-j'][end if][frottes] [the noun].";
	now the report rubbing rule response (B) is "[The actor] [frottes] [the noun].";
	now the block setting it to rule response (A) is "[Tu] ne [adapt the verb peux for bg] [regarding the noun][le] régler sur rien.";
	now the report waving hands rule response (A) is "[Tu-j'][agites] les mains.";
	now the report waving hands rule response (B) is "[The actor] [agites] les mains.";
	now the block buying rule response (A) is "[The noun] [negate the verb es for bg] à vendre.";
	now the block climbing rule response (A) is "[Tu] [negate the verb peux for bg] arriver à grand-chose de cette manière.";
	now the block sleeping rule response (A) is "[Tu] [negate the verb as for bg] particulièrement sommeil.";
	now the adjust light rule response (A) is "[Tu es] [plongé] dans l'obscurité.";
	now the generate action rule response (A) is "[bracket]ne prenant en compte que les seize premiers[close bracket][command clarification break]";
	now the generate action rule response (B) is "[bracket]Il n'y a rien à faire.[close bracket]";
	now the basic accessibility rule response (A) is "[bracket]Vous devez indiquer un nom plus précis.[close bracket]";
	now the basic visibility rule response (A) is "Il [adapt the verb fais for bg] noir et [tu] ne [adapt the verb peux for bg] rien voir.";
	now the requested actions require persuasion rule response (A) is "[The noun] [adapt the verb as for bg] mieux à faire";
	now the carry out requested actions rule response (A) is "[The noun] [adapt the verb es for bg] [incapable] de faire cela.";
	now the access through barriers rule response (A) is "Ce [negate the verb es for bg] disponible.";
	now the can't reach inside closed containers rule response (A) is "[The noun] [negate the verb es for bg] [ouvert].";
	now the can't reach inside rooms rule response (A) is "[Tu] [negate the verb peux for bg] atteindre l'intérieur [du noun]..";
	now the can't reach outside closed containers rule response (A) is "[The noun] [negate the verb es for bg] [ouvert].";
	now the list writer internal rule response (A) is " (";
	now the list writer internal rule response (B) is ")";
	now the list writer internal rule response (C) is " et ";
	now the list writer internal rule response (D) is "procurant de la lumière";
	now the list writer internal rule response (E) is "[regarding the noun][fermé]";
	now the list writer internal rule response (F) is "[regarding the noun][vide]";
	now the list writer internal rule response (G) is "[regarding the noun][fermé] et [vide]";
	now the list writer internal rule response (H) is "[regarding the noun][fermé] et procurant de la lumière";
	now the list writer internal rule response (I) is "[regarding the noun][vide] et procurant de la lumière";
	now the list writer internal rule response (J) is "[regarding the noun][fermé], [vide][if serial comma option is active],[end if] et procurant de la lumière";
	now the list writer internal rule response (K) is "procurant de la lumière et [regarding the noun][porté]";
	now the list writer internal rule response (L) is "[regarding the noun][porté]";
	now the list writer internal rule response (M) is "[regarding the noun][ouvert]";
	now the list writer internal rule response (N) is "[regarding the noun][ouvert] mais [vide]";
	now the list writer internal rule response (O) is "[regarding the noun][fermé]";
	now the list writer internal rule response (P) is "[regarding the noun][fermé] et [verrouillé]";
	now the list writer internal rule response (Q) is "contenant";
	now the list writer internal rule response (R) is "sur [regarding the noun][le]quel[if the noun is a female]le[end if][if the noun is a plural-named]s[end if] ";
	now the list writer internal rule response (S) is ", sur [regarding the noun][le]quel[if the noun is a female]le[end if][if the noun is a plural-named]s[end if] ";
	now the list writer internal rule response (T) is "dans [regarding the noun][le]quel[if the noun is a female]le[end if][if the noun is a plural-named]s[end if] ";
	now the list writer internal rule response (U) is ", à l'intérieur [if the noun is male and the noun is singular-named]duquel[else if the noun is male and the noun is plural-named]desquels[else if the noun is female and the noun is singular-named]de laquelle[else if the noun is male and the noun is singular-named]desquelles[end if] ";
	now the list writer internal rule response (V) is "[regarding list writer internals]se [adapt the verb trouves for bg]";
	now the list writer internal rule response (W) is "il n'y [adapt the verb as for bg] rien";
	now the list writer internal rule response (X) is "Rien";
	now the list writer internal rule response (Y) is "rien";
	now the action processing internal rule response (A) is "[bracket]Cette commande demande de faire quelque chose en dehors du jeu, aussi n'a-t-elle de sens que de vous à moi. Il ne peut être demandé de faire cela [au noun].[close bracket]";
	now the action processing internal rule response (B) is "[bracket]Vous devez indiquer un objet.[close bracket]";
	now the action processing internal rule response (C) is "[bracket]Vous ne devez pas indiquer un objet.[close bracket]";
	now the action processing internal rule response (D) is "[bracket]Vous devez fournir un nom.[close bracket]";
	now the action processing internal rule response (E) is "[bracket]Vous ne devez pas fournir un nom.[close bracket]";
	now the action processing internal rule response (F) is "[bracket]Vous devez indiquer un second objet.[close bracket]";
	now the action processing internal rule response (G) is "[bracket]Vous ne devez pas indiquer un objet.[close bracket]";
	now the action processing internal rule response (H) is "[bracket]Vous devez fournir un second nom.[close bracket]";
	now the action processing internal rule response (I) is "[bracket]Vous ne devez pas fournir un nom.[close bracket]";
	now the action processing internal rule response (J) is "[bracket]Comme quelque chose de dramatique vient de se produire, votre liste de commandes a été raccourcie.[close bracket]";
	now the parser error internal rule response (A) is "[bracket]Je n'ai pas compris cette phrase.[close bracket]";
	now the parser error internal rule response (B) is "Merci de reformuler. Je n'ai compris que[_]: ";
	now the parser error internal rule response (C) is "Merci de reformuler. Je n'ai compris que[_]: (aller) ";
	now the parser error internal rule response (D) is "[bracket]Je n'ai pas compris ce nombre.[close bracket]";
	now the parser error internal rule response (E) is "[Tu] ne [adapt the verb vois for bg] rien de tel, à moins que cela ne [if the story tense is past historic tense or the story tense is perfect tense or the story tense is past tense]fût[else]soit[end if] sans grande importance.";
	now the parser error internal rule response (F) is "[bracket]Vous semblez en avoir dit trop peu.[close bracket]";
	now the parser error internal rule response (G) is "[Tu] ne [regarding the noun][l'][regarding the player][adapt the verb as for bg] pas sur [toi].";
	now the parser error internal rule response (H) is "[bracket]Vous ne pouvez pas utiliser plusieurs objets avec ce verbe.[close bracket]";
	now the parser error internal rule response (I) is "[bracket]Vous ne pouvez utiliser plusieurs objets qu'une fois par ligne.[close bracket]";
	now the parser error internal rule response (J) is "[bracket]Je ne suis pas sûr de ce à quoi «[_][pronoun i6 dictionary word][_]» se réfère.[close bracket]";
	now the parser error internal rule response (K) is "[bracket][Tu] [negate the verb peux for bg] voir «[_][pronoun i6 dictionary word][_]» ([the noun]) pour l'instant.[close bracket]";
	now the parser error internal rule response (L) is "[bracket]Vous avez exclu quelque chose qui n'était de toute façon pas compris dans la liste.[close bracket]";
	now the parser error internal rule response (M) is "[Tu] ne [adapt the verb peux for bg] agir ainsi qu'avec une chose douée de conscience.";
	now the parser error internal rule response (N) is "[bracket]Je ne connais pas ce verbe.[close bracket]";
	now the parser error internal rule response (O) is "[bracket]Ce n'est pas une chose à laquelle vous aurez à vous référer au cours du jeu.[close bracket]";
	now the parser error internal rule response (P) is "[bracket]Je n'ai pas compris la fin de la phrase.[close bracket]";
	now the parser error internal rule response (Q) is "[bracket][if number understood is 0]Aucun n'est disponible[else if number understood is 1]Seulement [number understood] est disponible[else]Seulement [number understood] sont disponibles[end if].[close bracket]";
	now the parser error internal rule response (R) is "[bracket]Ce nom n'a pas de sens dans ce contexte.[close bracket]";
	now the parser error internal rule response (S) is "[bracket]Pour répéter une commande comme «[_]grenouille, saute[_]», écrivez seulement «[_]encore[_]», et non «[_]grenouille, encore[_]».[close bracket]";
	now the parser error internal rule response (T) is "[bracket]Vous ne pouvez pas commencer par une virgule.[close bracket]";
	now the parser error internal rule response (U) is "[bracket]Vous semblez vouloir parler à quelqu'un, mais je ne vois pas à qui.[close bracket]";
	now the parser error internal rule response (V) is "[bracket]Vous ne pouvez pas discuter avec [the noun].[close bracket]";
	now the parser error internal rule response (W) is "[bracket]Pour parler à quelqu'un, essayez d'écrire «[_][italic type]quelqu'un[roman type], bonjour[_]» ou quelque chose de similaire.[close bracket]";
	now the parser error internal rule response (X) is "[bracket]Je vous demande pardon[_]?[close bracket]";
	now the parser nothing error internal rule response (A) is "[bracket]Il n'y a rien à faire.[close bracket]";
	now the parser nothing error internal rule response (B) is "[bracket]Rien [if the story tense is the future tense]ne sera[else]n['][adapt the verb es for bg][end if] disponible.[close bracket]";
	now the parser nothing error internal rule response (C) is "[regarding the noun][Il] [adapt the verb sembles for bg] appartenir [au noun].";
	now the parser nothing error internal rule response (D) is "[regarding the noun][Il] [negate the verb peux for bg] contenir quelque chose.";
	now the parser nothing error internal rule response (E) is "[The noun] [negate the verb es for bg] [ouvert].";
	now the parser nothing error internal rule response (F) is "[The noun] [adapt the verb es for bg] [vide].";
	now the darkness name internal rule response (A) is "L'obscurité";
	now the parser command internal rule response (A) is "[bracket]Désolé, impossible de corriger.[close bracket]";
	now the parser command internal rule response (B) is "[bracket]N'y pensez même pas.[close bracket]";
	now the parser command internal rule response (C) is "[bracket]«[_]Euh[_]» ne peut corriger qu'un seul mot.[close bracket]";
	now the parser command internal rule response (D) is "[bracket]Vous pouvez difficilement répéter cela.[close bracket]";
	now the parser clarification internal rule response (A) is "Précisez[_]: ";
	now the parser clarification internal rule response (B) is "Précisez[_]: ";
	now the parser clarification internal rule response (C) is "[bracket]Désolé, il peut y avoir seulement un objet ici. Lequel voulez-vous exactement[_]?[close bracket]";
	now the parser clarification internal rule response (D) is "[bracket]Pouvez-vous préciser ce qui est concerné par cette action[_]?[close bracket]";
	now the parser clarification internal rule response (E) is "[bracket]Pouvez-vous préciser ce qui est concerné par cette action[_]?[close bracket]";
	now the parser clarification internal rule response (F) is "ces choses";
	now the parser clarification internal rule response (G) is "cela";
	now the parser clarification internal rule response (H) is " ou ";
	now the yes or no question internal rule response (A) is "[bracket]Répondez par oui ou par non.[close bracket]";
	now the print protagonist internal rule response (A) is "[Tu]";
	now the print protagonist internal rule response (B) is "[toi]-même[if the player is plural-named]s[end if]";
	now the print protagonist internal rule response (C) is "[regarding the player][ton] [ancien] [toi]";
	now the standard implicit taking rule response (A) is "(prenant d'abord [the noun])[command clarification break]";
	now the standard implicit taking rule response (B) is "([the second noun] prenant d'abord [the noun])[command clarification break]";
	now the print obituary headline rule response (A) is " [Tu] [adapt the verb es for bg] [mort] ";
	now the print obituary headline rule response (B) is " Vous avez gagné ";
	now the print obituary headline rule response (C) is " Fin ";
	now the immediately undo rule response (A) is "[bracket]L'utilisation de la commande «[_]annuler[_]» n'est pas autorisée dans ce jeu.[close bracket]";
	now the immediately undo rule response (B) is "[bracket]Vous ne pouvez pas annuler alors que rien n'a encore été fait.[close bracket]";
	now the immediately undo rule response (C) is "[bracket]Votre interpréteur ne permet pas d'annuler. Désolé[_]![close bracket]";
	now the immediately undo rule response (D) is "[bracket]Impossible d'annuler. Désolé[_]![close bracket]";
	now the immediately undo rule response (E) is "[bracket]Action précédente annulée.[close bracket]";
	now the quit the game rule response (A) is "[bracket]Voulez-vous vraiment quitter[_]?[close bracket] ";
	now the save the game rule response (A) is "[bracket]La sauvegarde a échoué.[close bracket]";
	now the save the game rule response (B) is "[bracket]La partie a bien été sauvegardée[close bracket]";
	now the restore the game rule response (A) is "[bracket]Le chargement a échoué.[close bracket]";
	now the restore the game rule response (B) is "[bracket]La sauvegarde a bien été chargée[bracket]";
	now the restart the game rule response (A) is "[bracket]Voulez-vous vraiment recommencer[_]?[close bracket] ";
	now the restart the game rule response (B) is "[bracket]Échec.[close bracket]";
	now the verify the story file rule response (A) is "[bracket]Le fichier semble intact.[close bracket]";
	now the verify the story file rule response (B) is "[bracket]Le fichier est probablement corrompu.[close bracket]";
	now the switch the story transcript on rule response (A) is "[bracket]Une transcription est déjà en cours.[close bracket]";
	now the switch the story transcript on rule response (B) is "Début d'une transcription de…";
	now the switch the story transcript on rule response (C) is "[bracket]Impossible de commencer la transcription.[close bracket]";
	now the switch the story transcript off rule response (A) is "[bracket]Aucune transcription n'est en cours.[close bracket]";
	now the switch the story transcript off rule response (B) is "[line break][bracket]Fin de la transcription.[close bracket]";
	now the switch the story transcript off rule response (C) is "[bracket]Impossible de terminer la transcription.[close bracket]";
	now the announce the score rule response (A) is "[if the story has ended]Dans cette partie, vous avez obtenu[otherwise]Vous avez jusqu'à présent obtenu[end if] un score de [score] sur un total possible de [maximum score], en [turn count] tour[s]";
	now the announce the score rule response (B) is ", ce qui vous donne le rang de ";
	now the announce the score rule response (C) is "[bracket]Il n'y a pas de score dans cette histoire.[close bracket]";
	now the announce the score rule response (D) is "[bracket]Votre score vient d'augmenter de [number understood in words] point[s].[close bracket]";
	now the announce the score rule response (E) is "[bracket]Votre score vient de diminuer de [number understood in words] point[s].[close bracket]";
	now the standard report preferring abbreviated room descriptions rule response (A) is " est passé en mode «[_]description courte[_]»[_]; tous les lieux, même ceux étant visités pour la première fois, seront décrits brièvement.";
	now the standard report preferring unabbreviated room descriptions rule response (A) is " est passé en mode «[_]description longue[_]»[_]; tous les lieux, même ceux qui ont déjà été visités, seront décrits en détail.";
	now the standard report preferring sometimes abbreviated room descriptions rule response (A) is " est passé en mode «[_]description normale[_]»[_]; seuls les lieux visités pour la première fois seront décrits en détail.";
	now the standard report switching score notification on rule response (A) is "[bracket]Les notifications du score ont été activées.[close bracket]";
	now the standard report switching score notification off rule response (A) is "[bracket]Les notifications du score ont été désactivées.[close bracket]";
	now the announce the pronoun meanings rule response (A) is "Pour l'instant, ";
	now the announce the pronoun meanings rule response (B) is "signifie ";
	now the announce the pronoun meanings rule response (C) is "n'est pas défini";
	now the announce the pronoun meanings rule response (D) is "aucun pronom n'a été défini.".

Section 1.1.1.2 - In English

To use the English Standard Rules responses:
	now the block vaguely going rule response (A) is "You'll have to say which compass direction to go in.";
	now the print the final prompt rule response (A) is "> [run paragraph on]";
	now the print the final question rule response (A) is "Would you like to ";
	now the print the final question rule response (B) is " or ";
	now the standard respond to final question rule response (A) is "Please give one of the answers above.";
	now the you-can-also-see rule response (A) is "[We] ";
	now the you-can-also-see rule response (B) is "On [the domain] [we] ";
	now the you-can-also-see rule response (C) is "In [the domain] [we] ";
	now the you-can-also-see rule response (D) is "[can] also see ";
	now the you-can-also-see rule response (E) is "[can] see ";
	now the you-can-also-see rule response (F) is " here";
	now the use initial appearance in room descriptions rule response (A) is "On [the item] ";
	now the describe what's on scenery supporters in room descriptions rule response (A) is "On [the item] ";
	now the print empty inventory rule response (A) is "[We] [are] carrying nothing.";
	now the print standard inventory rule response (A) is "[We] [are] carrying:[line break]";
	now the report other people taking inventory rule response (A) is "[The actor] [look] through [their] possessions.";
	now the can't take yourself rule response (A) is "[We] [are] always self-possessed.";
	now the can't take other people rule response (A) is "I don't suppose [the noun] [would care] for that.";
	now the can't take component parts rule response (A) is "[regarding the noun][Those] [seem] to be a part of [the whole].";
	now the can't take people's possessions rule response (A) is "[regarding the noun][Those] [seem] to belong to [the owner].";
	now the can't take items out of play rule response (A) is "[regarding the noun][Those] [aren't] available.";
	now the can't take what you're inside rule response (A) is "[We] [would have] to get [if noun is a supporter]off[otherwise]out of[end if] [the noun] first.";
	now the can't take what's already taken rule response (A) is "[We] already [have] [regarding the noun][those].";
	now the can't take scenery rule response (A) is "[regarding the noun][They're] hardly portable.";
	now the can only take things rule response (A) is "[We] [cannot] carry [the noun].";
	now the can't take what's fixed in place rule response (A) is "[regarding the noun][They're] fixed in place.";
	now the use player's holdall to avoid exceeding carrying capacity rule response (A) is "(putting [the transferred item] into [the current working sack] to make room)[command clarification break]";
	now the can't exceed carrying capacity rule response (A) is "[We]['re] carrying too many things already.";
	now the standard report taking rule response (A) is "Taken.";
	now the standard report taking rule response (B) is "[The actor] [pick] up [the noun].";
	now the can't remove what's not inside rule response (A) is "But [regarding the noun][they] [aren't] there now.";
	now the can't remove from people rule response (A) is "[regarding the noun][Those] [seem] to belong to [the owner].";
	now the can't drop yourself rule response (A) is "[We] [lack] the dexterity.";
	now the can't drop body parts rule response (A) is "[We] [can't drop] part of [ourselves].";
	now the can't drop what's already dropped rule response (A) is "[The noun] [are] already here.";
	now the can't drop what's not held rule response (A) is "[We] [haven't] got [regarding the noun][those].";
	now the can't drop clothes being worn rule response (A) is "(first taking [the noun] off)[command clarification break]";
	now the can't drop if this exceeds carrying capacity rule response (A) is "[There] [are] no more room on [the receptacle].";
	now the can't drop if this exceeds carrying capacity rule response (B) is "[There] [are] no more room in [the receptacle].";
	now the standard report dropping rule response (A) is "Dropped.";
	now the standard report dropping rule response (B) is "[The actor] [put] down [the noun].";
	now the can't put something on itself rule response (A) is "[We] [can't put] something on top of itself.";
	now the can't put onto what's not a supporter rule response (A) is "Putting things on [the second noun] [would achieve] nothing.";
	now the can't put clothes being worn rule response (A) is "(first taking [regarding the noun][them] off)[command clarification break]";
	now the can't put if this exceeds carrying capacity rule response (A) is "[There] [are] no more room on [the second noun].";
	now the concise report putting rule response (A) is "Done.";
	now the standard report putting rule response (A) is "[The actor] [put] [the noun] on [the second noun].";
	now the can't insert something into itself rule response (A) is "[We] [can't put] something inside itself.";
	now the can't insert into closed containers rule response (A) is "[The second noun] [are] closed.";
	now the can't insert into what's not a container rule response (A) is "[regarding the second noun][Those] [can't contain] things.";
	now the can't insert clothes being worn rule response (A) is "(first taking [regarding the noun][them] off)[command clarification break]";
	now the can't insert if this exceeds carrying capacity rule response (A) is "[There] [are] no more room in [the second noun].";
	now the concise report inserting rule response (A) is "Done.";
	now the standard report inserting rule response (A) is "[The actor] [put] [the noun] into [the second noun].";
	now the can't eat unless edible rule response (A) is "[regarding the noun][They're] plainly inedible.";
	now the can't eat clothing without removing it first rule response (A) is "(first taking [the noun] off)[command clarification break]";
	now the can't eat other people's food rule response (A) is "[The owner] [might not appreciate] that.";
	now the standard report eating rule response (A) is "[We] [eat] [the noun]. Not bad.";
	now the standard report eating rule response (B) is "[The actor] [eat] [the noun].";
	now the stand up before going rule response (A) is "(first getting off [the chaise])[command clarification break]";
	now the can't travel in what's not a vehicle rule response (A) is "[We] [would have] to get off [the nonvehicle] first.";
	now the can't travel in what's not a vehicle rule response (B) is "[We] [would have] to get out of [the nonvehicle] first.";
	now the can't go through undescribed doors rule response (A) is "[We] [can't go] that way.";
	now the can't go through closed doors rule response (A) is "(first opening [the door gone through])[command clarification break]";
	now the can't go that way rule response (A) is "[We] [can't go] that way.";
	now the can't go that way rule response (B) is "[We] [can't], since [the door gone through] [lead] nowhere.";
	now the describe room gone into rule response (A) is "[The actor] [go] up";
	now the describe room gone into rule response (B) is "[The actor] [go] down";
	now the describe room gone into rule response (C) is "[The actor] [go] [noun]";
	now the describe room gone into rule response (D) is "[The actor] [arrive] from above";
	now the describe room gone into rule response (E) is "[The actor] [arrive] from below";
	now the describe room gone into rule response (F) is "[The actor] [arrive] from [the back way]";
	now the describe room gone into rule response (G) is "[The actor] [arrive]";
	now the describe room gone into rule response (H) is "[The actor] [arrive] at [the room gone to] from above";
	now the describe room gone into rule response (I) is "[The actor] [arrive] at [the room gone to] from below";
	now the describe room gone into rule response (J) is "[The actor] [arrive] at [the room gone to] from [the back way]";
	now the describe room gone into rule response (K) is "[The actor] [go] through [the noun]";
	now the describe room gone into rule response (L) is "[The actor] [arrive] from [the noun]";
	now the describe room gone into rule response (M) is "on [the vehicle gone by]";
	now the describe room gone into rule response (N) is "in [the vehicle gone by]";
	now the describe room gone into rule response (O) is ", pushing [the thing gone with] in front, and [us] along too";
	now the describe room gone into rule response (P) is ", pushing [the thing gone with] in front";
	now the describe room gone into rule response (Q) is ", pushing [the thing gone with] away";
	now the describe room gone into rule response (R) is ", pushing [the thing gone with] in";
	now the describe room gone into rule response (S) is ", taking [us] along";
	now the can't enter what's already entered rule response (A) is "But [we]['re] already on [the noun].";
	now the can't enter what's already entered rule response (B) is "But [we]['re] already in [the noun].";
	now the can't enter what's not enterable rule response (A) is "[regarding the noun][They're] not something [we] [can] stand on.";
	now the can't enter what's not enterable rule response (B) is "[regarding the noun][They're] not something [we] [can] sit down on.";
	now the can't enter what's not enterable rule response (C) is "[regarding the noun][They're] not something [we] [can] lie down on.";
	now the can't enter what's not enterable rule response (D) is "[regarding the noun][They're] not something [we] [can] enter.";
	now the can't enter closed containers rule response (A) is "[We] [can't get] into the closed [noun].";
	now the can't enter if this exceeds carrying capacity rule response (A) is "[There] [are] no more room on [the noun].";
	now the can't enter if this exceeds carrying capacity rule response (B) is "[There] [are] no more room in [the noun].";
	now the can't enter something carried rule response (A) is "[We] [can] only get into something free-standing.";
	now the implicitly pass through other barriers rule response (A) is "(getting off [the current home])[command clarification break]";
	now the implicitly pass through other barriers rule response (B) is "(getting out of [the current home])[command clarification break]";
	now the implicitly pass through other barriers rule response (C) is "(getting onto [the target])[command clarification break]";
	now the implicitly pass through other barriers rule response (D) is "(getting into [the target])[command clarification break]";
	now the implicitly pass through other barriers rule response (E) is "(entering [the target])[command clarification break]";
	now the standard report entering rule response (A) is "[We] [get] onto [the noun].";
	now the standard report entering rule response (B) is "[We] [get] into [the noun].";
	now the standard report entering rule response (C) is "[The actor] [get] into [the noun].";
	now the standard report entering rule response (D) is "[The actor] [get] onto [the noun].";
	now the can't exit when not inside anything rule response (A) is "But [we] [aren't] in anything at the [if story tense is present tense]moment[otherwise]time[end if].";
	now the can't exit closed containers rule response (A) is "You can't get out of the closed [cage].";
	now the standard report exiting rule response (A) is "[We] [get] off [the container exited from].";
	now the standard report exiting rule response (B) is "[We] [get] out of [the container exited from].";
	now the standard report exiting rule response (C) is "[The actor] [get] out of [the container exited from].";
	now the can't get off things rule response (A) is "But [we] [aren't] on [the noun] at the [if story tense is present tense]moment[otherwise]time[end if].";
	now the standard report getting off rule response (A) is "[The actor] [get] off [the noun].";
	now the room description heading rule response (A) is "Darkness";
	now the room description heading rule response (B) is " (on [the intermediate level])";
	now the room description heading rule response (C) is " (in [the intermediate level])";
	now the room description body text rule response (A) is "[It] [are] pitch dark, and [we] [can't see] a thing.";
	now the other people looking rule response (A) is "[The actor] [look] around.";
	now the examine directions rule response (A) is "[We] [see] nothing unexpected in that direction.";
	now the examine supporters rule response (A) is "On [the noun] ";
	now the examine devices rule response (A) is "[The noun] [are] [if story tense is present tense]currently [end if]switched [if the noun is switched on]on[otherwise]off[end if].";
	now the examine undescribed things rule response (A) is "[We] [see] nothing special about [the noun].";
	now the report other people examining rule response (A) is "[The actor] [look] closely at [the noun].";
	now the standard looking under rule response (A) is "[We] [find] nothing of interest.";
	now the report other people looking under rule response (A) is "[The actor] [look] under [the noun].";
	now the can't search unless container or supporter rule response (A) is "[We] [find] nothing of interest.";
	now the can't search closed opaque containers rule response (A) is "[We] [can't see] inside, since [the noun] [are] closed.";
	now the standard search containers rule response (A) is "In [the noun] ";
	now the standard search containers rule response (B) is "[The noun] [are] empty.";
	now the standard search supporters rule response (A) is "On [the noun] ";
	now the standard search supporters rule response (B) is "[There] [are] nothing on [the noun].";
	now the report other people searching rule response (A) is "[The actor] [search] [the noun].";
	now the block consulting rule response (A) is "[We] [discover] nothing of interest in [the noun].";
	now the block consulting rule response (B) is "[The actor] [look] at [the noun].";
	now the can't lock without a lock rule response (A) is "[regarding the noun][Those] [don't] seem to be something [we] [can] lock.";
	now the can't lock what's already locked rule response (A) is "[regarding the noun][They're] locked at the [if story tense is present tense]moment[otherwise]time[end if].";
	now the can't lock what's open rule response (A) is "First [we] [would have] to close [the noun].";
	now the can't lock without the correct key rule response (A) is "[regarding the second noun][Those] [don't] seem to fit the lock.";
	now the standard report locking rule response (A) is "[We] [lock] [the noun].";
	now the standard report locking rule response (B) is "[The actor] [lock] [the noun].";
	now the can't unlock without a lock rule response (A) is "[regarding the noun][Those] [don't] seem to be something [we] [can] unlock.";
	now the can't unlock what's already unlocked rule response (A) is "[regarding the noun][They're] unlocked at the [if story tense is present tense]moment[otherwise]time[end if].";
	now the can't unlock without the correct key rule response (A) is "[regarding the second noun][Those] [don't] seem to fit the lock.";
	now the standard report unlocking rule response (A) is "[We] [unlock] [the noun].";
	now the standard report unlocking rule response (B) is "[The actor] [unlock] [the noun].";
	now the can't switch on unless switchable rule response (A) is "[regarding the noun][They] [aren't] something [we] [can] switch.";
	now the can't switch on what's already on rule response (A) is "[regarding the noun][They're] already on.";
	now the standard report switching on rule response (A) is "[The actor] [switch] [the noun] on.";
	now the can't switch off unless switchable rule response (A) is "[regarding the noun][They] [aren't] something [we] [can] switch.";
	now the can't switch off what's already off rule response (A) is "[regarding the noun][They're] already off.";
	now the standard report switching off rule response (A) is "[The actor] [switch] [the noun] off.";
	now the can't open unless openable rule response (A) is "[regarding the noun][They] [aren't] something [we] [can] open.";
	now the can't open what's locked rule response (A) is "[regarding the noun][They] [seem] to be locked.";
	now the can't open what's already open rule response (A) is "[regarding the noun][They're] already open.";
	now the reveal any newly visible interior rule response (A) is "[We] [open] [the noun], revealing ";
	now the standard report opening rule response (A) is "[We] [open] [the noun].";
	now the standard report opening rule response (B) is "[The actor] [open] [the noun].";
	now the standard report opening rule response (C) is "[The noun] [open].";
	now the can't close unless openable rule response (A) is "[regarding the noun][They] [aren't] something [we] [can] close.";
	now the can't close what's already closed rule response (A) is "[regarding the noun][They're] already closed.";
	now the standard report closing rule response (A) is "[We] [close] [the noun].";
	now the standard report closing rule response (B) is "[The actor] [close] [the noun].";
	now the standard report closing rule response (C) is "[The noun] [close].";
	now the can't wear what's not clothing rule response (A) is "[We] [can't wear] [regarding the noun][those]!";
	now the can't wear what's not held rule response (A) is "[We] [aren't] holding [regarding the noun][those]!";
	now the can't wear what's already worn rule response (A) is "[We]['re] already wearing [regarding the noun][those]!";
	now the standard report wearing rule response (A) is "[We] [put] on [the noun].";
	now the standard report wearing rule response (B) is "[The actor] [put] on [the noun].";
	now the can't take off what's not worn rule response (A) is "[We] [aren't] wearing [the noun].";
	now the can't exceed carrying capacity when taking off rule response (A) is "[We]['re] carrying too many things already.";
	now the standard report taking off rule response (A) is "[We] [take] off [the noun].";
	now the standard report taking off rule response (B) is "[The actor] [take] off [the noun].";
	now the can't give what you haven't got rule response (A) is "[We] [aren't] holding [the noun].";
	now the can't give to yourself rule response (A) is "[We] [can't give] [the noun] to [ourselves].";
	now the can't give to a non-person rule response (A) is "[The second noun] [aren't] able to receive things.";
	now the can't give clothes being worn rule response (A) is "(first taking [the noun] off)[command clarification break]";
	now the block giving rule response (A) is "[The second noun] [don't] seem interested.";
	now the can't exceed carrying capacity when giving rule response (A) is "[The second noun] [are] carrying too many things already.";
	now the standard report giving rule response (A) is "[We] [give] [the noun] to [the second noun].";
	now the standard report giving rule response (B) is "[The actor] [give] [the noun] to [us].";
	now the standard report giving rule response (C) is "[The actor] [give] [the noun] to [the second noun].";
	now the can't show what you haven't got rule response (A) is "[We] [aren't] holding [the noun].";
	now the block showing rule response (A) is "[The second noun] [are] unimpressed.";
	now the block waking rule response (A) is "That [seem] unnecessary.";
	now the implicitly remove thrown clothing rule response (A) is "(first taking [the noun] off)[command clarification break]";
	now the futile to throw things at inanimate objects rule response (A) is "Futile.";
	now the block throwing at rule response (A) is "[We] [lack] the nerve when it [if story tense is the past tense]came[otherwise]comes[end if] to the crucial moment.";
	now the block attacking rule response (A) is "Violence [aren't] the answer to this one.";
	now the kissing yourself rule response (A) is "[We] [don't] get much from that.";
	now the block kissing rule response (A) is "[The noun] [might not] like that.";
	now the block answering rule response (A) is "[There] [are] no reply.";
	now the telling yourself rule response (A) is "[We] [talk] to [ourselves] a while.";
	now the block telling rule response (A) is "This [provoke] no reaction.";
	now the block asking rule response (A) is "[There] [are] no reply.";
	now the standard report waiting rule response (A) is "Time [pass].";
	now the standard report waiting rule response (B) is "[The actor] [wait].";
	now the report touching yourself rule response (A) is "[We] [achieve] nothing by this.";
	now the report touching yourself rule response (B) is "[The actor] [touch] [themselves].";
	now the report touching other people rule response (A) is "[The noun] [might not like] that.";
	now the report touching other people rule response (B) is "[The actor] [touch] [us].";
	now the report touching other people rule response (C) is "[The actor] [touch] [the noun].";
	now the report touching things rule response (A) is "[We] [feel] nothing unexpected.";
	now the report touching things rule response (B) is "[The actor] [touch] [the noun].";
	now the can't wave what's not held rule response (A) is "But [we] [aren't] holding [regarding the noun][those].";
	now the report waving things rule response (A) is "[We] [wave] [the noun].";
	now the report waving things rule response (B) is "[The actor] [wave] [the noun].";
	now the can't pull what's fixed in place rule response (A) is "[regarding the noun][They] [are] fixed in place.";
	now the can't pull scenery rule response (A) is "[We] [are] unable to.";
	now the can't pull people rule response (A) is "[The noun] [might not like] that.";
	now the report pulling rule response (A) is "Nothing obvious [happen].";
	now the report pulling rule response (B) is "[The actor] [pull] [the noun].";
	now the can't push what's fixed in place rule response (A) is "[regarding the noun][They] [are] fixed in place.";
	now the can't push scenery rule response (A) is "[We] [are] unable to.";
	now the can't push people rule response (A) is "[The noun] [might not like] that.";
	now the report pushing rule response (A) is "Nothing obvious [happen].";
	now the report pushing rule response (B) is "[The actor] [push] [the noun].";
	now the can't turn what's fixed in place rule response (A) is "[regarding the noun][They] [are] fixed in place.";
	now the can't turn scenery rule response (A) is "[We] [are] unable to.";
	now the can't turn people rule response (A) is "[The noun] [might not like] that.";
	now the report turning rule response (A) is "Nothing obvious [happen].";
	now the report turning rule response (B) is "[The actor] [turn] [the noun].";
	now the can't push unpushable things rule response (A) is "[The noun] [cannot] be pushed from place to place.";
	now the can't push to non-directions rule response (A) is "[regarding the noun][They] [aren't] a direction.";
	now the can't push vertically rule response (A) is "[The noun] [cannot] be pushed up or down.";
	now the can't push from within rule response (A) is "[The noun] [cannot] be pushed from here.";
	now the block pushing in directions rule response (A) is "[The noun] [cannot] be pushed from place to place.";
	now the innuendo about squeezing people rule response (A) is "[The noun] [might not like] that.";
	now the report squeezing rule response (A) is "[We] [achieve] nothing by this.";
	now the report squeezing rule response (B) is "[The actor] [squeeze] [the noun].";
	now the block saying yes rule response (A) is "That was a rhetorical question.";
	now the block saying no rule response (A) is "That was a rhetorical question.";
	now the block burning rule response (A) is "This dangerous act [would achieve] little.";
	now the block waking up rule response (A) is "The dreadful truth [are], this [are not] a dream.";
	now the block thinking rule response (A) is "What a good idea.";
	now the report smelling rule response (A) is "[We] [smell] nothing unexpected.";
	now the report smelling rule response (B) is "[The actor] [sniff].";
	now the report listening rule response (A) is "[We] [hear] nothing unexpected.";
	now the report listening rule response (B) is "[The actor] [listen].";
	now the report tasting rule response (A) is "[We] [taste] nothing unexpected.";
	now the report tasting rule response (B) is "[The actor] [taste] [the noun].";
	now the block cutting rule response (A) is "Cutting [regarding the noun][them] up [would achieve] little.";
	now the report jumping rule response (A) is "[We] [jump] on the spot.";
	now the report jumping rule response (B) is "[The actor] [jump] on the spot.";
	now the block tying rule response (A) is "[We] [would achieve] nothing by this.";
	now the block drinking rule response (A) is "[There's] nothing suitable to drink here.";
	now the block saying sorry rule response (A) is "Oh, don't [if American dialect option is active]apologize[otherwise]apologise[end if].";
	now the block swinging rule response (A) is "[There's] nothing sensible to swing here.";
	now the can't rub another person rule response (A) is "[The noun] [might not like] that.";
	now the report rubbing rule response (A) is "[We] [rub] [the noun].";
	now the report rubbing rule response (B) is "[The actor] [rub] [the noun].";
	now the block setting it to rule response (A) is "No, [we] [can't set] [regarding the noun][those] to anything.";
	now the report waving hands rule response (A) is "[We] [wave].";
	now the report waving hands rule response (B) is "[The actor] [wave].";
	now the block buying rule response (A) is "Nothing [are] on sale.";
	now the block climbing rule response (A) is "I don't think much is to be achieved by that.";
	now the block sleeping rule response (A) is "[We] [aren't] feeling especially drowsy.";
	now the adjust light rule response (A) is "[It] [are] [if story tense is present tense]now [end if]pitch dark in [if story tense is present tense]here[else]there[end if]!";
	now the generate action rule response (A) is "(considering the first sixteen objects only)[command clarification break]";
	now the generate action rule response (B) is "Nothing to do!";
	now the basic accessibility rule response (A) is "You must name something more substantial.";
	now the basic visibility rule response (A) is "It is pitch dark, and you can't see a thing.";
	now the requested actions require persuasion rule response (A) is "[The noun] [have] better things to do.";
	now the carry out requested actions rule response (A) is "[The noun] [are] unable to do that.";
	now the access through barriers rule response (A) is "[regarding the noun][Those] [aren't] available.";
	now the can't reach inside closed containers rule response (A) is "[The noun] [aren't] open.";
	now the can't reach inside rooms rule response (A) is "[We] [can't] reach into [the noun].";
	now the can't reach outside closed containers rule response (A) is "[The noun] [aren't] open.";
	now the list writer internal rule response (A) is " (";
	now the list writer internal rule response (B) is ")";
	now the list writer internal rule response (C) is " and ";
	now the list writer internal rule response (D) is "providing light";
	now the list writer internal rule response (E) is "closed";
	now the list writer internal rule response (F) is "empty";
	now the list writer internal rule response (G) is "closed and empty";
	now the list writer internal rule response (H) is "closed and providing light";
	now the list writer internal rule response (I) is "empty and providing light";
	now the list writer internal rule response (J) is "closed, empty[if serial comma option is active],[end if] and providing light";
	now the list writer internal rule response (K) is "providing light and being worn";
	now the list writer internal rule response (L) is "being worn";
	now the list writer internal rule response (M) is "open";
	now the list writer internal rule response (N) is "open but empty";
	now the list writer internal rule response (O) is "closed";
	now the list writer internal rule response (P) is "closed and locked";
	now the list writer internal rule response (Q) is "containing";
	now the list writer internal rule response (R) is "on [if the noun is a person]whom[otherwise]which[end if] ";
	now the list writer internal rule response (S) is ", on top of [if the noun is a person]whom[otherwise]which[end if] ";
	now the list writer internal rule response (T) is "in [if the noun is a person]whom[otherwise]which[end if] ";
	now the list writer internal rule response (U) is ", inside [if the noun is a person]whom[otherwise]which[end if] ";
	now the list writer internal rule response (V) is "[regarding list writer internals][are]";
	now the list writer internal rule response (W) is "[regarding list writer internals][are] nothing";
	now the list writer internal rule response (X) is "Nothing";
	now the list writer internal rule response (Y) is "nothing";
	now the action processing internal rule response (A) is "[bracket]That command asks to do something outside of play, so it can only make sense from you to me. [The noun] cannot be asked to do this.[close bracket]";
	now the action processing internal rule response (B) is "You must name an object.";
	now the action processing internal rule response (C) is "You may not name an object.";
	now the action processing internal rule response (D) is "You must supply a noun.";
	now the action processing internal rule response (E) is "You may not supply a noun.";
	now the action processing internal rule response (F) is "You must name a second object.";
	now the action processing internal rule response (G) is "You may not name a second object.";
	now the action processing internal rule response (H) is "You must supply a second noun.";
	now the action processing internal rule response (I) is "You may not supply a second noun.";
	now the action processing internal rule response (J) is "(Since something dramatic has happened, your list of commands has been cut short.)";
	now the parser error internal rule response (A) is "I didn't understand that sentence.";
	now the parser error internal rule response (B) is "I only understood you as far as wanting to ";
	now the parser error internal rule response (C) is "I only understood you as far as wanting to (go) ";
	now the parser error internal rule response (D) is "I didn't understand that number.";
	now the parser error internal rule response (E) is "[We] [can't] see any such thing.";
	now the parser error internal rule response (F) is "You seem to have said too little!";
	now the parser error internal rule response (G) is "[We] [aren't] holding that!";
	now the parser error internal rule response (H) is "You can't use multiple objects with that verb.";
	now the parser error internal rule response (I) is "You can only use multiple objects once on a line.";
	now the parser error internal rule response (J) is "I'm not sure what ['][pronoun i6 dictionary word]['] refers to.";
	now the parser error internal rule response (K) is "[We] [can't] see ['][pronoun i6 dictionary word]['] ([the noun]) at the moment.";
	now the parser error internal rule response (L) is "You excepted something not included anyway!";
	now the parser error internal rule response (M) is "You can only do that to something animate.";
	now the parser error internal rule response (N) is "That's not a verb I [if American dialect option is active]recognize[otherwise]recognise[end if].";
	now the parser error internal rule response (O) is "That's not something you need to refer to in the course of this game.";
	now the parser error internal rule response (P) is "I didn't understand the way that finished.";
	now the parser error internal rule response (Q) is "[if number understood is 0]None[otherwise]Only [number understood][end if] of those [are] available.";
	now the parser error internal rule response (R) is "That noun did not make sense in this context.";
	now the parser error internal rule response (S) is "To repeat a command like 'frog, jump', just say 'again', not 'frog, again'.";
	now the parser error internal rule response (T) is "You can't begin with a comma.";
	now the parser error internal rule response (U) is "You seem to want to talk to someone, but I can't see whom.";
	now the parser error internal rule response (V) is "You can't talk to [the noun].";
	now the parser error internal rule response (W) is "To talk to someone, try 'someone, hello' or some such.";
	now the parser error internal rule response (X) is "I beg your pardon?";
	now the parser nothing error internal rule response (A) is "Nothing to do!";
	now the parser nothing error internal rule response (B) is "There are none at all available!";
	now the parser nothing error internal rule response (C) is "[regarding the noun][Those] [seem] to belong to [the noun].";
	now the parser nothing error internal rule response (D) is "[regarding the noun][Those] [can't] contain things.";
	now the parser nothing error internal rule response (E) is "[The noun] [aren't] open.";
	now the parser nothing error internal rule response (F) is "[The noun] [are] empty.";
	now the darkness name internal rule response (A) is "Darkness";
	now the parser command internal rule response (A) is "Sorry, that can't be corrected.";
	now the parser command internal rule response (B) is "Think nothing of it.";
	now the parser command internal rule response (C) is "'Oops' can only correct a single word.";
	now the parser command internal rule response (D) is "You can hardly repeat that.";
	now the parser clarification internal rule response (A) is "Who do you mean, ";
	now the parser clarification internal rule response (B) is "Which do you mean, ";
	now the parser clarification internal rule response (C) is "Sorry, you can only have one item here. Which exactly?";
	now the parser clarification internal rule response (D) is "Whom do you want [if the noun is not the player][the noun] [end if]to [parser command so far]?";
	now the parser clarification internal rule response (E) is "What do you want [if the noun is not the player][the noun] [end if]to [parser command so far]?";
	now the parser clarification internal rule response (F) is "those things";
	now the parser clarification internal rule response (G) is "that";
	now the parser clarification internal rule response (H) is " or ";
	now the yes or no question internal rule response (A) is "Please answer yes or no.";
	now the print protagonist internal rule response (A) is "[We]";
	now the print protagonist internal rule response (B) is "[ourselves]";
	now the print protagonist internal rule response (C) is "[our] former self";
	now the standard implicit taking rule response (A) is "(first taking [the noun])[command clarification break]";
	now the standard implicit taking rule response (B) is "([the second noun] first taking [the noun])[command clarification break]";
	now the print obituary headline rule response (A) is " You have died ";
	now the print obituary headline rule response (B) is " You have won ";
	now the print obituary headline rule response (C) is " The End ";
	now the immediately undo rule response (A) is "The use of 'undo' is forbidden in this game.";
	now the immediately undo rule response (B) is "You can't 'undo' what hasn't been done!";
	now the immediately undo rule response (C) is "Your interpreter does not provide 'undo'. Sorry!";
	now the immediately undo rule response (D) is "'Undo' failed. Sorry!";
	now the immediately undo rule response (E) is "[bracket]Previous turn undone.[close bracket]";
	now the quit the game rule response (A) is "Are you sure you want to quit? ";
	now the save the game rule response (A) is "Save failed.";
	now the save the game rule response (B) is "Ok.";
	now the restore the game rule response (A) is "Restore failed.";
	now the restore the game rule response (B) is "Ok.";
	now the restart the game rule response (A) is "Are you sure you want to restart? ";
	now the restart the game rule response (B) is "Failed.";
	now the verify the story file rule response (A) is "The game file has verified as intact.";
	now the verify the story file rule response (B) is "The game file did not verify as intact, and may be corrupt.";
	now the switch the story transcript on rule response (A) is "Transcripting is already on.";
	now the switch the story transcript on rule response (B) is "Start of a transcript of";
	now the switch the story transcript on rule response (C) is "Attempt to begin transcript failed.";
	now the switch the story transcript off rule response (A) is "Transcripting is already off.";
	now the switch the story transcript off rule response (B) is "[line break]End of transcript.";
	now the switch the story transcript off rule response (C) is "Attempt to end transcript failed.";
	now the announce the score rule response (A) is "[if the story has ended]In that game you scored[otherwise]You have so far scored[end if] [score] out of a possible [maximum score], in [turn count] turn[s]";
	now the announce the score rule response (B) is ", earning you the rank of ";
	now the announce the score rule response (C) is "[There] [are] no score in this story.";
	now the announce the score rule response (D) is "[bracket]Your score has just gone up by [number understood in words] point[s].[close bracket]";
	now the announce the score rule response (E) is "[bracket]Your score has just gone down by [number understood in words] point[s].[close bracket]";
	now the standard report preferring abbreviated room descriptions rule response (A) is " is now in its 'superbrief' mode, which always gives short descriptions of locations (even if you haven't been there before).";
	now the standard report preferring unabbreviated room descriptions rule response (A) is " is now in its 'verbose' mode, which always gives long descriptions of locations (even if you've been there before).";
	now the standard report preferring sometimes abbreviated room descriptions rule response (A) is " is now in its 'brief' printing mode, which gives long descriptions of places never before visited and short descriptions otherwise.";
	now the standard report switching score notification on rule response (A) is "Score notification on.";
	now the standard report switching score notification off rule response (A) is "Score notification off.";
	now the announce the pronoun meanings rule response (A) is "At the moment, ";
	now the announce the pronoun meanings rule response (B) is "means ";
	now the announce the pronoun meanings rule response (C) is "is unset";
	now the announce the pronoun meanings rule response (D) is "no pronouns are known to the game.".

Chapter 1.1.2 - In Rideable Vehicles (for use with Rideable Vehicles by Graham Nelson)

Section 1.1.2.1 - In French

To use the French Rideable Vehicle responses:
	now the can't mount when mounted on an animal rule response (A) is "[Tu es-étais] déjà sur [the steed].";
	now the can't mount when mounted on a vehicle rule response (A) is "[Tu es-étais] déjà sur [the conveyance]";
	now the can't mount something unrideable rule response (A) is "[Tu] [negate the verb peux for bg] monter sur [the noun].";
	now the standard report mounting rule response (A) is "[Tu] [vas] sur [the noun].";
	now the standard report mounting rule response (B) is "[The actor] [vas] sur [the noun].";
	now the mounting excuses rule response (A) is "[The person asked] [adapt the verb es for bg] déjà sur [the steed].";
	now the mounting excuses rule response (B) is "[The person asked] [adapt the verb es for bg] déjà sur [the conveyance].";
	now the mounting excuses rule response (C) is "[The person asked] [adapt the verb peux for bg] monter sur [the noun].";
	now the can't dismount when not mounted rule response (A) is "[Tu] [negate the verb es for bg] sur quelque chose.";
	now the standard report dismounting rule response (A) is "[Tu] [descends] [du noun].[line break][run paragraph on]";
	now the standard report dismounting rule response (B) is "[The actor] [descends] [du noun].";
	now the dismounting excuses rule response (A) is "[The person asked] [negate the verb es for bg] sur quelque chose.".

Section 1.1.2.2 - In English

To use the English Rideable Vehicle responses:
	now the can't mount when mounted on an animal rule response (A) is "[We] [are] already riding [the steed].";
	now the can't mount when mounted on a vehicle rule response (A) is "[We] [are] already riding [the conveyance].";
	now the can't mount something unrideable rule response (A) is "[The noun] [cannot] be ridden.";
	now the standard report mounting rule response (A) is "[We] [mount] [the noun].";
	now the standard report mounting rule response (B) is "[The actor] [mount] [the noun].";
	now the mounting excuses rule response (A) is "[The person asked] [are] already riding [the steed].";
	now the mounting excuses rule response (B) is "[The person asked] [are] already riding [the conveyance].";
	now the mounting excuses rule response (C) is "[The noun] [cannot] be ridden.";
	now the can't dismount when not mounted rule response (A) is "[We] [are] not riding anything.";
	now the standard report dismounting rule response (A) is "[We] [dismount] [the noun].[line break][run paragraph on]";
	now the standard report dismounting rule response (B) is "[The actor] [dismount] [the noun].";
	now the dismounting excuses rule response (A) is "[The person asked] [are] not riding anything.".

Chapter 1.1.3 - In Locksmith (for use with Locksmith by Emily Short)

Section 1.1.3.1 - In French

In French assurer is a verb.

To use the French Locksmith responses:
	[now the opening doors before entering rule response (A) is "(ouvrant d'abord [the blocking door])[command clarification break]";
	now the closing doors before locking rule response (A) is "(fermant d'abord [the door ajar])[command clarification break]";
	now the closing doors before locking keylessly rule response (A) is "(fermant d'abord [the door ajar])[command clarification break]";]
	now the unlocking before opening rule response (A) is "(déverrouillant d'abord [the sealed chest])[command clarification break]";
	now the standard printing key lack rule response (A) is "[Tu] [negate the verb as for bg] la clef correspondant [au locked-thing].";
	now the right second rule response (A) is "[regarding the second noun][Il] [negate the verb entres for bg] dans la serrure.";
	now the standard keylessly unlocking rule response (A) is "(avec [the key unlocked with])[command clarification break]";
	now the standard keylessly locking rule response (A) is "(avec [the key locked with])[command clarification break]";
	now the identify passkeys in inventory rule response (A) is " (ouvrant [the list of things unbolted by the item])";
	now the passkey description rule response (A) is "[The noun] [adapt the verb ouvres for bg] [the list of things unbolted by the noun].";
	now the limiting keychains rule response (A) is "[The noun] [negate the verb es for bg] une clef.";
	now the must have accessible the noun rule response (A) is "[Tu] ne [peux] rien faire [si tu] [ne tiens pas] [the noun].";
	now the must have accessible the second noun rule response (A) is "[Tu] ne [peux] rien faire [si tu] [ne tiens pas] [the second noun].";
	now the lock debugging rule response (A) is "[bracket]Déverrouillage [du item].[close bracket]";
	now the report universal unlocking rule response (A) is "[bracket]Un fort clic stéréophonique [t'][assures] que tout dans le jeu a été déverrouillé.[close bracket]".

Section 1.1.3.2 - In English

To use the English Locksmith responses:
	[now the opening doors before entering rule response (A) is "(first opening [the blocking door])[command clarification break]";
	now the closing doors before locking rule response (A) is "(first closing [the door ajar])[command clarification break]";
	now the closing doors before locking keylessly rule response (A) is "(first closing [the door ajar])[command clarification break]";]
	now the unlocking before opening rule response (A) is "(first unlocking [the sealed chest])[command clarification break]";
	now the standard printing key lack rule response (A) is "[We] [lack] a key that fits [the locked-thing].";
	now the right second rule response (A) is "[The second noun] [do not fit] [the noun].";
	now the standard keylessly unlocking rule response (A) is "(with [the key unlocked with])[command clarification break]";
	now the standard keylessly locking rule response (A) is "(with [the key locked with])[command clarification break]";
	now the identify passkeys in inventory rule response (A) is " (which [open] [the list of things unbolted by the item])";
	now the passkey description rule response (A) is "[The noun] [unlock] [the list of things unbolted by the noun].";
	now the limiting keychains rule response (A) is "[The noun] [are] not a key.";
	now the must have accessible the noun rule response (A) is "Without holding [the noun], [we] [can] do nothing.";
	now the must have accessible the second noun rule response (A) is "Without holding [the second noun], [we] [can] do nothing.";
	now the lock debugging rule response (A) is "Unlocking [the item].";
	now the report universal unlocking rule response (A) is "A loud stereophonic click assures you that everything in the game has been unlocked.".

Chapter 1.1.4 - In Basic Screen Effects (for use with Basic Screen Effects by Emily Short)

Section 1.1.4.1 - In French

To use the French Basic Screen Effects responses:
	now the standard pausing the game rule response (A) is "[bracket][paragraph break]Appuyez sur ESPACE pour continuer.[close bracket]".

Section 1.1.4.2 - In English

To use the English Basic Screen Effects responses:
	now the standard pausing the game rule response (A) is "[paragraph break]Please press SPACE to continue.".

Chapter 1.1.5 - In Inanimate Listeners (for use with Inanimate Listeners by Emily Short)

Section 1.1.5.1 - In French

To use the French Inanimate Listeners responses:
	now the unsuccessful persuasion of inanimate objects rule response (A) is "[The target] [negate the verb peux for bg] faire tout ce que quelqu'un [regarding nothing][adapt the verb peux for bg] accomplir.".

Section 1.1.5.2 - In English

To use the French Inanimate Listeners responses:
	now the unsuccessful persuasion of inanimate objects rule response (A) is "[The target] [cannot] do everything a person can.".

Chapter 1.1.6 - In Skeleton Keys (for use with Skeleton Keys by Emily Short)

Section 1.1.6.1 - In French

To use the French Skeleton Keys responses:
	now the right second rule response (A) is "[Tu] [negate the verb portes for bg] [the second noun].";
	now the right second rule response (B) is "[regarding the second noun][Il] [negate the verb entres for bg] dans la serrure.".

Section 1.1.6.2 - In English

To use the English Skeleton Keys responses:
	now the right second rule response (A) is "[We]['re not] carrying [the second noun].";
	now the right second rule response (B) is "[The second noun] [do not fit] [the noun].".

Chapter 1.1.6 - In Epistemology (for use with Epistemology by Eric Eve)

Section 1.1.6.1 - In French

To use the French Epistemology responses:
	now the report epistemic status rule response (A) is "[bracket][noun] - [if seen]vu[otherwise]non vu[end if] / [if familiar]familier[otherwise]non familier[end if] / [if known]connu[otherwise]inconnu[end if].[close bracket]".

Section 1.1.6.2 - In English

To use the English Epistemology responses:
	now the report epistemic status rule response (A) is "[noun] - [if seen]seen[otherwise]unseen[end if] / [if familiar]familiar[otherwise]unfamiliar[end if] / [if known]known[otherwise]unknown[end if].".

Part 1.2 - The Final Question

Table of Final Question Options (replaced)
final question wording	only if victorious	topic	final response rule	final response activity
"[if in French]RECOMMENCER[else]RESTART[end if]"	false	"recommencer/restart"	immediately restart the VM rule	--
"[if in French]CHARGER une partie sauvegardée[else]RESTORE a saved game[end if]"	false	"charger/restore"	immediately restore saved game rule	--
"[if in French]lire quelques SUGGESTIONS amusantes[else]see some suggestions for AMUSING things to do[end if]"	true	"suggestions/suggestion/amusing"	--	amusing a victorious player
"[if in French]QUITTER[else]QUIT[end if]"	false	"quitter/quit"	immediately quit rule	--
"[if in French]ANNULER la dernière commande [else]UNDO the last command[end if]"	false	"annuler/undo"	immediately undo rule	--

Part 1.3 - The banner

Include (-
[ Banner;
	BeginActivity(PRINTING_BANNER_TEXT_ACT);
	if (ForActivity(PRINTING_BANNER_TEXT_ACT) == false) {
		VM_Style(HEADER_VMSTY);
		TEXT_TY_Say(Story);
		VM_Style(NORMAL_VMSTY);
		new_line;
		TEXT_TY_Say(Headline);
		#ifdef Story_Author;
		print " par "; TEXT_TY_Say(Story_Author);
		#endif; ! Story_Author
		new_line;
		VM_Describe_Release();
		print " / Inform 7 build ", (PrintI6Text) NI_BUILD_COUNT, " ";
		print "(I6/v"; inversion;
		print " lib ", (PrintI6Text) LibRelease, ") ";
		#Ifdef STRICT_MODE;
		print "S";
		#Endif; ! STRICT_MODE
		#Ifdef DEBUG;
		print "D";
		#Endif; ! DEBUG
		new_line;
	}
	EndActivity(PRINTING_BANNER_TEXT_ACT);
];
-) instead of "Banner" in "Printing.i6t".

Part 1.5 - Rule replacements

The bilingual examine containers rule is listed instead of the examine containers rule in the carry out examining rulebook.

This is the bilingual examine containers rule:
	if the noun is a container:
		if in French:
			if the noun is open or the noun is transparent:
				if something described which is not scenery is in the noun and something which
					is not the player is in the noun:
					say "[The noun] [adapt the verb contiens for bg] " (A);
					list the contents of the noun, as a sentence, tersely, not listing
						concealed items;
					say ".";
					now examine text printed is true;
				otherwise if examine text printed is false:
					if the player is in the noun:
						make no decision;
					say "[The noun] [adapt the verb es for bg] [vide]." (B);
					now examine text printed is true;
		else:
			if the noun is open or the noun is transparent:
				if something described which is not scenery is in the noun and something which
					is not the player is in the noun:
					say "In [the noun] " (C);
					list the contents of the noun, as a sentence, tersely, not listing
						concealed items, prefacing with is/are;
					say ".";
					now examine text printed is true;
				otherwise if examine text printed is false:
					if the player is in the noun:
						make no decision;
					say "[The noun] [are] empty." (D);
					now examine text printed is true.

The bilingual examine supporters rule is listed instead of the examine supporters rule in the carry out examining rulebook.

This is the bilingual examine supporters rule:
	if the noun is a supporter:
		if something described which is not scenery is on the noun and something which is
			not the player is on the noun:
			if in French:
				say "Il y [adapt the verb as for bg] " (A);
				list the contents of the noun, as a sentence, tersely, not listing
					concealed items, including contents,
					giving brief inventory information;
				say " sur [the noun]." (B);
				now examine text printed is true;
			else:
				say "On [the noun] " (C);
				list the contents of the noun, as a sentence, tersely, not listing
					concealed items, prefacing with is/are, including contents,
					giving brief inventory information;
				say ".";
				now examine text printed is true.

The bilingual standard search containers rule is listed instead of the standard search containers rule in the report searching rulebook.

Report searching a container (this is the bilingual standard search containers rule):
	if in French:
		if the noun contains a described thing which is not scenery:
			say "[The noun] [adapt the verb contiens for bg] " (A);
			list the contents of the noun, as a sentence, tersely, not listing
				concealed items;
			say ".";
		otherwise:
			say "[The noun] [adapt the verb es for bg] [vide]." (B);
	else:
		if the noun contains a described thing which is not scenery:
			say "In [the noun] " (C);
			list the contents of the noun, as a sentence, tersely, not listing
				concealed items, prefacing with is/are;
			say ".";
		otherwise:
			say "[The noun] [are] empty." (D).

The bilingual standard search supporters rule is listed instead of the standard search supporters rule in the report searching rulebook.

Report searching a supporter (this is the bilingual standard search supporters rule):
	if in French:
		if the noun supports a described thing which is not scenery:
			say "Il y [adapt the verb as for bg] " (A);
			list the contents of the noun, as a sentence, tersely, not listing
				concealed items;
			say " sur [the noun].";
		otherwise:
			say "Il n'y [adapt the verb as for bg] rien sur [the noun]" (B);
	else:
		if the noun supports a described thing which is not scenery:
			say "On [the noun] " (C);
			list the contents of the noun, as a sentence, tersely, not listing
				concealed items, prefacing with is/are;
			say ".";
		otherwise:
			now the prior named object is nothing;
			say "[There] [are] nothing on [the noun]." (D).

The bilingual can't enter what's not enterable rule is listed instead of the can't enter what's not enterable rule in the check entering rulebook.

Check an actor entering (this is the bilingual can't enter what's not enterable rule):
	if the noun is not enterable:
		if the player is the actor:
			if in French:
				if the player's command includes "asseoir":
					say "[Tu] [negate the verb peux for bg] [t'-s']y asseoir." (A);
				otherwise if the player's command includes "dans":
					say "[Tu] [negate the verb peux for bg] y entrer." (B);
				otherwise if the player's command includes "allonger" or the player's command includes "coucher":
					say "[Tu] [negate the verb peux for bg] [t'-s']y allonger." (C);
				otherwise:
					say "[Tu] [negate the verb peux for bg] y aller." (D);
			else:
				if the player's command includes "stand":
					say "[regarding the noun][They're] not something [we] [can] stand on." (E);
				otherwise if the player's command includes "sit":
					say "[regarding the noun][They're] not something [we] [can] sit down on." (F);
				otherwise if the player's command includes "lie":
					say "[regarding the noun][They're] not something [we] [can] lie down on." (G);
				otherwise:
					say "[regarding the noun][They're] not something [we] [can] enter." (H);
		stop the action.

[TODO : trouver un moyen d'utiliser les bons guillemets dans le bout de code I6 qui affiche la signification des pronoms.]

Volume - Understanding

A thing is usually privately-named.
Understand "me" or "myself" as yourself when in English.
Understand "a/an/the/some" as a thing when in English.

Part - Understanding directions

Chapter - Understanding directions (in place of Chapter 2.2.3 - Directions in French Language by Nathanael Marion)

North translates into French as le nord.
South translates into French as le sud.
East translates into French as le est.
West translates into French as le ouest.
Northeast translates into French as le nord-est.
Southwest translates into French as le sud-ouest.
Southeast translates into French as le sud-est.
Northwest translates into French as le nord-ouest.
Inside translates into French as dedans.
Outside translates into French as dehors.
Up translates into French as haut.
Down translates into French as bas.

The French printed name of the north is "nord".
The French printed name of the south is "sud".
The French printed name of the east is "est".
The French printed name of the west is "ouest".
The French printed name of the northeast is "nord-est".
The French printed name of the southwest is "sud-ouest".
The French printed name of the southeast is "sud-est".
The French printed name of the northwest is "nord-ouest".
The French printed name of the inside is "intérieur".
The French printed name of the outside is "extérieur".
The French printed name of the up is "haut".
The French printed name of the down is "bas".

The English printed name of the north is "north".
The English printed name of the south is "south".
The English printed name of the east is "east".
The English printed name of the west is "west".
The English printed name of the northeast is "northeast".
The English printed name of the southwest is "southwest".
The English printed name of the southeast is "southeast".
The English printed name of the northwest is "northwest".
The English printed name of the inside is "inside".
The English printed name of the outside is "outside".
The English printed name of the up is "up".
The English printed name of the down is "down".

Understand the command "nord" as something new.
Understand the command "sud" as something new.
Understand the command "ouest" as something new.
Understand the command "est" as something new.
Understand the command "nordouest" as something new.
Understand the command "nordest" as something new.
Understand the command "sudouest" as something new.
Understand the command "sudest" as something new.
Understand the command "haut" as something new.
[Understand the command "down" as somehing new.]
Understand the command "dedans" as something new.
Understand the command "dehors" as something new.
Understand the command "o" as something new.
Understand the command "no" as something new.
Understand the command "so" as something new.
Understand the command "h" as something new.
Understand the command "b" as something new.
Understand the command "interieur" as something new.
Understand the command "exterieur" as something new.

Understand the command "north" as something new.
Understand the command "south" as something new.
Understand the command "west" as something new.
Understand the command "east" as something new.
Understand the command "northwest" as something new.
Understand the command "northeast" as something new.
Understand the command "southwest" as something new.
Understand the command "southeast" as something new.
Understand the command "up" as something new.
[Understand the command "down" as somehing new.]
Understand the command "inside" as something new.
Understand the command "outside" as something new.
Understand the command "w" as something new.
Understand the command "nw" as something new.
Understand the command "sw" as something new.
Understand the command "u" as something new.
Understand the command "d" as something new.
Understand the command "in" as something new.
Understand the command "out" as something new.

Understand "nord" as north when in French.
Understand "sud" as south when in French.
Understand "ouest" as west when in French.
Understand "est" as east when in French.
Understand "nordouest" as northwest when in French.
Understand "nordest" as northeast when in French.
Understand "sudouest" as southwest when in French.
Understand "sudest" as southeast when in French.
Understand "haut" as up when in French.
Understand "bas" as down when in French.
Understand "dedans" as inside when in French.
Understand "dehors" as outside when in French.
Understand "interieur" as inside when in French.
Understand "exterieur" as outside when in French.
Understand "o" as west when in French.
Understand "no" as northwest when in French.
Understand "so" as southwest when in French.
Understand "h" as up when in French.
Understand "b" as down when in French.

Understand "north" as north when in English.
Understand "south" as south when in English.
Understand "west" as west when in English.
Understand "east" as east when in English.
Understand "northwest" as northwest when in English.
Understand "northeast" as northeast when in English.
Understand "southwest" as southwest when in English.
Understand "southeast" as southeast when in English.
Understand "up" as up when in English.
Understand "down" as down when in English.
Understand "inside" as inside when in English.
Understand "outside" as outside when in English.
Understand "w" as west when in English.
Understand "nw" as northwest when in English.
Understand "sw" as southwest when in English.
Understand "u" as up when in English.
Understand "d" as down when in English.
Understand "in" as inside when in English.
Understand "out" as outside when in English.

Part - Understanding French actions (in place of Part 4.2 - Understand grammar in French Language by Nathanael Marion)

Understand "de/du/des/d'" as "[de]".
Understand "->/a/au/aux" as "[à]".

Understand "inventaire" as taking inventory when in French.
Understand "prendre [things]" as taking when in French.
Understand "lever [something]" as taking when in French.
Understand the command "pr", "ramasser", "decoller" and "cueillir" as "prendre".
Understand the command "relever" and "soulever" as "lever".
Understand "retirer [things inside] [de] [something]" as removing it from when in French.
Understand "deposer [things preferably held]", "deposer [things preferably held] sur le sol", "deposer [things preferably held] au sol", "deposer [things preferably held] par terre", "mettre [things preferably held] sur le sol", "mettre [things preferably held] au sol" or "mettre [things preferably held] par terre" as dropping when in French.
Understand "deposer [other things] sur [something]" or "mettre [other things] sur [something]" as putting it on when in French.
Understand "deposer [other things] dans [something]", "mettre [other things] dans [something]" or "inserer [other things] dans [something]" as inserting it into when in French.
Understand the command "poser", "abandonner", "lacher", "laisser" as "deposer". 
Understand "manger [something preferably held]" or "manger [de] [something preferably held]" as eating when in French.
Understand the command "devorer", "grignoter", "mastiquer" and "ingerer" as "manger".
Understand "aller" as going when in French.
Understand "aller ->/à/a/au/aux/en/vers/par [direction]" or "aller [direction]" as going when in French.
Understand "prendre [direction]" or "prendre vers [direction]" as going when in French.
Understand the command "marcher", "courir", "passer", "fuir", "suivre", "emprunter" and "franchir" as "aller".
Understand "entrer dans/par/-- [something]" as entering when in French.
Understand "aller ->/a/au/aux/en/vers/par/dans/sur/sous/-- [something]" as entering when in French.
Understand "monter ->/a/au/aux/en/vers/par/dans/sur/sous/-- [something]" as entering when in French.
Understand the command "remonter" as "monter".
Understand "asseoir vous dans/sur/-- [something]" or "asseoir vous en haut [de] [something]" as entering when in French.
Understand the command "rentrer" as "entrer".
Understand the command "allonger" and "coucher" as "asseoir".
Understand "sortir", "sortir de la", "sortir d' ici" or "dehors" as exiting when in French.
Understand the command "partir" as "sortir".
Understand "descendre [de] [something]" as getting off when in French.
Understand the command "redescendre" as "descendre".
Understand "regarder autour/--" as looking when in French.
Understand "regarder [something]" and "examiner [something]" as examining when in French.
Understand "regarder sous [something]" and "examiner sous [something]" as looking under when in French.
Understand "regarder en sous -l' [de] [something]" as looking under when in French.
Understand "regarder dans/sur/derriere [something]" and "examiner dans/sur/derriere [something]" as searching when in French.
Understand "regarder a l'/-- interieur [de] [something]" as searching when in French.
Understand "regarder a travers [something]" as searching when in French.
Understand "chercher dans/sur/derriere/-- [something]" as searching when in French.
Understand the command "fouiller" and "farfouiller" as "chercher".
Understand the command "decrire", "observer", "r", "v" and "voir" as "regarder".
Understand "consulter [text] dans [something]" as consulting it about (with nouns reversed) when in French.
Understand "consulter [something] au sujet de/du/des/d [text]", "consulter [something] a propos de/du/des/d [text]" or "consulter [something] a/sur [text]" as consulting it about when in French.
Understand the command "lire" as "consulter".
Understand "fermer [something] avec [something preferably held]", "verrouiller [something] avec [something preferably held]", "fermer [something] a cle/clef" or "fermer a cle/clef [something]" as locking it with when in French.
Understand "ouvrir [something] avec [something preferably held]" or "deverrouiller [something] avec [something preferably held]" as unlocking it with when in French.
Understand "allumer [something]", "mettre [something] en marche/route" or "mettre en marche/route [something]" as switching on when in French.
Understand the command "demarrer" and "commuter" as "allumer".
Understand "eteindre [something]" as switching off when in French.
Understand the command "arreter" as "eteindre".
Understand "ouvrir [something]" as opening when in French.
Understand the command "forcer" as "ouvrir".
Understand "fermer [something]" as closing when in French.
Understand "revetir [something preferably held]", "mettre [something preferably held]" or "habiller vous de/du/des/d'/avec [something preferably held]" as wearing when in French. 
Understand the command "enfiler" as "revetir".
Understand "enlever [something preferably held]" as taking off when in French.
Understand "donner [something preferably held] [à] [someone]" as giving it to when in French. 
Understand "donner [à] [someone] [something preferably held]" as giving it to (with nouns reversed) when in French.
Understand "nourrir [someone] avec [something preferably held]" as giving it to (with nouns reversed) when in French.
Understand the commands "payer", "offrir" and "remettre" as "donner".
Understand "montrer [something] [à] [someone]"  as showing it to when in French.  
Understand the command "presenter" and "pointer" as "montrer".
Understand "reveiller [someone]" as waking when in French.
Understand the command "eveiller" as "reveiller".
Understand "lancer [something preferably held] vers/a/au/aux/sur/contre/dans [something]" or "lancer [something preferably held] en direction [de] [something]" as throwing it at when in French.
Understand the command "jeter" as "lancer".
Understand "attaquer a/au/aux/contre/-- [something]" as attacking when in French.
Understand "tuer [someone]" or "torturer [someone]" as attacking when in French.
Understand the command "frapper", "cogner", "combattre", "briser", "detruire" and "casser" as "attaquer".
Understand "embrasser [someone]" as kissing when in French.
Understand the command "etreindre" as "embrasser".
Understand "repondre [text] ->/a/au/aux [someone]" as answering it that (with nouns reversed) when in French.
Understand "repondre [à] [someone] que [text]" as answering it that when in French.
Understand the command "dire", "crier" and "hurler" as "repondre".
Understand "raconter a propos [de] [text] ->/a/au/aux [someone]" as telling it about (with nouns reversed) when in French.
Understand "raconter [à] [someone] a propos [de] [text]" as telling it about when in French.
Understand "questionner [someone] a propos [de] [text]", "questionner [someone] au sujet [de] [text]" or "questionner [someone] sur [text]" as asking it about when in French.
Understand "demander [à] [someone] a propos [de] [text]", "demander [à] [someone] au sujet [de] [text]" or "demander [à] [someone] pour [text]" as asking it about when in French.
Understand "demander [something] [à] [someone]" as asking it for (with nouns reversed) when in French.
Understand "attendre" as waiting when in French.
Understand the command "a" or "patienter" as "attendre".
Understand "toucher [something]" as touching when in French.
Understand the command "caresser", "tater" and "palper" as "toucher".
Understand "agiter [something]" as waving when in French.
Understand the command "brandir" and "secouer" as "agiter".
Understand "tirer [something]" as pulling when in French.
Understand the command "trainer" as "tirer".
Understand "pousser [something]" as pushing when in French.
Understand "presser [device]" as pushing when in French.
Understand "appuyer sur/-- [something]" as pushing when in French.
Understand "tourner [something]" as turning when in French.
Understand the command "devisser" and "visser" as "tourner".
Understand "pousser [something] a/en/au/vers [direction]" as pushing it to when in French.
Understand the command "deplacer" and "bouger" as "pousser".
Understand "ecraser [something]" or "presser [something]" as squeezing when in French.
Understand the command "tordre" and "comprimer" as "ecraser".
Understand "oui" as saying yes when in French.
Understand the command "ouais", "ok", "ouaip" and "yep" as "oui".
Understand "non" as saying no when in French.
Understand "bruler [something]" or "mettre le feu [à] [something]" as burning when in French.
Understand the command "embraser", "cramer" and "incendier" as "bruler".
Understand "reveiller vous" or "reveiller le joueur" as waking up when in French.
Understand "penser" as thinking when in French.
Understand the command "reflechir" as "penser".
Understand "sentir" as smelling when in French. 
Understand "sentir [something]" as smelling when in French.
Understand the command "renifler" and "humer" as "sentir".
Understand "ecouter" as listening when in French.
Understand "ecouter [something]" as listening when in French.
Understand the command "entendre" as "ecouter".
Understand "gouter [something]" as tasting when in French.
Understand "couper [something]" as cutting when in French.
Understand the commands "decouper", "trancher" and "elaguer" as "couper".
Understand "sauter" as jumping when in French.
Understand the command "bondir" as "sauter".
Understand "nouer [something] ->/a/au/aux/avec/sur [something]" as tying it to when in French. 
Understand the command "attacher", "fixer", "connecter" and "brancher" as "nouer".
Understand "boire du/de/dans/a/au/aux/-- [something]" as drinking when in French.
Understand the command "avaler" and "siroter" as "boire".
Understand "desole" or "excuser vous" as saying sorry when in French.
Understand  the command "pardon" as "desole".
Understand "balancer [something]" or "balancer vous a/au/aux/sur [something]" as swinging when in French.
Understand the command "pendre", "suspendre" and "osciller" as "balancer".
Understand "frotter [something]" as rubbing when in French.
Understand the commands "cirer", "astiquer", "balayer", "nettoyer", "depoussierer", "essuyer" and "recurer" as "frotter".
Understand "regler [something] sur [text]" as setting it to when in French.
Understand the command "ajuster" as "regler".
Understand "agiter la/les/ma/mes/sa/ses/votre/vos main/mains" as waving hands when in French.
Understand "saluer" or "saluer avec la main" as waving hands when in French.
Understand "acheter [something]" as buying when in French.
Understand "grimper à/a/au/aux/par/dans/sur/-- [something]" or "grimper par sur -l' [something]" as climbing when in French. 
Understand the command "gravir" and "escalader" as "grimper".
Understand "dormir" as sleeping when in French.
Understand the command "somnoler" as "dormir".
Understand "quitter" as quitting the game when in French.
Understand "sauver" or "sauvegarder" as saving the game when in French.
Understand "charger" or "restaurer" as restoring the game when in French.
Understand "recommencer" as restarting the game when in French.
Understand "mode court" as preferring abbreviated room descriptions when in French.
Understand "mode long"  as preferring unabbreviated room descriptions when in French.
Understand "mode normal" as preferring sometimes abbreviated room descriptions when in French.
Understand "notifier" or "notification on" as switching score notification on when in French.
Understand "notifier off" or "notification off" as switching score notification off when in French.
Understand "pronoms" as requesting the pronoun meanings when in French.
Understand "* [text]" as a mistake ("[italic type][bracket]Commentaire noté.[close bracket][roman type]") when in French.

[TODO : Inclure les commandes pour Rideable Vehicles et Locksmith.]

Part - Understanding English actions

Section - Understanding English actions (in place of Section SR4/10 - Grammar in Standard Rules by Graham Nelson)

Understand "take [things]" as taking when in English.
Understand "take off [something]" as taking off when in English.
Understand "take [something] off" as taking off when in English.
Understand "take [things inside] from [something]" as removing it from when in English.
Understand "take [things inside] off [something]" as removing it from when in English.
Understand "take inventory" as taking inventory when in English.
Understand the commands "carry" and "hold" as "take".
Understand "get in/on" as entering when in English.
Understand "get out/off/down/up" as exiting when in English.
Understand "get [things]" as taking when in English.
Understand "get in/into/on/onto [something]" as entering when in English.
Understand "get off/down [something]" as getting off when in English.
Understand "get [things inside] from [something]" as removing it from when in English.
Understand "pick up [things]" or "pick [things] up" as taking when in English.
Understand "stand" or "stand up" as exiting when in English.
Understand "stand on [something]" as entering when in English.
Understand "remove [something preferably held]" as taking off when in English.
Understand "remove [things inside] from [something]" as removing it from when in English.
Understand "shed [something preferably held]" as taking off when in English.
Understand the commands "doff" and "disrobe" as "shed".
Understand "wear [something preferably held]" as wearing when in English.
Understand the command "don" as "wear".
Understand "put [other things] in/inside/into [something]" as inserting it into when in English.
Understand "put [other things] on/onto [something]" as putting it on when in English.
Understand "put on [something preferably held]" as wearing when in English.
Understand "put [something preferably held] on" as wearing when in English.
Understand "put down [things preferably held]" or "put [things preferably held] down" as dropping when in English.
Understand "insert [other things] in/into [something]" as inserting it into when in English.
Understand "drop [things preferably held]" as dropping when in English.
Understand "drop [other things] in/into/down [something]" as inserting it into when in English.
Understand "drop [other things] on/onto [something]" as putting it on when in English.
Understand "drop [something preferably held] at/against [something]" as throwing it at when in English.
Understand the commands "throw" and "discard" as "drop".
Understand "give [something preferably held] to [someone]" as giving it to when in English.
Understand "give [someone] [something preferably held]" as giving it to (with nouns reversed) when in English.
Understand the commands "pay" and "offer" and "feed" as "give".
Understand "show [someone] [something preferably held]" as showing it to (with nouns reversed) when in English.
Understand "show [something preferably held] to [someone]" as showing it to when in English.
Understand the commands "present" and "display" as "show".
Understand "go" as going when in English.
Understand "go [direction]" as going when in English.
Understand "go [something]" as entering when in English.
Understand "go into/in/inside/through [something]" as entering when in English.
Understand the commands "walk" and "run" as "go".
Understand "inventory" as taking inventory when in English.
Understand "look" as looking when in English.
Understand "look at [something]" as examining when in English.
Understand "look [something]" as examining when in English.
Understand "look inside/in/into/through [something]" as searching when in English.
Understand "look under [something]" as looking under when in English.
Understand "look up [text] in [something]" as consulting it about (with nouns reversed) when in English.
Understand the command "l" as "look".
Understand "consult [something] on/about [text]" as consulting it about when in English.
Understand "open [something]" as opening when in English.
Understand "open [something] with [something preferably held]" as unlocking it with when in English.
Understand the commands "unwrap", "uncover" as "open".
Understand "close [something]" as closing when in English.
Understand "close up [something]" as closing when in English.
Understand "close off [something]" as switching off when in English.
Understand the commands "shut" and "cover" as "close".
Understand "enter" as entering when in English.
Understand "enter [something]" as entering when in English.
Understand the command "cross" as "enter".
Understand "sit on top of [something]" as entering when in English.
Understand "sit on/in/inside [something]" as entering when in English.
Understand "exit" as exiting when in English.
Understand the commands "leave" and "out" as "exit".
Understand "examine [something]" as examining when in English.
Understand the commands "watch", "describe" and "check" as "examine".
Understand "read about [text] in [something]" as consulting it about (with nouns reversed) when in English.
Understand "read [text] in [something]" as consulting it about (with nouns reversed) when in English.
Understand "yes" as saying yes when in English.
Understand the command "y" as "yes".
Understand "no" as saying no when in English.
Understand "sorry" as saying sorry when in English.
Understand "search [something]" as searching when in English.
Understand "wave" as waving hands when in English.
Understand "wave [something]" as waving when in English.
Understand "set [something] to [text]" as setting it to when in English.
Understand the command "adjust" as "set".
Understand "pull [something]" as pulling when in English.
Understand the command "drag" as "pull".
Understand "push [something]" as pushing when in English.
Understand "push [something] [direction]" or "push [something] to [direction]" as pushing it to when in English.
Understand the commands "move", "shift", "clear" and "press" as "push".
Understand "turn [something]" as turning when in English.
Understand "turn [something] on" or "turn on [something]" as switching on when in English.
Understand "turn [something] off" or "turn off [something]" as switching off when in English.
Understand the commands "rotate", "twist", "unscrew" and "screw" as "turn".
Understand "switch [something switched on]" as switching off when in English.
Understand "switch [something]" or "switch on [something]" or "switch [something] on" as switching on when in English.
Understand "switch [something] off" or "switch off [something]" as switching off when in English.
Understand "lock [something] with [something preferably held]" as locking it with when in English.
Understand "unlock [something] with [something preferably held]" as unlocking it with when in English.
Understand "attack [something]" as attacking when in English.
Understand the commands "break", "smash", "hit", "fight", "torture", "wreck", "crack", "destroy", "murder", "kill", "punch" and "thump" as "attack".
Understand "wait" as waiting when in English.
Understand "answer [text] to [someone]" as answering it that (with nouns reversed) when in English.
Understand the commands "say", "shout" and "speak" as "answer".
Understand "tell [someone] about [text]" as telling it about when in English.
Understand "ask [someone] about [text]" as asking it about when in English.
Understand "ask [someone] for [something]" as asking it for when in English.
Understand "eat [something preferably held]" as eating when in English.
Understand "sleep" as sleeping when in English.
Understand the command "nap" as "sleep".
Understand "climb [something]" or "climb up/over [something]" as climbing when in English.
Understand the command "scale" as "climb".
Understand "buy [something]" as buying when in English.
Understand the command "purchase" as "buy".
Understand "squeeze [something]" as squeezing when in English.
Understand the command "squash" as "squeeze".
Understand "swing [something]" or "swing on [something]" as swinging when in English.
Understand "wake" or "wake up" as waking up when in English.
Understand "wake [someone]" or "wake [someone] up" or "wake up [someone]" as waking when in English.
Understand the commands "awake" and "awaken" as "wake".
Understand "kiss [someone]" as kissing when in English.
Understand the commands "embrace" and "hug" as "kiss".
Understand "think" as thinking when in English.
Understand "smell" as smelling when in English.
Understand "smell [something]" as smelling when in English.
Understand the command "sniff" as "smell".
Understand "listen" as listening to when in English.
Understand "hear [something]" as listening to when in English.
Understand "listen to [something]" as listening to when in English.
Understand "taste [something]" as tasting when in English.
Understand "touch [something]" as touching when in English.
Understand the command "feel" as "touch".
Understand "rub [something]" as rubbing when in English.
Understand the commands "shine", "polish", "sweep", "clean", "dust", "wipe" and "scrub" as "rub".
Understand "tie [something] to [something]" as tying it to when in English.
Understand the commands "attach" and "fasten" as "tie".
Understand "burn [something]" as burning when in English.
Understand the command "light" as "burn".
Understand "drink [something]" as drinking when in English.
Understand the commands "swallow" and "sip" as "drink".
Understand "cut [something]" as cutting when in English.
Understand the commands "slice", "prune" and "chop" as "cut".
Understand "jump" as jumping when in English.
Understand the commands "skip" and "hop" as "jump".
Understand "score" as requesting the score when in English.
Understand "quit" or "q" as quitting the game when in English.
Understand "save" as saving the game when in English.
Understand "restart" as restarting the game when in English.
Understand "restore" as restoring the game when in English.
Understand "verify" as verifying the story file when in English.
Understand "version" as requesting the story file version when in English.
Understand "script" or "script on" or "transcript" or "transcript on" as switching the story transcript on when in English.
Understand "script off" or "transcript off" as switching the story transcript off when in English.
Understand "superbrief" or "short" as preferring abbreviated room descriptions when in English.
Understand "verbose" or "long" as preferring unabbreviated room descriptions when in English.
Understand "brief" or "normal" as preferring sometimes abbreviated room descriptions when in English.
Understand "nouns" or "pronouns" as requesting the pronoun meanings when in English.
Understand "notify" or "notify on" as switching score notification on when in English.
Understand "notify off" as switching score notification off when in English.
Understand "* [text]" as a mistake ("[italic type][bracket]Noted.[close bracket][roman type]") when in English.

Part - Commands that are the same in both language

Understand "inv" as taking inventory.
Understand the command "i" as "inv".
Understand "x [something]" as examining.
Understand "z" as waiting.

Part - Specific French actions (in place of Part 4.3 - Additional language-specific actions in French Language by Nathanael Marion)

[Reading is an action applying to one visible thing and requiring light.
Understand "lire [something]" as reading when in French.
Understand "read [something]" as reading when in English


Report reading (this is the standard report reading rule):
	say "[if in French]Il n'y [adapt the verb as from the third person singular] rien de spécial à lire [ici][else]There [adapt the verb are from the third person singular] nothing to read [here][end if]." (A).

To try is a verb.

Report someone reading (this is the report other people reading rule):
	say "[The actor] [if the current language is the French language][essayes]de lire quelque chose sur [else][try] reading something on[end if] [the noun]." (A).


Singing is an action applying to nothing.
Understand "sing" as singing when in English.
Understand "chanter" as singing when in French.
Understand the command "fredonner" as "chanter".
 
Report singing (this is the standard report singing rule):
	say "[if in French][Tu] [conditionally adapt the verb chantes in past historic tense] un morceau de la première chanson qui [te-lui] [adapt the verb viens from the third person singular] à l'esprit[else][Our] singing [adapt the verb are from the third person singular] abominable[end if]." (A).

Report someone singing (this is the report other people singing rule):
	say "[if in French][The actor] se [conditionally adapt the verb mets in past historic tense] à chanter pendant un moment[else]The singing of [the actor] [adapt the verb are from the third person singular] abominable[end if]." (A). 


Swimming is an action applying to nothing.
Understand "swim" as swimming when in English.
Understand the command "dive" as "swim".
Understand "nager" as swimming when in French. 
Understand the command "plonger" as "nager".

Check swimming (this is the block swimming rule):
	say "[if in French]Il n'y [adapt the verb as from the third person singular] pas assez d'eau pour nager [ici][else]There [negate the verb are from the third person singular] enough water [here][end if]." (A) instead.

Check someone swimming (this is the block other actor swimming rule):
	stop the action. 


Talking to is an action applying to one visible thing. 
Understand "talk to [something]" or "speak to [something]" as talking to when in English.
Understand "parler ->/a/au/aux/avec/-- [something]" or "questionner [something]" as talking to when in French.
Understand the command "discuter" as "parler".

Check an actor talking to (this is the can't talk to a non-person rule):
	if the noun is not a person:
		if the actor is the player, say "[if in French][Tu] [ne peux pas] parler [au noun] [else][We] [cannot] speak to the noun[end if]!" (A) instead;
		else stop the action.

Check someone talking to (this is the other people can't talk to a non-person rule):
	if the noun is not a person, stop the action.

To speak is a verb.

Check an actor talking to (this is the talking to yourself rule):
	if the actor is the player and the noun is the player:
		if in French:
			say "[Tu] [te-se] [parles] à [toi]-même[if the player is plural-named]s[end if] pendant un moment." (A) instead;
		else:
			say "[We] [speak] to [ourselves] for some moment." (B) instead;
	else if the actor is not the player and the noun is the actor:
		stop the action.

Unsuccessful attempt by someone talking to while the reason the action failed is the talking to yourself rule (this is the other people talking to themselves rule): 
	if in French:
		say "[The actor] [marmonnes] quelques mots à [lui]-même[if the actor is plural-named]s[end if] pendant un moment." (A);
	else:
		say "[The actor] [say] some words to [ourselves] for some time." (B).

Report an actor talking to (this is the standard report talking to rule):
	if in French:
		say "[if the actor is the player][Tu][else][The actor][end if] n['][as] rien à dire." (A);
	else:
		say "[if the actor is the player][We][else][The actor][end if] [have] nothing to say." (B).]


InGoing is an action applying to nothing.
Understand "entrer" as InGoing when in French.
Check an actor InGoing:
	convert to the going action on inside.
The specification of the InGoing action is "Cette action n'en est pas véritablement une : elle est dans tous les cas convertie en l'action 'going dedans' et existe uniquement pour que le joueur puisse taper le verbe 'entrer' plutôt que le nom de la direction. À cause de cela, c'est généralement une mauvaise idée d'écrire des règles pour cette action : si une règle telle que 'Instead of InGoing, ...' est écrite, alors elle ne s'appliquera pas si le joueur tape simplement 'aller dedans' ou 'dedans'. La meilleure façon de procéder est d'écrire une règle concernant l'action 'going dedans'."

OutGoing is an action applying to one thing.
Understand "sortir vers/a/au/aux/de/du/des/d' [something]" as OutGoing when in French.
Check an actor OutGoing (this is the standard check outgoing rule):
	if the actor is not in the noun:
		if the actor is the player, say "Mais [tu] [n'es pas] dans [the noun][_]!" (A);
		stop the action;
	else if the actor is in the noun:
		convert to the exiting action on nothing.

[TODO : à voir avec Rideable Vehicles.]
DownGoing is an action applying to nothing.
Understand "descendre" as DownGoing when in French.
Check an actor DownGoing:
	if the actor is on a supporter (called S):
		convert to the getting off action on S;
	else:
		convert to the going action on down.
The specification of the DownGoing action is "Cette action n'en est pas véritablement une : elle est dans tous les cas convertie en l'action 'getting off' si le joueur est sur un support et en l'action 'going bas' sinon. Elle existe uniquement pour que le joueur puisse taper le verbe 'descendre'. À cause de cela, c'est généralement une mauvaise idée d'écrire des règles pour cette action : si une règle telle que 'Instead of DownGoing, ...' est écrite, alors elle ne s'appliquera pas si le joueur tape simplement 'descendre de la plate-forme' ou 'bas' par exemple. La meilleure façon de procéder est d'écrire une règle concernant l'action 'going bas' ou 'getting off', selon le cas."

UpGoing is an action applying to nothing.
Understand "monter" as UpGoing when in French.
Check an actor UpGoing:
	convert to the going action on up.
The specification of the UpGoing action is "Cette action n'en est pas véritablement une : elle est dans tous les cas convertie en l'action 'going haut' et existe uniquement pour que le joueur puisse taper le verbe 'monter' plutôt que le nom de la direction. À cause de cela, c'est généralement une mauvaise idée d'écrire des règles pour cette action : si une règle telle que 'Instead of UpGoing, ...' est écrite, alors elle ne s'appliquera pas si le joueur tape simplement 'aller en haut' ou 'haut'. La meilleure façon de procéder est d'écrire une règle concernant l'action 'going haut'."

UpStanding is an action applying to nothing.
Understand "debout", "lever" or "lever vous" as UpStanding when in French.
Check an actor UpStanding (this is the standard check upstanding rule): 
	if the actor is in the location:
		if the actor is the player, say "Inutile." (A);
		stop the action;
	else:
		convert to the exiting action on nothing.

Switching Between English and French ends here.

---- DOCUMENTATION ----

Cette extension permet de changer la langue d'une fiction interactive en cours de jeu. Toutes les réponses par défaut (y compris celles des extensions incluses dans Inform) seront écrites dans la langue choisie, et les commandes entrées par le joueur ne pourront être écrites que dans celle-ci.

Tout d'abord, la source doit inclure l'extension French Language par Nathanael Marion :

	"Bilingue" by Nathanaël Marion (in French)

Une fois que cela est fait, il suffit d'utiliser l'une des deux phrases suivantes pour changer de langue au cours du jeu :

	switch to French;
	switch to English;

Il est possible de les utiliser à n'importe quel moment de la partie, mais il devient obligatoire d'initialiser la langue à son commencement : 

	When play begins:
		switch to French.
	
	Carry out going to United Kingdom:
		switch to English.

Cependant, une partie du travail revient à l'auteur de la fiction interactive. En effet, vous devez fournir les traductions des textes que vous écrivez dans votre source. Pour ce faire, il faut utiliser les conditions suivantes :

	if in French: ...
	if in English: ...

L'extension facilite la traduction des noms affichés à l'écran, des articles et des descriptions, pour que vous n'ayez pas à utiliser ces conditions en permanence. Ainsi, tous les objets possèdent maintenant six nouveaux textes, le "French printed name", le "English printed name", le "French printed plural name", le "English printed plural name", le "French description", le "English description", le "French indefinite article" et le "English indefinite article". Ils permettent de spécifier le nom, la description, et l'article de chaque objet selon la langue.
Il devient obligatoire de spécifier les "printed name" pour tous les objets, sinon ils s'afficheront en tant que "*objet non traduit*" (ou "*untranslated object*" en anglais) dans le jeu ! Ce n'est pas le cas des descriptions et des articles, qui auront des textes par défaut si non spécifiées dans la source.

	La lampe de cuivre est une chose.
	Le French printed name est "lampe de cuivre". Le English printed name est "brass lamp".
	Le French indefinite article est "ma". Le English indefinite article est "my".
	La French description est "C'est la lumineuse et indispensable lampe de cuivre !". La English description est "This is the indispensable brass lamp!".

Si l'on veut le même nom, la même description ou le même article pour un objet quelle que soit la langue (par exemple pour un nom propre ou le titre d'un livre), il suffit de modifier le "printed name", la "description" ou le "indefinite article", sans mentionner la langue.

	Paris est un endroit. Le printed name est "Paris".

Enfin, si vous créez de nouvelles actions ou des nouveaux synonymes, il est important de ne pas oublier de les rendre bilingues :

	Smiling is an action applying to nothing.
	Understand "smile" as smiling when in English.
	Understand "sourire" as smiling when in French.
	
	Report smiling:
		say "[if in French]Vous souriez[else]You smile[end if].".

Les lignes de compréhension ("Understand ...") sont extrêmement importantes pour les choses. En effet, ceux-ci sont désormais "privately-named" par défaut. Cela signifie qu'Inform ne créera plus automatiquement ces lignes de compréhension et que le joueur ne sera plus en mesure d'intéragir avec les choses si vous ne précisez pas de synonymes. Il reste cependant possible de le rendre "publicly-named" dans la source.

Example: * Tunnel sous la Manche - Un petit exemple illustrant le fonctionnement de l'extension.

	*: "Tunnel sous la Manche" by Nathanaël Marion (in French)
	
	Include Switching Between English And French by Nathanael Marion.
	
	When play begins:
		now the story viewpoint is second person plural;
		switch to French.

	La France est un endroit. "Vous êtes en France, et le Royaume-Uni est au nord.".
	The printed name is "France".

	La tour Eiffel est une fixed in place chose dans la France.
	Le printed name est "tour Eiffel".
	Le indefinite article of la tour is "la".
	
	Le dictionnaire est une chose dans la France.
	Le French printed name est "dictionnaire".
	Le English printed name est "dictionary".
	Le French indefinite article est "votre".
	Le English indefinite article est "your".
	Understand "dictionnaire" as the dictionnaire when in French.
	Understand "dictionary" as the dictionnaire when in English.
	La French description est "Le plus bel ouvrage du monde.".
	La English description est "The most beautiful book in the world.".
	
	Instead of reading the dictionnaire:
		if in French:
			say "Vous n'avez pas le temps de lire tous ces mots merveilleux.";
		else:
			say "You lack the time to read all these marvellous words.". 
	
	
	United Kingdom is north from France. "You are in the United Kingdom, and France is south.".
	The printed name is "United Kingdom".
	
	Big Ben is fixed in place in United Kingdom.
	The printed name is "Big Ben".

	Carry out going to United Kingdom:
		switch to English.
   
	Carry out going to France:
		switch to French.   
	
	
	Test me with "wait / attendre / prendre le dictionnaire / i / nord / attendre / wait / i".